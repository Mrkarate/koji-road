var AnimaTexturas = pc.createScript('animaTexturas');
AnimaTexturas.attributes.add("texturas", {type: "asset", assetType: "texture", array: true, title: "texturas"});
AnimaTexturas.attributes.add('loop', {type: 'number', default:0,title: 'loop'});
AnimaTexturas.attributes.add('autoAnim', {type: 'number', default:0,title: 'autoAnim'});
AnimaTexturas.attributes.add('texture3D', {type: 'number', default:0,title: 'texture3D'});
AnimaTexturas.attributes.add('frames', {type: 'number', default:60,title: 'frames'});

// initialize code called once per entity
AnimaTexturas.prototype.initialize = function() {
    
    var anim = this.entity;    
    var array = [this.entity,this.texturas,0,this.texturas.length,false,0,0,0,false];
    this.cont = 0;
    this.arrayAnimaciones = [];
    this.arrayAnimaciones[0] = array;   
    this.callBack = null;
    
    //is.arrayAnimaciones[0][4] = true;   
    if(this.autoAnim == 1)
    {        
        if(this.loop == 1)
            this.arrayAnimaciones[0][8] = true;
        this.arrayAnimaciones[0][4] = true; 
    }
};
AnimaTexturas.prototype.inicioAnim = function(callBack,desde,hasta)
{  
     this.callBack = callBack;
     this.cont = 0;
     var array = [this.entity,this.texturas,0,this.texturas.length,false,0,0,0,false];
     this.cont = 0;         
     if(desde != null && hasta != null)
     {
        this.arrayAnimaciones[0][2] = desde;
        this.arrayAnimaciones[0][3] = hasta;    
     }
     else
     {
        var array = [this.entity,this.texturas,0,this.texturas.length,false,0,0,0,false];       
        this.arrayAnimaciones = [];
        this.arrayAnimaciones[0] = array;   
     }
     this.cont = 0;  
     this.entity.element.texture = this.texturas[this.arrayAnimaciones[0][2]].resource;
     this.arrayAnimaciones[0][4] = true; 
     
};
AnimaTexturas.prototype.stopAnim = function()
{
    this.arrayAnimaciones[0][4] = false;
};
// update code called every frame
AnimaTexturas.prototype.update = function(dt) {
     
    var tiempo = dt;
    var fps = 1/tiempo;
    this.cont = this.cont + tiempo;    
    var frameLimite = fps / this.frames;
    frameLimite = frameLimite / fps;
    
    if (this.cont >= frameLimite)
    {
        this.cont = 0;
        for(var i = 0; i < this.arrayAnimaciones.length;i++)
        {
           if(this.arrayAnimaciones[i][4] == true)           
           {    
               if(this.arrayAnimaciones[i][7] == 0)
               {
                    var currentFrame = this.arrayAnimaciones[i][5];
                    if(currentFrame == 0)
                        currentFrame = this.arrayAnimaciones[i][2];
                    if(this.texture3D == 0)                    
                        this.arrayAnimaciones[i][0].element.texture = this.arrayAnimaciones[i][1][currentFrame].resource;    
                    else
                         this.cambiaTextura3D(this.arrayAnimaciones[i][1][currentFrame].resource);
                   //console.log( this.arrayAnimaciones[i][1][currentFrame].resource);
                    this.arrayAnimaciones[i][5] = this.arrayAnimaciones[i][5] + 1;
                    currentFrame = this.arrayAnimaciones[i][5];
                    if(currentFrame == this.arrayAnimaciones[i][3])
                    {
                        if(this.arrayAnimaciones[i][6]  == 0)      
                        {
                            if( this.arrayAnimaciones[i][8])
                                 this.arrayAnimaciones[i][5] = 1;
                            else
                            {
                                this.arrayAnimaciones[i][4] = false;   
                                this.arrayAnimaciones[i][5] = 0;
                                if(this.callBack != null)
                                    this.callBack();

                            }
                        }
                        else if(this.arrayAnimaciones[i][6]  == 2)      
                        {
                            this.arrayAnimaciones[i][7] = 1;
                        }                    
                    }               
               }           
               if(this.arrayAnimaciones[i][7] == 1)
               {
                    var currentFrame = this.arrayAnimaciones[i][5];
                    if(currentFrame == this.arrayAnimaciones[i][3])
                    {
                        currentFrame = this.arrayAnimaciones[i][3] - 2;
                        this.arrayAnimaciones[i][5] = currentFrame;
                    }
                    if(this.texture3D == 0)       
                        this.arrayAnimaciones[i][0].element.texture = this.arrayAnimaciones[i][1][currentFrame].resource;           
                    else
                        this.cambiaTextura3D(this.arrayAnimaciones[i][1][currentFrame].resource);
                    this.arrayAnimaciones[i][5] = this.arrayAnimaciones[i][5] - 1;             
                    currentFrame = this.arrayAnimaciones[i][5];
                    if(currentFrame == this.arrayAnimaciones[i][2]-1)
                    {                   
                        this.arrayAnimaciones[i][4] = false;   
                        this.arrayAnimaciones[i][5] = 0;

                        if(this.arrayAnimaciones[i][6]  == 2)                         
                            this.arrayAnimaciones[i][7] = 0;                                  
                    }               
               }           
           }
        }
    }
};
AnimaTexturas.prototype.cambiaTextura3D = function(textura)
{
   var meshInstances = this.entity.model.meshInstances;
    for (var i = 0; i < meshInstances.length; ++i) { 
        var mesh = meshInstances[i];
        mesh.material.diffuseMap = textura;
        mesh.material.opacityMap = textura;
        mesh.material.update();
    }  
};

