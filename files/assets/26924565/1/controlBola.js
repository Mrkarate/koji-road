var ControlBola = pc.createScript('controlBola');
ControlBola.attributes.add('bolaSkin', {type: 'entity', title: 'bolaSkin'});
ControlBola.attributes.add('bolaColider', {type: 'entity', title: 'bolaColider'});
ControlBola.attributes.add('scoresMCpos', {type: 'entity', title: 'scoresMCpos'});
ControlBola.attributes.add('bolaSombra', {type: 'entity', title: 'bolaSombra'});
ControlBola.attributes.add('startTrail', {type: 'entity', title: 'startTrail'});
ControlBola.attributes.add('fog', {type: 'entity', title: 'fog'});
ControlBola.attributes.add('fogT', {type: 'entity', title: 'fogT'});
ControlBola.attributes.add('msgs1', {type: 'entity', title: 'msgs1'});
ControlBola.attributes.add('msgs2', {type: 'entity', title: 'msgs2'});
ControlBola.attributes.add('msgs3', {type: 'entity', title: 'msgs3'});
ControlBola.attributes.add('msgs4', {type: 'entity', title: 'msgs4'});
ControlBola.attributes.add('scoresMC', {type: 'entity', title: 'scoresMC'});
ControlBola.attributes.add('sombra', {type: 'entity', title: 'sombra'});
ControlBola.attributes.add('camara', {type: 'entity', title: 'camara'});
ControlBola.attributes.add('cameraFake', {type: 'entity', title: 'cameraFake'});
ControlBola.attributes.add('particulas_rojas', {type: 'entity', title: 'particulas_rojas'});
ControlBola.attributes.add('particulas_verdes', {type: 'entity', title: 'particulas_verdes'});
ControlBola.attributes.add('particulas_amarillas', {type: 'entity', title: 'particulas_amarillas'});
ControlBola.attributes.add('brillo_rojas', {type: 'entity', title: 'brillo_rojas'});
ControlBola.attributes.add('brillo_verdes', {type: 'entity', title: 'brillo_verdes'});
ControlBola.attributes.add('brillo_amarillas', {type: 'entity', title: 'brillo_amarillas'});
ControlBola.attributes.add('ondaCaida', {type: 'entity', title: 'ondaCaida'});
ControlBola.attributes.add('ondaMuerte', {type: 'entity', title: 'ondaMuerte'});
ControlBola.attributes.add("materialBolas", {type: "asset", assetType: "material", array: true, title: "materialBolas"});
ControlBola.attributes.add('currentColor', {type: 'string', title: 'currentColor'});
ControlBola.attributes.add('velocidadSaltoGrande', {type: 'number', title: 'velocidadSaltoGrande'});
ControlBola.attributes.add('glow_up', {type: 'entity', title: 'glow_up'});
ControlBola.attributes.add('glow_down', {type: 'entity', title: 'glow_down'});
ControlBola.attributes.add('glow_left', {type: 'entity', title: 'glow_left'});
ControlBola.attributes.add('glow_right', {type: 'entity', title: 'glow_right'});
ControlBola.attributes.add('P1', {type: 'entity', title: 'P1'});
ControlBola.attributes.add('P2', {type: 'entity', title: 'P2'});
ControlBola.attributes.add('tutorialMC', {type: 'entity', title: 'tutorialMC'});
ControlBola.attributes.add('bolaPlayerStiker', {type: 'entity', title: 'bolaPlayerStiker'});

// initialize code called once per entity
ControlBola.prototype.initialize = function() {
   
    _app = this.app;
 
    this.conTutorial = false;
    asignaEventos(this.tutorialMC.children[0],true,1,this.clickTutorial.bind(this)); 
    this.path = this.entity.script.cameraPath;    
    this.shaker = this.camara.script.shake;
    this.indexMSG = 0;
    this.trailIndex = 0;
    this.trailTotal = this.bolaSombra.children.length;
    this.hiloTrail = [];
    
    this.esperaEternaContador = 0;
    this.elementosAlfa = [];
    this.elementosAlfaInv = [];
    this.hiloAlfa = null;
    _control = this; 
   
   
    if (isMobile.any()) {
        this.app.touch.on(pc.EVENT_TOUCHSTART, this.onTouchStart, this);
        this.app.touch.on(pc.EVENT_TOUCHMOVE, this.onTouchMove, this);
        this.app.touch.on(pc.EVENT_TOUCHEND, this.onTouchEnd, this);
      
    }
    else
    {
        this.app.mouse.on(pc.EVENT_MOUSEDOWN, this.onMouseDown, this);
        this.app.mouse.on(pc.EVENT_MOUSEMOVE, this.onMouseMove, this);
        this.app.mouse.on(pc.EVENT_MOUSEUP, this.onMouseUp, this);
        document.addEventListener ("mouseout", this.onMouseLeave);
    }
    this.puntoPartida = this.entity.getPosition().clone();
    this.bolaPartida = this.bolaSkin.getLocalPosition().clone();
    this.camaraPartida = this.camara.getLocalPosition().clone();
    this.bolaY = this.bolaSkin.getLocalPosition().clone().y;
    this.iniVars();
    iniVars();
    gameStart = false;
    this.checkSoundConfig();
    this.path.start(false);
    //tocaSonido("click");
    this.anterior = null;
    //setTimeout(this.empezarGame.bind(this),window.gameplay.delayStart);
    this.panelMain();
     //this.fog.script.cambiaTextura.cambio(currentLevel-1);
     this.camara.camera.clearColor = new pc.Color().fromString(window["pista"+currentLevel+"_niebla_color"]);
  
    
    
};
ControlBola.prototype.clickTutorial = function()
{    
    this.tutorialMC.enabled = false;
    this.clickStartb();
};
ControlBola.tap = false;
ControlBola.prototype.iniVars = function()
{
    this.bolaSombra.enabled = false;
    this.setColor("rojo");    
    this.entity.setPosition(this.puntoPartida);
    this.bolaSkin.setLocalPosition(this.bolaPartida);
    this.camara.setLocalPosition(this.camaraPartida);
    this.cameraFake.setLocalPosition(this.camaraPartida);
    this.bolaSkin.model.enabled = true;
    this.bolaPlayerStiker.model.enabled = true;
    this.sombra.enabled = true;    
    this.clickInicial = null;
    this.clickInicialFijo = null;
    this.clickFinal = null;
    this.bolaInicial = null;
    this.bolaInicialFijo = null;
    this.hiloParticulas = [null,null,null];
    this.indexParticulas = [0,0,0];
    this.totalParticulas = this.particulas_rojas.children.length;
    this.puntoMouseMove = null;
    this.bolaDistXAcomulada = 0;  
    ControlBola.tap = false;  
    
    this.glow_up.enabled = false;
    this.glow_down.enabled = false;
    this.glow_left.enabled = false;
    this.glow_right.enabled = false;
    
    this.P1.enabled = false;
    this.P2.enabled = false;
};


ControlBola.prototype.setColor = function(color)
{
   var color = this.getColor(color);  
   //this.glow_up.enabled = true;
   //this.glow_down.enabled = true;
   //this.glow_left.enabled = true;
   //this.glow_right.enabled = true;
   //this.glow_up.script.cambiaTextura.cambio(color);
   //this.glow_down.script.cambiaTextura.cambio(color);
   //this.glow_left.script.cambiaTextura.cambio(color);
   //this.glow_right.script.cambiaTextura.cambio(color);   
   var material = this.materialBolas[color].resource;
   var sticker = this.materialBolas[color+3].resource;
   this.bolaSkin.model.meshInstances[0].material = material; 
   this.bolaPlayerStiker.model.meshInstances[0].material = sticker;    
   this.currentColor = color;
};
ControlBola.prototype.getColor = function(color)
{
    switch(color)
    {
        case "rojo":
            //this.camara.camera.clearColor = new pc.Color().fromString("ffe0de");
            return(0);
        break;
        case "amarillo":
            //this.camara.camera.clearColor = new pc.Color().fromString("f9f3cb");
            return(1);
        break;
        case "verde":
            //this.camara.camera.clearColor = new pc.Color().fromString("d9fadf");
            return(2);
        break;
    }
    return(-1);
};
ControlBola.prototype.saltoGrande = function(llegada,tipo)
{
    enSalto = true;
    if(tipo == 1)
    {
        
        var pos = this.entity.getLocalPosition();    
        var destino = new pc.Vec3();
        
        destino = llegada;
        var distancia = new pc.Vec3();
        distancia.sub2(pos,destino); 
        distancia = distancia.length();
        var t = distancia/this.velocidadSaltoGrande;
        var tween =  this.entity.tween(pos).to(destino, t, pc.Linear);   
        tween.on('complete',this.finSaltoGrande.bind(this));
        tween.start();  
    }
    else if(tipo == 0)
    {         
        tocaSonido("saltoGrande");
        this.sombra.enabled = false;
        var pos = this.entity.getLocalPosition();    
        var destino = new pc.Vec3();        
        destino.add2(pos,new pc.Vec3(0,10,14));
        var distancia = new pc.Vec3();
        distancia.sub2(pos,destino); 
        distancia = distancia.length();
        var t = distancia/this.velocidadSaltoGrande;
        var tween =  this.entity.tween(pos).to(destino, t, pc.Linear);     
        tween.on('complete',this.saltoGrande.bind(this,llegada,1));
        tween.start(); 
        this.fogT.script.animaUvs.setAnim(1,0,0.5);
        this.startTrail.enabled = false;
        
        /* setTimeout(function() {
           
        }.bind(this),t*1000/0.8);*/
       
    }
};
ControlBola.prototype.finSaltoGrande = function()
{
    //this.fog.script.cambiaTextura.cambio(currentLevel-1);
    this.camara.camera.clearColor = new pc.Color().fromString(window["pista"+currentLevel+"_niebla_color"]);
    this.fogT.script.animaUvs.setAnim(1,0.5,1);
    reproduceAnim(this.bolaSkin,1); 
    reproduceAnim(this.bolaPlayerStiker,1);
    enSalto = false;
    this.sombra.enabled = true;
    this.path.empezar();
    tocaSonido("caida");
    this.ondaCaida.enabled = true;
    reproduceAnim(this.ondaCaida,0,1,true,1500);
    vibrar(250);
    this.startTrail.enabled = true;  
    
    
};
ControlBola.prototype.saltoRampa = function(tipo)
{
    
    if(tipo == 1)
    {
          this.brillo_amarillas.enabled = false;
        this.brillo_rojas.enabled = false;
        this.brillo_verdes.enabled = false;
        
        
        var pos = this.bolaSkin.parent.getLocalPosition();    
        var destino = new pc.Vec3();
        
        destino = new pc.Vec3(0,0,0);
        var distancia = new pc.Vec3();
        distancia.sub2(pos,destino); 
        distancia = distancia.length();
         var vel = 50;
        var t = distancia/vel;
        var tween =  this.bolaSkin.parent.tween(pos).to(destino, t, pc.Linear);   
        tween.on('complete',this.finSaltoRampa.bind(this));
        tween.start(); 
        vibrar(180);
        
        
    }
    else if(tipo == 0)
    {         
        this.sombra.enabled = false;
        var pos = this.bolaSkin.parent.getLocalPosition();    
        var destino = new pc.Vec3();        
        destino.add2(pos,new pc.Vec3(0,5.5,0));
        var distancia = new pc.Vec3();
        distancia.sub2(pos,destino); 
        distancia = distancia.length();
        var vel = 50;
        var t = distancia/vel;
        var tween =  this.bolaSkin.parent.tween(pos).to(destino, t, pc.Linear);     
        tween.on('complete',this.saltoRampa.bind(this,1));
        tween.start();  
    }
};
ControlBola.prototype.finSaltoRampa = function()
{
    reproduceAnim(this.bolaSkin,1);
    reproduceAnim(this.bolaPlayerStiker,1);
    this.sombra.enabled = true;
};
ControlBola.prototype.apagaMsg = function(index)
{
    this["msgs"+(index+1)].enabled = false;
};  
// update code called every frame
ControlBola.prototype.update = function(dt) 
{
    if(enPause)
        return;
    
   if(this.puntoMouseMove != null)
      this.calculaMouseMove();
    
   if(gameOver)
      return;
    
    this.haceTrail(dt);
    
  
    
   if(this.path.pathListo)
   {          
       var tocado = 0;
       for(var i = 0; i < objetosEnNivel.length; i ++)
       {
           var posBola = this.bolaColider.getPosition();           
           var posObjeto = objetosEnNivel[i][0].getPosition();
           var id = objetosEnNivel[i][2];
           if(id == idNoRev)
               continue;
           
           var distancia = new pc.Vec3();
           distancia.sub2(posBola,posObjeto);
           distancia = distancia.length();
           
           if(this.esBolaMov(objetosEnNivel[i][1]) && objetosEnNivel[i][0].enabled && posBola.z < posObjeto.z + 0.5 && distancia < 700 && objetosEnNivel[i][3] != null && objetosEnNivel[i][4] == null) 
           {                    
                   //console.log("funciono estoy llamando al " + objetosEnNivel[i][1]);
                    var corrimiento = objetosEnNivel[i][3];
                    var dist = 0;
                    var t = window.gameplay.enemy_ballSpeed;
                    var tn = t;
                    var nuevoMov = 0;
                    switch(corrimiento)
                    {
                        case 3.8:
                            dist = -3.8*2;                            
                            nuevoMov = 3.8*2;
                        break;
                        case 0:                            
                            var tipo = objetosEnNivel[i][1];
                            var len = tipo.length;
                            if(tipo[len-1] == "I")
                            {
                                dist = -3.8;
                                nuevoMov = 3.8*2;
                            }
                            else if(tipo[len-1] == "D")
                            {
                                dist = 3.8;
                                nuevoMov = -3.8*2;
                            }
                            t = t/2;
                        break;
                        case -3.8:
                            dist = 3.8*2;
                            nuevoMov = -3.8*2;
                        break;
                    }
                    objetosEnNivel[i][4] = 1;
                    ////console.log("lo voy a mover asi " + dist  + "  nuevo mov " + nuevoMov + "   con t " + t);
                    var pos = objetosEnNivel[i][0].getLocalPosition().clone();
                    objetosEnNivel[i][0].translateLocal(0,0,dist); 
                    var destino = objetosEnNivel[i][0].getLocalPosition().clone();
                    objetosEnNivel[i][0].setLocalPosition(pos);                    
                    objetosEnNivel[i][4] = objetosEnNivel[i][0].tween(objetosEnNivel[i][0].getLocalPosition()).to(destino, t, pc.Linear);   
                    objetosEnNivel[i][4].on('complete',this.mueveMov.bind(this,i,nuevoMov,tn));
                    objetosEnNivel[i][4].start();  
               
           }
           else if(this.esBolaMov(objetosEnNivel[i][1]) && posBola.z - 15 > posObjeto.z && objetosEnNivel[i][3] != null && objetosEnNivel[i][4] != null) 
           {        
               if(objetosEnNivel[i][4] != null)
               {
                   ////console.log("funciono  y lo detuve estoy llamando al " + objetosEnNivel[i][1]);
                   objetosEnNivel[i][4].stop();
                   objetosEnNivel[i][4] = null;    
                   
                   if(this.esBolaBi(objetosEnNivel[i][1]))
                   {
                      this.poneMSG(5);                       
                   }
               }
           }
           else if(this.esBolaBi(objetosEnNivel[i][1]) && !this.esBolaMov(objetosEnNivel[i][1]) && posBola.z - 5 > posObjeto.z && distancia < 5) 
           {             
                   ////console.log("funciono  y lo detuve estoy llamando al " + objetosEnNivel[i][1]);
               
               if(objetosEnNivel[i][0].enabled)
               {                
                  
                   this.poneMSG(5);                   
                   objetosEnNivel[i][0].enabled = false;
               }
               
           }
           else if(!bolaParpadeando && this.esBola(objetosEnNivel[i][1]) && objetosEnNivel[i][0].enabled && posBola.z < posObjeto.z +2) 
           {               
               if(posBola.z < posObjeto.z + bolaRadio*2.5 && distancia < bolaRadio*2.4)
               {    
                   if(objetosEnNivel[i][4] != null)
                       objetosEnNivel[i][4].stop();
                   objetosEnNivel[i][4] = null;      
                   tocado = this.comeBola(objetosEnNivel[i][0], objetosEnNivel[i][1],i,objetosEnNivel[i][2],id);
                 /* tocado = this.comeBola(objetosEnNivel[i][0], objetosEnNivel[i][1],i,objetosEnNivel[i][2],id); 
                  if(tocado == 1)
                  {
                      idNoRev = id;
                      break;
                  }*/   
                  ////console.log("encontre un muerto per ovoy a buscar si encuentro otra awea " + tocado);
               }
           }
           else if(!this.esBola(objetosEnNivel[i][1]) && objetosEnNivel[i][2] != -1 && posBola.z < posObjeto.z +2)
           {
              if(posBola.z < posObjeto.z+0.5 && distancia < volumenRampa )
               {      
                  this.subeRampa(objetosEnNivel[i][0], objetosEnNivel[i][1],i);
                  objetosEnNivel[i][2] = -1;
                  break;                   
               }
           }
       }
       //if(tocado == 2)
         //  this.gameOver();
   }
    
    
    if(this.elementosAlfa.length > 0)
    {
        for(var i = 0; i < this.elementosAlfa.length;i++)
        {
            this.elementosAlfa[i][1] = this.elementosAlfa[i][1] + dt*this.elementosAlfa[i][2];
            var alfa = this.elementosAlfa[i][1];
            if(alfa > 1)
                alfa = 1;
            this.elementosAlfa[i][0].opacity = alfa;
            if(alfa == 1)
            {
                this.elementosAlfa[i][2] = 0;
                this.elementosAlfa.splice(i,1);            
            }
        }
    }
    else if(this.elementosAlfaInv.length > 0)
    {
        for(var i = 0; i < this.elementosAlfaInv.length;i++)
        {
            this.elementosAlfaInv[i][1] = this.elementosAlfaInv[i][1] - dt*this.elementosAlfaInv[i][2];
            var alfa = this.elementosAlfaInv[i][1];
            if(alfa < 0)
                alfa = 0;
            this.elementosAlfaInv[i][0].opacity = alfa;
            if(alfa == 0)
            {
                this.elementosAlfaInv[i][2] = 0;
                this.elementosAlfaInv.splice(i,1);            
            }
        }
    }
   
    
}; 
ControlBola.prototype.mueveMov = function (i,dist,t) 
{       
     var pos = objetosEnNivel[i][0].getLocalPosition().clone();
     objetosEnNivel[i][0].translateLocal(0,0,dist); 
     var destino = objetosEnNivel[i][0].getLocalPosition().clone();
     objetosEnNivel[i][0].setLocalPosition(pos);                    
     objetosEnNivel[i][4] = objetosEnNivel[i][0].tween(objetosEnNivel[i][0].getLocalPosition()).to(destino, t, pc.Linear);   
     objetosEnNivel[i][4].on('complete',this.mueveMov.bind(this,i,dist*-1,t));
     objetosEnNivel[i][4].start();  
};
  
ControlBola.prototype.esBola = function (tipo) 
{  
  
    switch(tipo)
    {
        case "A":
        case "R":
        case "V":
        case "AI":
        case "RI":
        case "VI":
        case "AD":
        case "RD":
        case "VD":
        case "CA":
        case "CR":
        case "CV":
        case "CAI":
        case "CRI":
        case "CVI":
        case "CAD":
        case "CRD":
        case "CVD":
            return(true);
        break;
    }
    return(false);  
}; 
ControlBola.prototype.esBolaBi = function (tipo) 
{    
    switch(tipo)
    {       
        case "CA":
        case "CR":
        case "CV":
        case "CAI":
        case "CRI":
        case "CVI":
        case "CAD":
        case "CRD":
        case "CVD":
            return(true);
        break;
    }
    return(false);  
};
ControlBola.prototype.esBolaMov = function (tipo) 
{  
  
    switch(tipo)
    {        
        case "AI":
        case "RI":
        case "VI":
        case "AD":
        case "RD":
        case "VD":      
        case "CAI":
        case "CRI":
        case "CVI":
        case "CAD":
        case "CRD":
        case "CVD":
            return(true);
        break;
    }
    return(false);  
}; 
ControlBola.prototype.poneParticulas = function(conBrillo)
{
    if(conBrillo)
        clearInterval(this.hiloParticulas[this.currentColor]);
    var index = this.currentColor;
    if(this.currentColor == 1)
    {       
        reproduceParticulas(this.particulas_amarillas.children[this.indexParticulas[this.currentColor]],900);
        this.indexParticulas[this.currentColor] = this.aumentaIndexParticula(this.indexParticulas[this.currentColor]);
        if(conBrillo)
        {
            this.brillo_amarillas.enabled = true;
            reproduceAnim(this.brillo_amarillas,0);
            this.hiloParticulas[this.currentColor] = setInterval(this.apagaBrillos.bind(this,this.brillo_amarillas,index),500);
        }
    }
    else  if(this.currentColor == 2)
    {
         reproduceParticulas(this.particulas_verdes.children[this.indexParticulas[this.currentColor]],900);
         this.indexParticulas[this.currentColor] = this.aumentaIndexParticula(this.indexParticulas[this.currentColor]);
         if(conBrillo)
         {
             this.brillo_verdes.enabled = true;
             reproduceAnim(this.brillo_verdes,0);
             this.hiloParticulas[this.currentColor] = setInterval(this.apagaBrillos.bind(this,this.brillo_verdes,index),500);
         }
    }
    else  if(this.currentColor == 0)
    {
         reproduceParticulas(this.particulas_rojas.children[this.indexParticulas[this.currentColor]],900);
         this.indexParticulas[this.currentColor] = this.aumentaIndexParticula(this.indexParticulas[this.currentColor]);
         if(conBrillo)
         {
             this.brillo_rojas.enabled = true;
             reproduceAnim(this.brillo_rojas,0);
             this.hiloParticulas[this.currentColor] = setInterval(this.apagaBrillos.bind(this,this.brillo_rojas,index),500);
         }
    }
    
};
ControlBola.prototype.aumentaIndexParticula = function (num) 
{  
   num = num + 1;
   if(num == this.totalParticulas)
     num = 0;

   return(num);
};
ControlBola.prototype.apagaBrillos = function (brilloMC,index) 
{  
   brilloMC.enabled = false;
   clearInterval(this.hiloParticulas[index]);
};
ControlBola.prototype.subeRampa = function (rampa,tipo,index) 
{  
   //objetosEnNivel.splice(index,1);    
   reproduceAnim(this.bolaSkin,1); 
   reproduceAnim(this.bolaPlayerStiker,1);
   //this.shaker.shake(2, 0.3);
   switch(tipo)
   {
       case "RA":
           tocaSonido("rampaYellow");
           this.setColor("amarillo");
           this.poneParticulas(true);
           this.saltoRampa(0);
           combo = 0;
       break;
       case "RV":
           tocaSonido("rampaGreen");
           this.setColor("verde");
           this.poneParticulas(true);
           this.saltoRampa(0);
           combo = 0;
       break;
       case "RR":
           tocaSonido("rampaRed");
           this.setColor("rojo");
           this.poneParticulas(true);
           this.saltoRampa(0);
           combo = 0;
       break;
   }
   return(false);
    
};
ControlBola.prototype.comeBola = function (bola,tipo,index) 
{    
   
    //objetosEnNivel.splice(index,1);    
    //console.log("PERO Q REMIERDPA PASA?!??  " + tipo);
   
    if(this.currentColor == 0)  // 0 rojo, 1 amarillo // 2 verde
    {
        if(this.esBolaBi(tipo))
        {
             aumentador = aumentador + 1;
             tocaSonido("comboRedB"); 
             vibrar(200);
             var azar = Math.floor(Math.random()*5);
             this.poneMSG(azar); 
            
             this.poneParticulas(true);
          
             bola.enabled = false; 
           //this.shaker.shake(1, 0.2);         
             score = score + (1+aumentador);
             var scoreTXT = _2dScreen.findByName("score"); 
             setTexto(scoreTXT.element,score);
             setTexto(this.P1.children[2].element,score);
             this.bolaScore(); 
             return(1); 
        }
        else  if(tipo == "R" || tipo == "CR" || tipo == "CRI" || tipo == "CRD" || tipo == "RI" || tipo == "RD")
        {              
           combo = combo + 1;
           if(combo > 12)
               combo = 12;
           tocaSonido("comboRed"+combo);            
            
           this.poneParticulas(true);
          
           bola.enabled = false; 
           //this.shaker.shake(1, 0.2);         
           score = score + (1+aumentador);
           var scoreTXT = _2dScreen.findByName("score"); 
           setTexto(scoreTXT.element,score);
           setTexto(this.P1.children[2].element,score);
           this.bolaScore(); 
           return(1); 
        }
        else
        {
            this.gameOver();
            return(2);        
        }
        
    }
    else if(this.currentColor == 1)  // 0 rojo, 1 amarillo // 2 verde
    {
        if(this.esBolaBi(tipo))
        {
             aumentador = aumentador + 1;
             vibrar(200);
             tocaSonido("comboYellowB"); 
             var azar = Math.floor(Math.random()*5);
             this.poneMSG(azar);
            
             this.poneParticulas(true);
          
             bola.enabled = false; 
           //this.shaker.shake(1, 0.2);         
             score = score + (1+aumentador);
             var scoreTXT = _2dScreen.findByName("score"); 
             setTexto(scoreTXT.element,score);
             setTexto(this.P1.children[2].element,score);
             this.bolaScore(); 
             return(1); 
        }
        else if(tipo == "A" || tipo == "CA" || tipo == "CAI" || tipo == "CAD" || tipo == "AI" || tipo == "AD")
        {           
          
           combo = combo + 1;
           if(combo > 12)
              combo = 12;
           tocaSonido("comboYellow"+combo); 
          
           this.poneParticulas(true);
          
           bola.enabled = false; 
           //this.shaker.shake(1, 0.2);
           score = score + (1+aumentador);
           var scoreTXT = _2dScreen.findByName("score");
           setTexto(scoreTXT.element,score);
           setTexto( this.P1.children[2].element,score);
           this.bolaScore(); 
           return(1);
        }
        else       
        {
            this.gameOver();
            return(2);
        }
        
    }
    else if(this.currentColor == 2)  // 0 rojo, 1 amarillo // 2 verde
    {
        if(this.esBolaBi(tipo))
        {
             aumentador = aumentador + 1;
             tocaSonido("comboGreenB"); 
             var azar = Math.floor(Math.random()*5);
             vibrar(200);
             this.poneMSG(azar);
            
             this.poneParticulas(true);
          
             bola.enabled = false; 
           //this.shaker.shake(1, 0.2);         
             score = score + (1+aumentador);
             var scoreTXT = _2dScreen.findByName("score"); 
             setTexto(scoreTXT.element,score);
             setTexto(this.P1.children[2].element,score);
             this.bolaScore(); 
             return(1); 
        }
        else if(tipo == "V" || tipo == "CV" || tipo == "CVI" || tipo == "CVD" || tipo == "VI" || tipo == "VD")
        {      
           
            combo = combo + 1;
            if(combo > 12)
                combo = 12;
            tocaSonido("comboGreen"+combo); 
           
           this.poneParticulas(true);
         
           bola.enabled = false;            
            //this.shaker.shake(1, 0.2);
           score = score + (1+aumentador);
           var scoreTXT = _2dScreen.findByName("score");
           setTexto(scoreTXT.element,score);
           setTexto( this.P1.children[2].element,score);
           this.bolaScore(); 
           return(1);
        }
        else       
        {
           this.gameOver();
           return(2);
        }
                
    }
    return(0);
  
};
ControlBola.prototype.poneMSG = function(tipo)
{
   var msg = this["msgs"+(this.indexMSG+1)];
   msg.script.animaTextoMsg.clickBoton(null,true,tipo);
   msg.enabled = true;
   msg.script.animaTextoMsg.pone(this.apagaMsg.bind(this,this.indexMSG));  
  
    
    this.elementosAlfaInv.push([msg.element,1,1.4]);    
    this.elementosAlfaInv[0][0].opacity = 1;    
    this.hiloAlfa= setInterval(this.callBackAlfa.bind(this,""),100);     
    
    this.indexMSG  =this.indexMSG + 1;
    if(this.indexMSG == 4)
      this.indexMSG = 0;
};
ControlBola.prototype.bolaScore = function()
{
    var str = "";
    str = str +  (1+aumentador);   
    var len = str.length;
    if(len > 4)
        len = 4;
    
    var scoreMCPOS = this.scoresMCpos.children[0];    
    var scoreMC = this.scoresMC.children[len - 1];
    scoreMC.setPosition(scoreMCPOS.getPosition().clone());
    var texto = (1+aumentador);
    //console.log(texto);
    if(texto > 9999)
        texto = 9999;
    str = "";
    str = str + texto;
    
    scoreMC.enabled = true;   
    for(var i = 0; i< str.length; i++)    
    {
        scoreMC.children[i].script.mensajeScore.poneTextura(str[i]);
        //console.log("q wea!! " + str[i]);
    }
    
    scoreMC.children[str.length].script.mensajeScore.poneTextura(0);
    
};
ControlBola.prototype.gameOver = function (e) 
{    
     vibrar(700);
    _sonidos.stop("musicaIngame");
    this.glow_up.enabled = false;
    this.glow_down.enabled = false;
    this.glow_left.enabled = false;
    this.glow_right.enabled = false;
    tocaSonido("muerte");
    gameStart = false;
    this.path.pathListo = false;   
    gameOver = true;
    this.shaker.shake(5, 1);
    this.mataTrail();
    setTimeout(this.explota.bind(this),800);
    
    //setTimeout(this.restart.bind(this),2500);  
};
ControlBola.prototype.finGamePlay = function()
{
    tocaSonido("start");
    gameStart = false;
    this.path.pathListo = false;   
    gameOver = true;
    this.mataTrail();
    this.panelGameOver();
};
ControlBola.prototype.explota = function()
{
    this.ondaMuerte.enabled = true;
    reproduceAnim(this.ondaMuerte,0,1,true,1500);
    this.poneParticulas(false);
    this.poneParticulas(false);
    this.poneParticulas(false);   
    this.sombra.enabled = false;
    this.bolaSkin.model.enabled = false;
    this.bolaPlayerStiker.model.enabled = false;
    setTimeout(this.panelGameOver.bind(this),1000);
};
ControlBola.prototype.panelGameOver = function()
{
    for(var i = 0; i < objetosEnNivel.length;i++)
    {
        if(objetosEnNivel[i][4] != null)
            objetosEnNivel[i][4].stop();
        objetosEnNivel[i][4] = null;      
    }
    if(withFinalHud)
    {
           var scoreGame = _2dScreen.findByName("score");
           scoreGame.enabled = false; 
           //this.camara.script.bloom.enabled = true; 
           var elementos_name = [["panelRevive"]];
           var botonRetry = _2dScreen.findByName("panelRevive").children[0];
           var botonGetGame = _2dScreen.findByName("panelRevive").children[1]; 
           //var botonContinue = _2dScreen.findByName("panelRevive").children[1]; 
           //var tiempo = _2dScreen.findByName("panelRevive").children[2];  
           //var tap = _2dScreen.findByName("tapToContinue");

           var road = _2dScreen.findByName("roadTextoEnd");
           var best = _2dScreen.findByName("bestScore");
           var scoreTXT = _2dScreen.findByName("scoreEnd");
          
               
           if(score > bestScore) 
               bestScore = score;
        
   
            guardaVars(bestScore,"bestScore");

           road.element.text = road_texto+" : "+(currentLevelCont+1);
           best.element.text = best_score_texto + bestScore;
           scoreTXT.element.text = score;
           if(score == 0)
               scoreTXT.element.text = "0";


           _2dScreen.findByName("panelRevive").element.enabled = false;
           //tap.element.enabled = false;
           //tiempo.element.enabled = false;
           //botonAd.element.enabled = false;
           //botonBuy.element.enabled = false;
           //if(!yaContinuo) 
           {
                botonRetry.enabled = true;
                //botonGetGame.enabled = true;
                //tiempo.script.animaTexturas.initialize();
                //tap.element.enabled = true;
                //tiempo.element.enabled = true;
                //tiempo.script.animaTexturas.inicioAnim(null,0,1);
                //botonAd.element.enabled = true;
                //botonBuy.element.enabled = true;
           }
           ejecutaElementosEntrada(elementos_name,null,this.poneEventosGameOver.bind(this));   
    }
    else
    {
        window.gamePlayEvents.showEndScreen((currentLevelCont+1),score);
    }
    
};
ControlBola.prototype.poneEventosGameOver = function()
{
   var botonRetry = _2dScreen.findByName("panelRevive").children[0];
   var botonGetGame = _2dScreen.findByName("panelRevive").children[1]; 
   //var botonContinue = _2dScreen.findByName("panelRevive").children[1]; 
   //var tiempo = _2dScreen.findByName("panelRevive").children[2];  
   //var tap = _2dScreen.findByName("tapToContinue");
   //tap.enabled = true;
   //tap.element.enabled = true;
   
   
   //if(!yaContinuo)
   {
        botonRetry.element.enabled = true;
        //botonGetGame.element.enabled = true;
        //botonBuy.element.enabled = true;
        _2dScreen.findByName("panelRevive").element.enabled = true;
        //tiempo.script.animaTexturas.inicioAnim(this.sacarEvento.bind(this,botonAd));   
        asignaEventos(botonRetry,true,1,this.clickContinue.bind(this)); 
        //asignaEventos(botonGetGame,true,1,this.clickGetGame.bind(this)); 
   }
   //asignaEventos(botonContinue,true,1,this.clickContinue.bind(this));   
};
ControlBola.prototype.sacarEvento = function(mc)
{
   asignaEventos(mc,false);    
};
ControlBola.prototype.sacaEventosGameOver = function()
{  
   var panelRevive = _2dScreen.findByName("panelRevive");
   var botonRetry = _2dScreen.findByName("panelRevive").children[1];
   var botonGetGame = _2dScreen.findByName("panelRevive").children[0]; 
   //var botonContinue = _2dScreen.findByName("panelRevive").children[1]; 
   //var tiempo = _2dScreen.findByName("panelRevive").children[2]; 
   //var tap = _2dScreen.findByName("tapToContinue");
   
   var miniMapa = _2dScreen.findByName("miniMapa");
   var bolasMapa = _2dScreen.findByName("bolasMapa");
    
   
   miniMapa.enabled = false;
   bolasMapa.enabled = false;
    
   //tap.enabled = false;
   //tiempo.script.animaTexturas.stopAnim();    
   panelRevive.enabled = false;
   asignaEventos(botonGetGame,false); 
   asignaEventos(botonRetry,false); 
   //asignaEventos(botonContinue,false);  
};
ControlBola.prototype.clickGetGame = function()
{
   //this.camara.script.sepia.enabled = false;
  // clickGetGame();
   /*this.sacaEventosGameOver();
   this.bolaSkin.script.parpadear.enabled = true;
   this.sombra.enabled = true;
   bolaParpadeando = true;   
   yaContinuo = true;
   combo = 0;
   gameStart = true;
   this.path.pathListo = true;   
   gameOver = false;
   setTimeout(this.terminaParpadeoBola.bind(this),tiempoParpadeo*1000);*/
  
};
ControlBola.prototype.terminaParpadeoBola = function()
{
    bolaParpadeando = false;
    this.bolaSkin.script.parpadear.enabled = false;
    this.bolaSkin.model.enabled = true;
    this.bolaPlayerStiker.model.enabled = true;
};
ControlBola.prototype.clickBuyLife = function()
{
    tocaSonido("click");
   
};
ControlBola.prototype.clickContinue = function()
{  
    //console.log("CLICK AL CONTINUE?!?!?!");
    tocaSonido("click");
    this.backMain();
};
ControlBola.prototype.backMain = function()
{
    tocaSonido("start");
    //window.gameplay.startLevel = road;
    this.sacaEventosGameOver();
    //this.camara.script.bloom.enabled = false;
    this.camara.script.brightnessContrast.enabled = true;    
    this.camara.script.brightnessContrast.anim(0.00001,-1,0,this.panelMain.bind(this));  
    iniVars();    
    this.iniVars();
    this.path.apagaPistas();
    this.path.start(false);
    this.sombra.enabled = false;
    this.camara.camera.clearColor = new pc.Color().fromString(window["pista"+currentLevel+"_niebla_color"]);
    //this.fog.script.cambiaTextura.cambio(currentLevel-1);
    
};
ControlBola.prototype.panelMain = function()
{     
   /*this.sombra.enabled = true;
   this.camara.script.brightnessContrast.enabled = false;
   this.clickStartb(); */
   var musicb = _2dScreen.findByName("musicb");
   var startb = _2dScreen.findByName("startb");
   var logo = _2dScreen.findByName("logo");
   var tap = _2dScreen.findByName("tapToStart");
   var barra = _2dScreen.findByName("barraSwipe");
   var mano = _2dScreen.findByName("mano");    
    
   barra.enabled = true;
   mano.enabled = true;
    
   mano.setPosition(0,mano.getPosition().y,0);
   mano.script.animaMsg.animaMano(0);
    
   musicb.enabled = true;
   startb.enabled = true;
   logo.enabled = true;
   tap.enabled = true;
   this.sombra.enabled = true;
   this.tutorialMC.enabled = true;
   this.camara.script.brightnessContrast.enabled = false;
   asignaEventos(musicb,true,1,this.clickSoundConfig.bind(this,"sound"));
   asignaEventos(startb,true,1,this.clickStartb.bind(this));
   this.checkSoundConfig();
      
};
ControlBola.prototype.clickSoundConfig = function(tipo)
{
  
    if(tipo == "sound")
    {
        if(muteSound == 0)        
            muteSound = 1;        
        else        
             muteSound = 0;
         _2dScreen.findByName("musicb").script.animaMsg.clickBoton(null,false,muteSound);
        
    }
    /*else  if(tipo == "music")
    {
        if(muteMusic == 0)        
            muteMusic = 1;        
        else        
             muteMusic = 0;
        
        setMusicas();
         _2dScreen.findByName("musicb").script.animaMsg.clickBoton(null,false,muteMusic);
        
    }*/
    tocaSonido("click");
   
};
ControlBola.prototype.checkSoundConfig = function(tipo)
{     
   _2dScreen.findByName("musicb").script.animaMsg.clickBoton(null,false,muteSound);
   //_2dScreen.findByName("musicOnOff").script.animaMsg.clickBoton(null,false,muteMusic); 
   
};
ControlBola.prototype.clickStartb = function()
{     
    if(this.conTutorial)
    {
         tocaSonido("click");
        this.tutorialMC.enabled = true;
        this.conTutorial = false;
        return;
    }
    this.tutorialMC.enabled = false;
    tocaSonido("click");
    this.sacaMain();
    this.empezarGame();
    this.bolaSombra.enabled = true;
    setTimeout(function () {
    tocaSonido("musicaIngame"); }.bind(this),350);
    this.glow_up.enabled = false;
    this.glow_down.enabled = false;
    this.glow_left.enabled = false;
    this.glow_right.enabled = false;
    
    
    this.P1.children[0].element.text = "0";
    this.P2.children[0].element.text = "0";
    var scoreTXT = _2dScreen.findByName("score"); 
    if(battle_mode)
    {
        this.P1.enabled = true;
        this.P2.enabled = true;
        scoreTXT.enabled = false;
    }
    else if(soloP1)
    {
        this.P1.enabled = true;
        scoreTXT.enabled = false;
    }
    else
    {
        this.P1.enabled = false;
        this.P2.enabled = false;
        scoreTXT.enabled = true;
    }
};
ControlBola.prototype.sacaMain = function()
{     
   var musicb = _2dScreen.findByName("musicb");
   var startb = _2dScreen.findByName("startb");
   var logo = _2dScreen.findByName("logo");
   var tap = _2dScreen.findByName("tapToStart");
    
   var barra = _2dScreen.findByName("barraSwipe");
   var mano = _2dScreen.findByName("mano");
    
   mano.script.animaMsg.detieneMano();
   barra.enabled = false;
   mano.enabled = false;
   
   asignaEventos(musicb,false);
   asignaEventos(startb,false);
   musicb.enabled = false;
   startb.enabled = false;
   logo.enabled = false;
   tap.enabled = false;   
      
};
ControlBola.prototype.empezarGame = function()
{     
    gameStart = true;    
    this.path.empezar();
};
ControlBola.prototype.continue = function()
{      
    iniVars();    
    this.iniVars();
    this.path.start(false);
};

// metodos touch, moiuse//
// 
ControlBola.prototype.onMouseDown = function (e) 
{  
  
 this.metodoTouchStart(e.x,e.y);     
  
};
ControlBola.prototype.onMouseMove = function (e) {    
    if(ControlBola.tap)    
       this.metodoTouchMove(e.x,e.y);
    
};
ControlBola.prototype.onMouseUp = function (e)
{    
  this.metodoTouchUp(e.x,e.y);
    
};
ControlBola.prototype.onTouchStart = function (event) {
    
     var touch = event.touches[0];
     this.metodoTouchStart(touch.x,touch.y);
   
};
ControlBola.prototype.onTouchMove = function (event) {
    
    if(ControlBola.tap)
    {
       var touch = event.touches[0];
       this.metodoTouchMove(touch.x,touch.y);        
    } 
    
};
ControlBola.prototype.onTouchEnd = function (event) {    
   
   //var touch = event.touches[0];
   this.metodoTouchUp();//touch.x,touch.y);
   
};

ControlBola.prototype.metodoTouchStart = function(x,y)
{ 
  if(gameOver)
      return;
    
  ControlBola.tap = true;
  clearInterval(this.hiloSwipe);
  this.clickInicial = new pc.Vec2(x, y);
  this.clickInicialFijo = new pc.Vec2(x, y);
  var position = this.bolaSkin.getPosition();
  this.bolaInicial = new pc.Vec3(position.x,position.y,position.z);
  this.bolaInicialFijo = new pc.Vec3(position.x,position.y,position.z);  
       
};
ControlBola.prototype.metodoTouchMove = function(x,y)
{
     this.puntoMouseMove = new pc.Vec2(x,y);
};

ControlBola.prototype.calculaMouseMove = function()
{     
        
        if(!ControlBola.tap)
            return;
    
         if(gameOver)
          return;
    
        if(!gameStart)
            return;
         
        if(this.clickInicialFijo == null)
            return;
         if(this.puntoMouseMove == null)
             return;
        var anchoReal = 540;
        var altoReal = 960;
               
        
        var bolaPos = this.bolaSkin.getPosition();
        var iniX = 0;        
        var iniY = 0;
        var finX = window.innerWidth;//this.app.graphicsDevice.width * window.devicePixelRatio;
        var finY = window.innerHeight;//this.app.graphicsDevice.height * window.devicePixelRatio;          
         
        var x = this.clickInicialFijo.x + (this.puntoMouseMove.x - this.clickInicialFijo.x);
        var y = this.clickInicialFijo.y + (this.puntoMouseMove.y - this.clickInicialFijo.y);
        if(!isMobile.any())
        {
             finX = this.app.graphicsDevice.width;
             finY = this.app.graphicsDevice.height;
        }
    
        if(finX > finY)
        {
            var aux = finX;
            finX = finY;
            finY = finX;
            aux = altoReal;
            altoReal = anchoReal;
            anchoReal = altoReal;
        }
    
        var escala = finY/altoReal;
        var x1 = velocidadSwipe*escala;
        var y1 = -pistaAncho/2;
        
        var x2 = finX - velocidadSwipe*escala;
        var y2 = pistaAncho/2;
        
        var M = (y2-y1)/(x2-x1);
        var Y = M*(x -x1) + y1;
      
        
        M = (y2-y1)/(x2-x1);
        var Ya = M*(this.clickInicialFijo.x -x1) + y1;      
        var realX = Y - Ya;
         realX = realX + this.bolaDistXAcomulada;
         if(realX < -pistaAncho/2)
             realX = -pistaAncho/2;
         else if(realX > pistaAncho/2)
             realX = pistaAncho/2;
          
        var posMov = this.entity.getPosition(); 
        this.bolaSkin.setLocalPosition(realX ,this.bolaY,0);
        this.camara.setLocalPosition(realX*0.5,this.camaraPartida.y,this.camaraPartida.z);
        this.cameraFake.setLocalPosition(realX*0.5,this.camaraPartida.y,this.camaraPartida.z);
        
        this.clickFinal = new pc.Vec2(x,y);    
        var position = this.entity.getPosition();    
        if(this.bolaInicial != null && position.z > this.bolaInicial.z)
          this.bolaInicial = new pc.Vec3(position.x,position.y,position.z);  
};
ControlBola.prototype.metodoTouchUp = function(x,y)
{
   ControlBola.tap = false;  
   this.clickFinal = null;
   this.clickInicial = null;
   this.clickInicialFijo = null;
   this.bolaInicialFijo = null;
   this.bolaInicial = null;  
   this.puntoMouseMove = null;
   this.bolaDistXAcomulada = this.bolaSkin.getLocalPosition().x;
   
};
ControlBola.prototype.haceTrail = function(dt)
{         
    if(gameOver)
        return;
    
    return;
    var pos = this.startTrail.getPosition();
    var rot = this.startTrail.getRotation();
      
    if(this.anterior != null)
    {
        
        var target = null;        
        if(this.trailIndex > 0)
            target = this.bolaSombra.children[this.trailIndex-1];
        else
             target = this.bolaSombra.children[this.trailTotal - 1];
              
        ////console.log("el trail total idnex " + this.trailIndex);
        var a = pos;
        var b = target.getPosition();
        var r1 = new pc.Vec3();
        var r2 = new pc.Vec3();
        var r3 = new pc.Vec3();
        var r4 = new pc.Vec3();
        var r5 = new pc.Vec3();
        var r6 = new pc.Vec3();
        var r7 = new pc.Vec3();
        var r8 = new pc.Vec3();
        var r9 = new pc.Vec3();
        var r10 = new pc.Vec3();
        var r11 = new pc.Vec3();
  
        r1.lerp(a, b, 1);
        r2.lerp(a, b, 0.75); 
        r3.lerp(a, b, 0.5); 
        r4.lerp(a, b, 0.25);
        r5.lerp(a, b, 0);
        //r6.lerp(a, b, 0);
        
        var arrayR = [r1,r2,r3,r4,r5];//,r7,r8,r9,r10,r11];
        
        for(var i = 0; i < arrayR.length; i++)
        {
        
            clearInterval(this.hiloTrail[this.trailIndex]);
            this.bolaSombra.children[this.trailIndex].setLocalScale(window.gameplay.trailSize,window.gameplay.trailSize,window.gameplay.trailSize);
            this.bolaSombra.children[this.trailIndex].enabled = true;       
            this.bolaSombra.children[this.trailIndex].setPosition(arrayR[i]);   
            this.bolaSombra.children[this.trailIndex].setRotation(rot);            
            //this.bolaSombra.children[this.trailIndex].rotateLocal(90,0,0);
           
            //this.hiloTrail[this.trailIndex] = setInterval(this.achicaTrail.bind(this,this.trailIndex),16);
            this.trailIndex = this.trailIndex + 1;
            if(this.trailIndex == this.trailTotal) 
            {
                this.anterior = true;
                this.trailIndex = 0; 
            }
        }        
      
    }
    else
    {
       clearInterval(this.hiloTrail[this.trailIndex]);
        this.bolaSombra.children[this.trailIndex].setLocalScale(window.gameplay.trailSize,window.gameplay.trailSize,window.gameplay.trailSize);
        this.bolaSombra.children[this.trailIndex].setPosition(pos.x,pos.y,pos.z);    
       // this.hiloTrail[this.trailIndex] = setInterval(this.achicaTrail.bind(this,this.trailIndex),16);
        this.trailIndex = this.trailIndex + 1;
         this.anterior = true;              
    }
    
    
           
    //this.bolaSombra.children[this.trailIndex].setPosition(pos.x,pos.y,pos.z);    
    //this.hiloTrail[this.trailIndex] = setInterval(this.alfaTrail.bind(this,this.trailIndex),30);
    /*this.trailIndex = this.trailIndex + 1;
    if(this.trailIndex == this.trailTotal)                    
       this.trailIndex = 0;                    */
      
};
ControlBola.prototype.alfaTrail = function(index)
{    
    var mc = this.bolaSombra.children[index];
    var meshInstances = mc.model.meshInstances;
    for (var i = 0; i < meshInstances.length; ++i) { 
        var mesh = meshInstances[i];
        var op = mesh.material.opacity;
        var rest = op - 0.05; 
        ////console.log("rest rq " + rest);
        if(rest <= 0)
        {
            rest = 0;
            //mesh.material.opacity = 1;          
           // mesh.material.update();
            mesh.enabled = false;
            clearInterval(this.hiloTrail[index]);
        }
        else
        {         
            //mesh.material.opacity = rest;          
            //mesh.material.update();
        }
    }        
};
ControlBola.prototype.achicaTrail = function(index)
{    
    var escala = this.bolaSombra.children[index].getLocalScale();
    if(escala.x -0.3 < 0)
    {
        this.bolaSombra.children[index].setLocalScale(0,0,0); 
        clearInterval(this.hiloTrail[index]);
    }
    else    
        this.bolaSombra.children[index].setLocalScale(escala.x -0.3,escala.y -0.3,escala.z -0.3);            
    
};
ControlBola.prototype.mataTrail = function()
{
    //////////console.log("mate la wea ");
    for(var i = 0; i < this.trailTotal;i++)
    {
        clearInterval(this.hiloTrail[i]);
        this.bolaSombra.children[i].enabled = false;
        this.trailIndex = 0;
        this.bolaSombra.children[i].setLocalPosition(0,0,0);
        this.bolaSombra.children[i].setLocalScale(window.gameplay.trailSize,window.gameplay.trailSize,window.gameplay.trailSize);
        
    }
    
};

ControlBola.prototype.callBackAlfa = function(tipo,params)
{
    ////console.log(this.elementosAlfa.length);
    if(this.elementosAlfa.length == 0 && this.elementosAlfaInv.length == 0)
    {
        clearInterval (this.hiloAlfa);
        
    }
   
};