var Shake = pc.createScript('shake');  
Shake.attributes.add('bola', {type: 'entity', title: 'bola'});
Shake.attributes.add('cameraFake', {type: 'entity', title: 'cameraFake'});
    Shake.prototype.initialize = function() 
    {        
        this.timer = 0;
        this.shaking = false;
        this.amplitude = 1;
        this.duration = 0;
        this.frequency = 10;
        
        this.x = 0;
        this.y = 0;
        this.z = 0;
        
        this.offset = new pc.Vec3();
        this.position = new pc.Vec3();
    };

    Shake.prototype.reset = function () {
            this.shaking = false;
            this.offset.set(0, 0, 0);             
            this.entity.setLocalPosition(this.position);
            console.log("mi x local " + this.entity.getLocalPosition().x);
    };        
    Shake.prototype.shake = function (amplitude, duration)
    {
            if (this.shaking) {
                return;
            }
            this.amplitude = amplitude;
            this.duration = duration;
            this.timer = 0;            

            this.x = Math.random();
            this.y = Math.random();
            this.z = Math.random();
            this.lastTargetPosition = this.entity.getPosition().clone();
            this.position.copy(this.entity.getLocalPosition());
            this.shaking = true;
    };
    Shake.prototype.update = function (dt) 
    {
        if(this.shaking)
        {
           
            var desiredPosition = new pc.Vec3();
            var lastTargetPosition =  this.cameraFake.getPosition().clone();
            desiredPosition.copy(lastTargetPosition);   
            var shake = this.getShake(dt);
            if(!shake)
                return;
            desiredPosition.add(shake);
            this.entity.setPosition(desiredPosition);
        }
    };
    Shake.prototype.getShake = function (dt) 
    {
            if (this.shaking) {
                
                var f = 1 - (this.timer / this.duration);

                this.offset.x = this.getNoise(this.x) * this.amplitude * f;
                this.offset.y = this.getNoise(this.y) * this.amplitude * f;
                this.offset.z = this.getNoise(this.z) * this.amplitude * f;

                this.timer += dt;
                if (this.timer > this.duration) {
                    this.reset();
                    return(false);
                }

                var f = this.frequency*dt;
                this.x += f;
                this.y += f;
                this.z += f;
            } else {
                this.offset.set(0,0,0);
            }

            return this.offset;
     };

     Shake.prototype.fade = function (t) 
     {
         return t * t * t * (t * (t * 6 - 15) + 10);
     };

     Shake.prototype.lerp = function (t, a, b) 
     {
         return a + t * (b - a);
     };

     Shake.prototype.getNoise = function (x)
     {    
            var i = Math.floor(x);
            x = x - i;
            var u = this.fade(x);
            
            var a = (perm[i] & 1) ? x : -x;
            var b = (perm[i+1] & 1) ? x - 1.0 : 1 - x;
            
            return this.lerp(u, a, b);
     };

   
