var MensajeScore = pc.createScript('mensajeScore');
MensajeScore.attributes.add("texturas", {type: "asset", assetType: "texture", array: true, title: "texturas"});
MensajeScore.attributes.add('material', { type: 'asset', assetType: 'material', array: true });

// initialize code called once per entity
MensajeScore.prototype.initialize = function() {
    this.tween = null;
    var pos = this.entity.getLocalPosition();
    this.posicionInicial = new pc.Vec3(pos.x,pos.y,pos.z);
    this.entity.setLocalPosition(100,100,100);
    this.alfaCont = 1;
    this.contStartAlfa = 0;
    
    var materialDifuso = this.material[0].resource; 
     var textureDifuse = this.texturas[0].resource; 
    
     materialDifuso.diffuseMap = textureDifuse;
     materialDifuso.opacityMap = textureDifuse;
     materialDifuso.emissiveMap = textureDifuse;
     materialDifuso.opacity = 1;
     materialDifuso.update();     
};

MensajeScore.prototype.poneTextura = function(num,doble,mc) {
         
     if(this.tween !=  null)
         this.tween.stop();
    
     
     this.entity.enabled = true;
     var materialDifuso = this.material[0].resource; 
     var textureDifuse = this.texturas[num].resource; 
     this.entity.setLocalPosition(this.posicionInicial.x,this.posicionInicial.y,this.posicionInicial.z);    
     var extra = 1;
     if(doble != null)
         extra = doble;
    
     materialDifuso.diffuseMap = textureDifuse;
     materialDifuso.opacityMap = textureDifuse;
     materialDifuso.emissiveMap = textureDifuse;
     materialDifuso.opacity = 1;
     this.alfaCont = 1;
     this.contStartAlfa = 0;
     materialDifuso.update();        
     var posicion = this.entity.getLocalPosition();
      this.tween = this.entity.tween(posicion)
        .to(new pc.Vec3(posicion.x, posicion.y+ 1.8*extra, posicion.z), 1.6, pc.QuadraticOut)
        .on('update',this.controlaAlpha.bind(this,materialDifuso)) 
        .on('complete',this.escondeMensaje.bind(this)) 
        .start(); 
    
};
MensajeScore.prototype.controlaAlpha = function(materialDifuso) {
    this.contStartAlfa = this.contStartAlfa + 1;
    
    if(this.contStartAlfa >= 12)
    {
        this.alfaCont = this.alfaCont - 0.027;
        if(this.alfaCont < 0)
            this.alfaCont = 0;
        materialDifuso.opacity = this.alfaCont;
        materialDifuso.update(); 
        //console.log("Q CTM PASA PO!!");
    }
};
MensajeScore.prototype.escondeMensaje = function(materialDifuso) {
    
   this.entity.parent.enabled = false;
};
