var AnimaUvs = pc.createScript('animaUvs');
AnimaUvs.attributes.add('material', { type: 'asset', assetType: 'material', array: false });
AnimaUvs.attributes.add('speed', {type: 'number', default:0.01,title: 'speed'});
AnimaUvs.attributes.add('eje', {type: 'number', default:0,title: 'ejeX_o_Y'});
AnimaUvs.attributes.add('loop', {type: 'number', default:1,title: 'LOOP'});
AnimaUvs.attributes.add('desde', {type: 'number', default:0,title: 'desde'});
AnimaUvs.attributes.add('hasta', {type: 'number', default:1,title: 'hasta'});
AnimaUvs.attributes.add('tipoMaterial', {type: 'string', default:"opacityMapOffset",title: 'opacityMapOffset'});
AnimaUvs.attributes.add('apagar', {type: 'number', default:1,title: 'apagar'});


// initialize code called once per entity
AnimaUvs.prototype.initialize = function() {
      
    this.materialCargado = this.material.resource;
    //console.log(this.materialCargado);
};
AnimaUvs.prototype.resetMaterial = function(eje) {
    
        this.materialCargado[this.tipoMaterial] = new pc.Vec2(0,0);    
        this.materialCargado.update();  
        this.entity.enabled = true;  
        this.eje = eje;
   
};
AnimaUvs.prototype.setAnim = function(eje,desde,hasta) {
    
    this.desde = desde;
    this.hasta = hasta;
   
    if(eje == 1)
    {
        this.materialCargado[this.tipoMaterial] = new pc.Vec2(desde,0);    
     
    }
    else
    {
        this.materialCargado[this.tipoMaterial] = new pc.Vec2(0,desde);    
        
    }
    this.materialCargado.update();  
    this.entity.enabled = true;  
    this.eje = eje;
   
};


// update code called every frame
AnimaUvs.prototype.update = function(dt) {    
    
    var vector;
    switch(this.tipoMaterial)
    {
        case  "opacityMapOffset":
             vector =  this.materialCargado.opacityMapOffset;  
        break;
        case "diffuseMapOffset":
             vector =  this.materialCargado.diffuseMapOffset;  
        break;
        case "emissiveMapOffset":
            vector =  this.materialCargado.emissiveMapOffset;  
        break;
        
    }   
    if(this.eje == 0)
    {
        
        var valor = vector.y;
        var delta = valor - dt*this.speed;
        if(delta < 0)
        {
            if(this.loop == 0)
            {
                delta = 0;
                this.eje = -99;     
            }
            else
                delta = 1;
        }
        this.materialCargado[this.tipoMaterial] = new pc.Vec2(0,delta);    
        this.materialCargado.update();
    }
    else if(this.eje == 1)
    {         
        var valor = vector.x;
        var delta = valor + dt*this.speed;
        if(delta > this.hasta)
        {
            if(this.loop == 0)
            {
                delta = this.hasta;
                this.eje = -99;  
                if(this.apagar == 1)
                    setTimeout(this.apagaObjeto.bind(this),20);
            }
            else
                delta = this.desde;
        }
        //console.log("updateare el material " + delta);
       
        this.materialCargado[this.tipoMaterial] = new pc.Vec2(delta,0);    
        this.materialCargado.update();       
            
    }
};
AnimaUvs.prototype.apagaObjeto = function() {    
  this.entity.enabled = false;  
};
