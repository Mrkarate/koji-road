/**
 * Created by MrKarate on 04-12-15.
 */

var Physics = (function() {

    var app = pc.Application.getApplication();

    function cast(from, to) {
        var ret = null;
        app.systems.rigidbody.raycastFirst(from.clone(), to.clone(), function (result) {
            ret = result;
        });
        return ret;
    }

    function makeArray(item) {

        if(Array.isArray(item)) return item;
        if(item == undefined) return item;
        if(typeof item == 'string') return item.split(',').map(function(s) { return s.trim(); });

        return item;
    }

    var that;
    return that = {
        raycast: function (start, end, options) {

            start = start.clone();
            options = options || {};
            var results = [];
            var regex = options.match ? new RegExp(makeArray(options.match).join('|'), 'i') : null;
            var exclude = options.exclude ? new RegExp(makeArray(options.exclude).join('|'), 'i') : null;
            var result;
            do {
                result = cast(start, end);
                if(result) {
                    var match = true;
                    if (regex) {
                        match = result.entity.name.match(regex);
                    }
                    if (match && options.name) {
                        options.name = makeArray(options.name);
                        match = options.name.indexOf(result.entity.name);
                    }
                    if(match && exclude) {
                        match = !result.entity.name.match(exclude);
                    }
                    if (match && options.filter) {
                        match = options.filter(result.entity, match);
                    }
                    if (match) {
                        results.push(result);
                        if(options.max !== undefined) {
                            options.max--;
                            if(options.max <= 0) {
                                return results;
                            }
                        }
                    }
                    start = result.point.clone();
                    start.add(end.clone().sub(start).normalize().scale(0.0001));
                }

            } while(result);
            return results;
        },
        raycastPiso: function (start, end) {

            var ret = null;          
            app.systems.rigidbody.raycastFirst(start, end, function (result) {
                ret = result;
                console.log(ret);
            });
            return ret;
        },
        raycastDirection: function (start, direction, length, options) {
            var end = start.clone();
            end.add(direction.clone().normalize().scale(length));
            return that.raycast(start, end, options);
        }

    }

})();