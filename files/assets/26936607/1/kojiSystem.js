// texturas element, para 2d

var mano_textura = "https://www.bekhoteam.net/proyectos/koji/manoswipe.png";
var swipe_bar_textura = "https://www.bekhoteam.net/proyectos/koji/barraswipe.png";
var soundON_textura = "https://www.bekhoteam.net/proyectos/koji/sound-on.png";
var soundOFF_textura = "https://www.bekhoteam.net/proyectos/koji/sound-mute.png";


//textos y colores de textos
//
var tapToStart_texto = "-TAP TO START-";
var tapToContinue_texto = "-TAP TO CONTINUE-";
var road_texto = "ROAD";
var tapToStart_text_col = "#FFFFFF";
var tapToContinue_text_col = "#FFFFFF";
var road_text_col = "#CA00FF";
var instructions_texto = "INSTRUCTIONS";
var instructions_desc1_texto = "SWIPE OR USE ARROW KEYS TO MOVE THE BALL";
var instructions_desc2_texto = "COLLECT SAME COLORED BALLS AND AVOID OTHERS";
var instructions_desc3_texto = "COLLECT STARS FOR BONUS POINTS";
var instructions_text_col = "#15FF00";
var instructions_desc1_text_col = "#FFFFFF";
var instructions_desc2_text_col = "#FFFFFF";
var instructions_desc3_text_col = "#FFFFFF";
var logo1_texto = "COLOUR";
var logo2_texto = "CHASE";
var logo1_text_col = "#1EFBE9";
var logo2_text_col = "#FF23E4";

var msg1_texto = "amazing";
var msg2_texto = "good";
var msg3_texto = "incredible";
var msg4_texto = "terrific";
var msg5_texto = "wonderful";
var msg6_texto = "miss";
var msg_text_col =  "#FFFFFF";

var total_score_texto = "total score";
var total_score_text_col = "#FFFFFF";
var best_score_texto = "best :";
var best_score_text_col = "#FFFFFF";
var score_end_text_col = "#FFFF27";
var score_text_col = "#101010";


// musica
var main_music = "https://www.bekhoteam.net/proyectos/koji/tropical-paradise_by_theevs_Artlist.mp3";



//texturas y colores para materiales
var bola_sticker1_textura = "https://www.bekhoteam.net/proyectos/koji/sticker-morada.png";  //rojo
var bola_sticker2_textura = "https://www.bekhoteam.net/proyectos/koji/sticker-celeste.png";  // amarillo
var bola_sticker3_textura = "https://www.bekhoteam.net/proyectos/koji/sticker-verde.png"; // verde

var bola_color1 = "#FF00E4";
var bola_color2 = "#00FEFF";
var bola_color3 = "#06FF00";

var bolaBI_textura = "https://www.bekhoteam.net/proyectos/koji/moneda-star.png"; // verde

var rampa1_textura = "https://www.bekhoteam.net/proyectos/koji/ramparosa.png"; 
var rampa2_textura = "https://www.bekhoteam.net/proyectos/koji/rampaceleste.png"; 
var rampa3_textura = "https://www.bekhoteam.net/proyectos/koji/rampaverde.png"; 


var pista1_textura = "https://www.bekhoteam.net/proyectos/koji/pista.png";
var pista1_color = "#8A0097";
var pista1_borde_color = "#0B3473";
var pista1_edificios1_textura = "https://www.bekhoteam.net/proyectos/koji/edif1.png";
var pista1_edificios1_color = "#8A0097";
var pista1_edificios2_textura = "https://www.bekhoteam.net/proyectos/koji/edif8.png";
var pista1_edificios2_color = "#7E3A40";
var pista1_edificios3_textura = "https://www.bekhoteam.net/proyectos/koji/edifnara.png";
var pista1_edificios3_color = "#A0BA3D";
var pista1_edificiosCartel1_textura = "https://www.bekhoteam.net/proyectos/koji/posterbko.png";
var pista1_edificiosCartel2_textura = "https://www.bekhoteam.net/proyectos/koji/poster-vacio.png";
var pista1_edificiosCartel3_textura = "https://www.bekhoteam.net/proyectos/koji/posterkoji.png";
var pista1_rampaFinal_textura = "https://www.bekhoteam.net/proyectos/koji/rampa-pasanivel.png";
var pista1_aroCheckpoint_color = "#00FF40";
var pista1_niebla_color = "#dffdff";

var pista2_textura = "https://www.bekhoteam.net/proyectos/koji/pista.png";
var pista2_color = "#8A0097";
var pista2_borde_color = "#0B3473";
var pista2_edificios1_textura = "https://www.bekhoteam.net/proyectos/koji/edif1.png";
var pista2_edificios1_color = "#8A0097";
var pista2_edificios2_textura = "https://www.bekhoteam.net/proyectos/koji/edif8.png";
var pista2_edificios2_color = "#7E3A40";
var pista2_edificios3_textura = "https://www.bekhoteam.net/proyectos/koji/edifnara.png";
var pista2_edificios3_color = "#A0BA3D";
var pista2_edificiosCartel1_textura = "https://www.bekhoteam.net/proyectos/koji/posterbko.png";
var pista2_edificiosCartel2_textura = "https://www.bekhoteam.net/proyectos/koji/poster-vacio.png";
var pista2_edificiosCartel3_textura = "https://www.bekhoteam.net/proyectos/koji/posterkoji.png";
var pista2_rampaFinal_textura = "https://www.bekhoteam.net/proyectos/koji/rampa-pasanivel.png";
var pista2_aroCheckpoint_color = "#00FF40";
var pista2_niebla_color = "#dffdff";

var pista3_textura = "https://www.bekhoteam.net/proyectos/koji/pista.png";
var pista3_color = "#8A0097";
var pista3_borde_color = "#0B3473";
var pista3_edificios1_textura = "https://www.bekhoteam.net/proyectos/koji/edif1.png";
var pista3_edificios1_color = "#8A0097";
var pista3_edificios2_textura = "https://www.bekhoteam.net/proyectos/koji/edif8.png";
var pista3_edificios2_color = "#7E3A40";
var pista3_edificios3_textura = "https://www.bekhoteam.net/proyectos/koji/edifnara.png";
var pista3_edificios3_color = "#A0BA3D";
var pista3_edificiosCartel1_textura = "https://www.bekhoteam.net/proyectos/koji/posterbko.png";
var pista3_edificiosCartel2_textura = "https://www.bekhoteam.net/proyectos/koji/poster-vacio.png";
var pista3_edificiosCartel3_textura = "https://www.bekhoteam.net/proyectos/koji/posterkoji.png";
var pista3_rampaFinal_textura = "https://www.bekhoteam.net/proyectos/koji/rampa-pasanivel.png";
var pista3_aroCheckpoint_color = "#00FF40";
var pista3_niebla_color = "#ffe0de";

var pista4_textura = "https://www.bekhoteam.net/proyectos/koji/pista.png";
var pista4_color = "#8A0097";
var pista4_borde_color = "#0B3473";
var pista4_edificios1_textura = "https://www.bekhoteam.net/proyectos/koji/edif1.png";
var pista4_edificios1_color = "#8A0097";
var pista4_edificios2_textura = "https://www.bekhoteam.net/proyectos/koji/edif8.png";
var pista4_edificios2_color = "#7E3A40";
var pista4_edificios3_textura = "https://www.bekhoteam.net/proyectos/koji/edifnara.png";
var pista4_edificios3_color = "#A0BA3D";
var pista4_edificiosCartel1_textura = "https://www.bekhoteam.net/proyectos/koji/posterbko.png";
var pista4_edificiosCartel2_textura = "https://www.bekhoteam.net/proyectos/koji/poster-vacio.png";
var pista4_edificiosCartel3_textura = "https://www.bekhoteam.net/proyectos/koji/posterkoji.png";
var pista4_rampaFinal_textura = "https://www.bekhoteam.net/proyectos/koji/rampa-pasanivel.png";
var pista4_aroCheckpoint_color = "#00FF40";
var pista4_niebla_color = "#dffdff";

var pista5_textura = "https://www.bekhoteam.net/proyectos/koji/pista.png";
var pista5_color = "#8A0097";
var pista5_borde_color = "#0B3473";
var pista5_edificios1_textura = "https://www.bekhoteam.net/proyectos/koji/edif1.png";
var pista5_edificios1_color = "#8A0097";
var pista5_edificios2_textura = "https://www.bekhoteam.net/proyectos/koji/edif8.png";
var pista5_edificios2_color = "#7E3A40";
var pista5_edificios3_textura = "https://www.bekhoteam.net/proyectos/koji/edifnara.png";
var pista5_edificios3_color = "#A0BA3D";
var pista5_edificiosCartel1_textura = "https://www.bekhoteam.net/proyectos/koji/posterbko.png";
var pista5_edificiosCartel2_textura = "https://www.bekhoteam.net/proyectos/koji/poster-vacio.png";
var pista5_edificiosCartel3_textura = "https://www.bekhoteam.net/proyectos/koji/posterkoji.png";
var pista5_rampaFinal_textura = "https://www.bekhoteam.net/proyectos/koji/rampa-pasanivel.png";
var pista5_aroCheckpoint_color = "#00FF40";
var pista5_niebla_color = "#dffdff";

var pista6_textura = "https://www.bekhoteam.net/proyectos/koji/pista.png";
var pista6_color = "#8A0097";
var pista6_borde_color = "#0B3473";
var pista6_edificios1_textura = "https://www.bekhoteam.net/proyectos/koji/edif1.png";
var pista6_edificios1_color = "#8A0097";
var pista6_edificios2_textura = "https://www.bekhoteam.net/proyectos/koji/edif8.png";
var pista6_edificios2_color = "#7E3A40";
var pista6_edificios3_textura = "https://www.bekhoteam.net/proyectos/koji/edifnara.png";
var pista6_edificios3_color = "#A0BA3D";
var pista6_edificiosCartel1_textura = "https://www.bekhoteam.net/proyectos/koji/posterbko.png";
var pista6_edificiosCartel2_textura = "https://www.bekhoteam.net/proyectos/koji/poster-vacio.png";
var pista6_edificiosCartel3_textura = "https://www.bekhoteam.net/proyectos/koji/posterkoji.png";
var pista6_rampaFinal_textura = "https://www.bekhoteam.net/proyectos/koji/rampa-pasanivel.png";
var pista6_aroCheckpoint_color = "#00FF40";
var pista6_niebla_color = "#dffdff";

var pista7_textura = "https://www.bekhoteam.net/proyectos/koji/pista.png";
var pista7_color = "#8A0097";
var pista7_borde_color = "#0B3473";
var pista7_edificios1_textura = "https://www.bekhoteam.net/proyectos/koji/edif1.png";
var pista7_edificios1_color = "#8A0097";
var pista7_edificios2_textura = "https://www.bekhoteam.net/proyectos/koji/edif8.png";
var pista7_edificios2_color = "#7E3A40";
var pista7_edificios3_textura = "https://www.bekhoteam.net/proyectos/koji/edifnara.png";
var pista7_edificios3_color = "#A0BA3D";
var pista7_edificiosCartel1_textura = "https://www.bekhoteam.net/proyectos/koji/posterbko.png";
var pista7_edificiosCartel2_textura = "https://www.bekhoteam.net/proyectos/koji/poster-vacio.png";
var pista7_edificiosCartel3_textura = "https://www.bekhoteam.net/proyectos/koji/posterkoji.png";
var pista7_rampaFinal_textura = "https://www.bekhoteam.net/proyectos/koji/rampa-pasanivel.png";
var pista7_aroCheckpoint_color = "#00FF40";
var pista7_niebla_color = "#dffdff";

var pista8_textura = "https://www.bekhoteam.net/proyectos/koji/pista.png";
var pista8_color = "#8A0097";
var pista8_borde_color = "#0B3473";
var pista8_edificios1_textura = "https://www.bekhoteam.net/proyectos/koji/edif1.png";
var pista8_edificios1_color = "#8A0097";
var pista8_edificios2_textura = "https://www.bekhoteam.net/proyectos/koji/edif8.png";
var pista8_edificios2_color = "#7E3A40";
var pista8_edificios3_textura = "https://www.bekhoteam.net/proyectos/koji/edifneg.png";
var pista8_edificios3_color = "#A0BA3D";
var pista8_edificiosCartel1_textura = "https://www.bekhoteam.net/proyectos/koji/posterbko.png";
var pista8_edificiosCartel2_textura = "https://www.bekhoteam.net/proyectos/koji/poster-vacio.png";
var pista8_edificiosCartel3_textura = "https://www.bekhoteam.net/proyectos/koji/posterkoji.png";
var pista8_rampaFinal_textura = "https://www.bekhoteam.net/proyectos/koji/rampa-pasanivel.png";
var pista8_aroCheckpoint_color = "#00FF40";
var pista8_niebla_color = "#dffdff";


var infoKojiHTML = null;
var interval = window.setInterval(function(){
    console.log("LISTENING...");
    var info = document.getElementById("key1").innerHTML;
    if(info != infoKojiHTML){
        console.log("CAMBIO INFORMACION DEL JSON, ACTUALIZANDO VARIABLES");
        infoKojiHTML = info;
        var json = JSON.parse(infoKojiHTML);
        
        var gameAssets = json.Game_Assets;
        bola_color1 = gameAssets.bola_color1;
        bola_sticker1_textura = gameAssets.bola_sticker1_textura;
        bola_color2 = gameAssets.bola_color2;
        bola_sticker2_textura = gameAssets.bola_sticker2_textura;
        bola_color3 = gameAssets.bola_color3;
        bola_sticker3_textura = gameAssets.bola_sticker3_textura;
        rampa1_textura = gameAssets.rampa1_textura;
        rampa2_textura = gameAssets.rampa2_textura;
        rampa3_textura = gameAssets.rampa3_textura;

        var gamaTextEndPanelConfig = json.Game_Text_Playing_Config;
        total_score_texto = gamaTextEndPanelConfig.total_score_texto;
        total_score_text_col = gamaTextEndPanelConfig.total_score_text_col;
        best_score_texto = gamaTextEndPanelConfig.best_score_texto;
        best_score_text_col = gamaTextEndPanelConfig.best_score_text_col;
        score_end_text_col = gamaTextEndPanelConfig.score_end_text_col;
        tapToContinue_texto = gamaTextEndPanelConfig.tapToContinue_texto;
        tapToContinue_text_col = gamaTextEndPanelConfig.tapToContinue_text_col;

        var gameTextPlayingConfig = json.Game_Text_Playing_Config;
        road_texto = gameTextPlayingConfig.road_texto;
        road_text_col = gameTextPlayingConfig.road_text_col;
        score_text_col = gameTextPlayingConfig.score_text_col;
        msg1_texto = gameTextPlayingConfig.msg1_texto;
        msg2_texto = gameTextPlayingConfig.msg2_texto;
        msg3_texto = gameTextPlayingConfig.msg3_texto;
        msg4_texto = gameTextPlayingConfig.msg4texto;
        msg5_texto = gameTextPlayingConfig.msg5_texto;
        msg6_texto = gameTextPlayingConfig.msg6_texto;
        msg_text_col = gameTextPlayingConfig.msg_text_col;

        var gameTextTitleConfig = json.Game_Text_Title_Config;
        logo1_texto = gameTextTitleConfig.logo1_texto;
        logo1_text_col = gameTextTitleConfig.logo1_text_col;
        logo2_texto = gameTextTitleConfig.logo2_texto;
        logo2_text_col = gameTextTitleConfig.logo2_text_col;
        instructions_texto = gameTextTitleConfig.instructions_texto;
        instructions_text_col = gameTextTitleConfig.instructions_text_col;
        instructions_desc1_texto = gameTextTitleConfig.instructions_desc1_texto;
        instructions_desc1_text_col = gameTextTitleConfig.instructions_desc1_text_col;
        instructions_desc2_texto = gameTextTitleConfig.instructions_desc2_texto;
        instructions_desc2_text_col = gameTextTitleConfig.instructions_desc2_text_col;
        instructions_desc3_texto = gameTextTitleConfig.instructions_desc3_texto;
        instructions_desc3_text_col = gameTextTitleConfig.instructions_desc3_text_col;
        tapToStart_texto = gameTextTitleConfig.tapToStart_texto;
        tapToStart_text_col = gameTextTitleConfig.tapToStart_text_col;

        var gameUITitleImages = json.Game_UI_Title_images;
        soundON_textura = gameUITitleImages.soundON_textura;
        soundOFF_textura = gameUITitleImages.soundOFF_textura;
        mano_textura = gameUITitleImages.mano_textura;
        swipe_bar_textura = gameUITitleImages.swipe_bar_textura;

        var sounds = json.Sounds;
        main_music = sounds.main_music;
        
        var track1Customization = json.Track_1_customization;
        pista1_textura = track1Customization.pista1_textura;
        pista1_color = track1Customization.pista1_color;
        pista1_borde_color = track1Customization.pista1_borde_color;
        pista1_edificios1_textura = track1Customization.pista1_edificios1_textura;
        pista1_edificios1_color = track1Customization.pista1_edificios1_color;
        pista1_edificiosCartel1_textura = track1Customization.pista1_edificiosCartel1_textura;
        pista1_edificios2_textura = track1Customization.pista1_edificios2_textura;
        pista1_edificios2_color = track1Customization.pista1_edificios2_color;
        pista1_edificiosCartel2_textura = track1Customization.pista1_edificiosCartel2_textura;
        pista1_edificios3_textura = track1Customization.pista1_edificios3_textura;
        pista1_edificios3_color = track1Customization.pista1_edificios3_color;
        pista1_edificiosCartel3_textura = track1Customization.pista1_edificiosCartel3_textura;
        pista1_niebla_color = track1Customization.pista1_niebla_color;
        pista1_aroCheckpoint_color = track1Customization.pista1_aroCheckpoint_color;
        pista1_rampaFinal_textura = track1Customization.pista1_rampaFinal_textura;

        var track2Customization = json.Track_2_customization;
        pista2_textura = track2Customization.pista2_textura;
        pista2_color = track2Customization.pista2_color;
        pista2_borde_color = track2Customization.pista2_borde_color;
        pista2_edificios1_textura = track2Customization.pista2_edificios1_textura;
        pista2_edificios1_color = track2Customization.pista2_edificios1_color;
        pista2_edificiosCartel1_textura = track2Customization.pista2_edificiosCartel1_textura;
        pista2_edificios2_textura = track2Customization.pista2_edificios2_textura;
        pista2_edificios2_color = track2Customization.pista2_edificios2_color;
        pista2_edificiosCartel2_textura = track2Customization.pista2_edificiosCartel2_textura;
        pista2_edificios3_textura = track2Customization.pista2_edificios3_textura;
        pista2_edificios3_color = track2Customization.pista2_edificios3_color;
        pista2_edificiosCartel3_textura = track2Customization.pista2_edificiosCartel3_textura;
        pista2_niebla_color = track2Customization.pista2_niebla_color;
        pista2_aroCheckpoint_color = track2Customization.pista2_aroCheckpoint_color;
        pista2_rampaFinal_textura = track2Customization.pista2_rampaFinal_textura;
        
        var track3Customization = json.Track_3_customization;
        pista3_textura = track3Customization.pista3_textura;
        pista3_color = track3Customization.pista3_color;
        pista3_borde_color = track3Customization.pista3_borde_color;
        pista3_edificios1_textura = track3Customization.pista3_edificios1_textura;
        pista3_edificios1_color = track3Customization.pista3_edificios1_color;
        pista3_edificiosCartel1_textura = track3Customization.pista3_edificiosCartel1_textura;
        pista3_edificios2_textura = track3Customization.pista3_edificios2_textura;
        pista3_edificios2_color = track3Customization.pista3_edificios2_color;
        pista3_edificiosCartel2_textura = track3Customization.pista3_edificiosCartel2_textura;
        pista3_edificios3_textura = track3Customization.pista3_edificios3_textura;
        pista3_edificios3_color = track3Customization.pista3_edificios3_color;
        pista3_edificiosCartel3_textura = track3Customization.pista3_edificiosCartel3_textura;
        pista3_niebla_color = track3Customization.pista3_niebla_color;
        pista3_aroCheckpoint_color = track3Customization.pista3_aroCheckpoint_color;
        pista3_rampaFinal_textura = track3Customization.pista3_rampaFinal_textura;
        
        var track4Customization = json.Track_4_customization;
        pista4_textura = track4Customization.pista4_textura;
        pista4_color = track4Customization.pista4_color;
        pista4_borde_color = track4Customization.pista4_borde_color;
        pista4_edificios1_textura = track4Customization.pista4_edificios1_textura;
        pista4_edificios1_color = track4Customization.pista4_edificios1_color;
        pista4_edificiosCartel1_textura = track4Customization.pista4_edificiosCartel1_textura;
        pista4_edificios2_textura = track4Customization.pista4_edificios2_textura;
        pista4_edificios2_color = track4Customization.pista4_edificios2_color;
        pista4_edificiosCartel2_textura = track4Customization.pista4_edificiosCartel2_textura;
        pista4_edificios3_textura = track4Customization.pista4_edificios3_textura;
        pista4_edificios3_color = track4Customization.pista4_edificios3_color;
        pista4_edificiosCartel3_textura = track4Customization.pista4_edificiosCartel3_textura;
        pista4_niebla_color = track4Customization.pista4_niebla_color;
        pista4_aroCheckpoint_color = track4Customization.pista4_aroCheckpoint_color;
        pista4_rampaFinal_textura = track4Customization.pista4_rampaFinal_textura;
        
        var track5Customization = json.Track_5_customization;
        pista5_textura = track5Customization.pista5_textura;
        pista5_color = track5Customization.pista5_color;
        pista5_borde_color = track5Customization.pista5_borde_color;
        pista5_edificios1_textura = track5Customization.pista5_edificios1_textura;
        pista5_edificios1_color = track5Customization.pista5_edificios1_color;
        pista5_edificiosCartel1_textura = track5Customization.pista5_edificiosCartel1_textura;
        pista5_edificios2_textura = track5Customization.pista5_edificios2_textura;
        pista5_edificios2_color = track5Customization.pista5_edificios2_color;
        pista5_edificiosCartel2_textura = track5Customization.pista5_edificiosCartel2_textura;
        pista5_edificios3_textura = track5Customization.pista5_edificios3_textura;
        pista5_edificios3_color = track5Customization.pista5_edificios3_color;
        pista5_edificiosCartel3_textura = track5Customization.pista5_edificiosCartel3_textura;
        pista5_niebla_color = track5Customization.pista5_niebla_color;
        pista5_aroCheckpoint_color = track5Customization.pista5_aroCheckpoint_color;
        pista5_rampaFinal_textura = track5Customization.pista5_rampaFinal_textura;
        
        var track6Customization = json.Track_6_customization;
        pista6_textura = track6Customization.pista6_textura;
        pista6_color = track6Customization.pista6_color;
        pista6_borde_color = track6Customization.pista6_borde_color;
        pista6_edificios1_textura = track6Customization.pista6_edificios1_textura;
        pista6_edificios1_color = track6Customization.pista6_edificios1_color;
        pista6_edificiosCartel1_textura = track6Customization.pista6_edificiosCartel1_textura;
        pista6_edificios2_textura = track6Customization.pista6_edificios2_textura;
        pista6_edificios2_color = track6Customization.pista6_edificios2_color;
        pista6_edificiosCartel2_textura = track6Customization.pista6_edificiosCartel2_textura;
        pista6_edificios3_textura = track6Customization.pista6_edificios3_textura;
        pista6_edificios3_color = track6Customization.pista6_edificios3_color;
        pista6_edificiosCartel3_textura = track6Customization.pista6_edificiosCartel3_textura;
        pista6_niebla_color = track6Customization.pista6_niebla_color;
        pista6_aroCheckpoint_color = track6Customization.pista6_aroCheckpoint_color;
        pista6_rampaFinal_textura = track6Customization.pista6_rampaFinal_textura;
        
        var track7Customization = json.Track_7_customization;
        pista7_textura = track7Customization.pista7_textura;
        pista7_color = track7Customization.pista7_color;
        pista7_borde_color = track7Customization.pista7_borde_color;
        pista7_edificios1_textura = track7Customization.pista7_edificios1_textura;
        pista7_edificios1_color = track7Customization.pista7_edificios1_color;
        pista7_edificiosCartel1_textura = track7Customization.pista7_edificiosCartel1_textura;
        pista7_edificios2_textura = track7Customization.pista7_edificios2_textura;
        pista7_edificios2_color = track7Customization.pista7_edificios2_color;
        pista7_edificiosCartel2_textura = track7Customization.pista7_edificiosCartel2_textura;
        pista7_edificios3_textura = track7Customization.pista7_edificios3_textura;
        pista7_edificios3_color = track7Customization.pista7_edificios3_color;
        pista7_edificiosCartel3_textura = track7Customization.pista7_edificiosCartel3_textura;
        pista7_niebla_color = track7Customization.pista7_niebla_color;
        pista7_aroCheckpoint_color = track7Customization.pista7_aroCheckpoint_color;
        pista7_rampaFinal_textura = track7Customization.pista7_rampaFinal_textura;
        
        var track8Customization = json.Track_8_customization;
        pista8_textura = track8Customization.pista8_textura;
        pista8_color = track8Customization.pista8_color;
        pista8_borde_color = track8Customization.pista8_borde_color;
        pista8_edificios1_textura = track8Customization.pista8_edificios1_textura;
        pista8_edificios1_color = track8Customization.pista8_edificios1_color;
        pista8_edificiosCartel1_textura = track8Customization.pista8_edificiosCartel1_textura;
        pista8_edificios2_textura = track8Customization.pista8_edificios2_textura;
        pista8_edificios2_color = track8Customization.pista8_edificios2_color;
        pista8_edificiosCartel2_textura = track8Customization.pista8_edificiosCartel2_textura;
        pista8_edificios3_textura = track8Customization.pista8_edificios3_textura;
        pista8_edificios3_color = track8Customization.pista8_edificios3_color;
        pista8_edificiosCartel3_textura = track8Customization.pista8_edificiosCartel3_textura;
        pista8_niebla_color = track8Customization.pista8_niebla_color;
        pista8_aroCheckpoint_color = track8Customization.pista8_aroCheckpoint_color;
        pista8_rampaFinal_textura = track8Customization.pista8_rampaFinal_textura;
        
    }
    
    
}, 1000);



