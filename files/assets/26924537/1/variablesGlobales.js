var _control;
var _sonidos;
var _app;
var _2dScreen;
var score = 0;
var bestScore = 0;
var currentLevel = 1;
var currentLevelCont = 0;
var totalLevels = 8;
var arrayLevels = [];
var pistaAncho = 4.4*2;
var velocidadSwipe = 205;
var bolaRadio = 1.3;
var volumenRampa = 10;
var context;
var gameStart = false;
var enSalto = false;
var enPause = false;

var muteSound = 0;

var limiteBolas = 120;
var limiteRampas = 20;
var limiteBolasBi = 80;
var indexVientos = 0;
var limiteVientos = 4;
var objetosEnNivel = [];
var indexRampaRoja = 0;
var indexRampaVerde = 0;
var indexRampaAmarilla = 0;
var indexBolaRoja = 0;
var indexBolaAmarilla = 0;
var indexBolaVerde = 0;
var indexBolaBi = 0;
var idNoRev = -1;

var combo = 0;
var gameOver = false;
var bolaParpadeando = false;
var tiempoParpadeo = 4;
var yaContinuo = false;
var aumentador = 0;
var vientosPuestos = [];
var battle_mode = false;
var soloP1 = false;
var LEVELS = [1,2,3,4,5,6,7,8];
var LevelsObjetos = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
var currentLevelObjeto = 0;
var volumeAux = 0;
var levelElegido = -1;

function noEsNull(variable)
{   
    if(variable == "transparente")
        return(false);
    if(variable != null && variable != "")
        return(true);
    
    return(false);
}

function iniVars()
{
    enSalto = false;
    levelElegido = -1;
    arrayLevels = [];
    objetosEnNivel = [];
    vientosPuestos = [];
    indexRampaRoja = 0;
    indexRampaVerde = 0;
    indexRampaAmarilla = 0;
    indexBolaRoja = 0;
    indexVientos = 0;
    indexBolaAmarilla = 0;
    indexBolaVerde = 0;
    indexBolaBi = 0; 
    idNoRev = -1;
    aumentador = 0;
    combo = 0;
    gameOver = false;
    score = 0;
    LEVELS = [1,2,3,4,5,6,7,8];
    LevelsObjetos = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
    currentLevel = getLevel(true);//window.gameplay.startLevel;   
    
    
    currentLevelCont = 0;
    gameStart = false;
    bolaParpadeando = false;   
    yaContinuo = false;
}
function getLevel(inicial)
{
    if(LEVELS.length == 0)
        LEVELS = [1,2,3,4,5,6,7,8];
    
     if(LevelsObjetos.length == 0)
        LevelsObjetos = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
    
    levelElegido = levelElegido + 1;
    if(levelElegido == 8)
        levelElegido = 0;
    var index = levelElegido;//Math.floor(Math.random() * LEVELS.length);
    //if(inicial)
        //index = Math.floor(Math.random() * (LEVELS.length-2));
    var lvl = LEVELS[index];
    LEVELS.splice(index,1); 
    
    index = Math.floor(Math.random() * LevelsObjetos.length);
    if(inicial)
        index = Math.floor(Math.random() * 6);
    var lvlObjeto = LevelsObjetos[index];
    currentLevelObjeto = lvlObjeto;
    LevelsObjetos.splice(index,1); 
    
    return(lvl);
}

function reproduceAnim(mc,index,speed,desaparecer,time,loop)
{
    if(speed != null)
        mc.animation.speed = speed;
     var animacionIndex = mc.animation.assets[index];                   
     var nombre = mc.animation.animationsIndex[animacionIndex];                       
     mc.animation.play(nombre);      
    
    if(loop)
        mc.animation.loop = true;
    else
        mc.animation.loop = false;
    
    if(desaparecer)
        setTimeout(function(){ 
            mc.enabled = false;                
        },time);
};
function reproduceParticulas(particulas,tiempo)
{
     var particula = particulas;
     particula.enabled = true;
     particula.particlesystem.reset();   
     particula.particlesystem.play();
           
     setTimeout(function() {
         particula.enabled = false;                
     },tiempo);
}
function asignaEventos(mc,poner,evento,funcion)
{
    var ev = [];
    if(mc.element != null)
        mc = mc.element;
    switch(evento)
    {
        case 1:
            ev[0] = "touchstart";
            ev[1] = "mousedown";
        break;
        case 2:
            ev[0] = "touchend";
            ev[1] = "mouseup";
        break;
        case 3:
            ev[0] = "touchmove";
            ev[1] = "mousemove";
        break;
    }
    
    if(poner)
    {           
        if(isMobile.any()) 
            mc.on(ev[0],funcion);  

      
        else       
            mc.on(ev[1],funcion);      

    }
    else
    {
        mc.off();
    }
}

function setTexto(campo,valor)
{   
    campo.text = valor;
}
function tocaSonido(sonido)
{
    if(muteSound == 0)
         _sonidos.play(sonido);
}
function setMusicas()
{ 
    if(muteMusic == 0)
    {
        _sonidos.slot("musicTitle").volume = 0.5;
        _sonidos.slot("musicIngame").volume = 0.5;
        _sonidos.slot("musicBonus").volume = 0.5;
    }
    else  if(muteMusic == 1)
    {
        _sonidos.slot("musicTitle").volume = 0;
        _sonidos.slot("musicIngame").volume = 0;
        _sonidos.slot("musicBonus").volume = 0;
    }
}
function guarda_vars(variable,valor,esArray,dimensiones,index1,index2,index3,index4)
{
    if(esArray)
    {
        if(dimensiones == 1)
            variable[index1] = valor;
        else if(dimensiones == 2)
            variable[index1][index2] = valor;
        else if(dimensiones == 3)
            variable[index1][index2][index3] = valor;
        else if(dimensiones == 4)
            variable[index1][index2][index3][index4] = valor;
    }
    else
        variable = valor;
}
function forceMute(tipo)
{
    if(tipo)
    {
        volumeAux = _sonidos.slot("musicaIngame").volume;
        _sonidos.slot("musicaIngame").volume = 0;
    }
    else
        _sonidos.slot("musicaIngame").volume = volumeAux;
}
function guardaVars(variable,nombre)
{
    //console.log("guardate la variable nombre " + nombre + "  con el valor " + variable);
   //localStorage.setItem(nombre,variable+"");
}
function cargaVars(nombre)
{     
    return(null);
    //return(localStorage.getItem(nombre));             
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function ejecutaElementosSalida(elementos_name,padre,CALLBACK)
{   
    var elementos = [];    
    for(var i = 0; i< elementos_name.length;i++)
    {
        elementos[i] = [];
        if(padre == null)
            elementos[i][0] = _2dScreen.findByName(elementos_name[i][0]);       
        else
            elementos[i][0] = _2dScreen.padre.findByName(elementos_name[i][0]);       
        var callBack = null;
        if(i == 0)
            callBack = CALLBACK;            
        elementos[i][0].script.animaMsg.saca(true,callBack);  
        if(elementos_name[i][1] != null)
            asignaEventos(elementos[i][0].element,elementos_name[i][1]);
    }    
   
}
function ejecutaElementosEntrada(elementos_name,padre,CALLBACK)
{   
    var elementos = [];
    for(var i = 0; i< elementos_name.length;i++)
    {
        elementos[i] = [];
        if(padre == null)
            elementos[i][0] = _2dScreen.findByName(elementos_name[i][0]);       
        else
            elementos[i][0] = _2dScreen.padre.findByName(elementos_name[i][0]);       
        elementos[i][0].enabled = true;
        var callBack = null;
        if(i == 0)
            callBack = CALLBACK;
        elementos[i][0].script.animaMsg.pone(callBack);  
        if(elementos_name[i][1] != null)          
            asignaEventos(elementos[i][0].element,elementos_name[i][1],elementos_name[i][2],elementos_name[i][3]);       
    }     
};


var perm = [151,160,137,91,90,15,
                131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
                190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
                88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
                77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
                102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
                135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
                5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
                223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
                129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
                251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
                49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
                138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,151];


function cargaTextura(URL,_app,callback)
{
    _app.loader.getHandler("texture").crossOrigin = "anonymous";
    console.log("intentando cargar foto de url " + URL);
    var asset = new pc.Asset("foto"+Math.round(Math.random()*1000), "texture", {
    url: URL
         });
         ////console.log(foto);
        
        _app.assets.add(asset);
        _app.assets.load(asset);
                                          
        asset.on("load", function (asset) {
                     
            if(callback != null)
                callback(asset);
    });  
}
function getTexture (base64) 
{
  var texture = new pc.gfx.Texture(_app.graphicsDevice);
  
  var img = new Image();
  img.onload = function () {
    texture.minFilter = pc.gfx.FILTER_LINEAR;
    texture.magFilter = pc.gfx.FILTER_LINEAR;
    texture.addressU = pc.ADDRESS_REPEAT;//pc.gfx.ADDRESS_CLAMP_TO_EDGE;
    texture.addressV = pc.ADDRESS_REPEAT;//pc.gfx.ADDRESS_CLAMP_TO_EDGE;
    texture.setSource(img);
  };
  img.src = base64;
  return texture;
}
function cambiaTextura3D(textura,mc)
{
   var meshInstances = mc.model.meshInstances;
    for (var i = 0; i < meshInstances.length; ++i) { 
        var mesh = meshInstances[i];
        mesh.material.diffuseMap = textura;
        mesh.material.emissiveMap = textura;
        mesh.material.update();
    }     
}

function cargaMusica(URL,app,callBack)
{
   // _app.loader.getHandler("sound").crossOrigin = "anonymous";  
   console.log("intentando cargar URL " + URL);
    app.assets.loadFromUrl(URL, "audio", function (err, asset) {          
         console.log("cargado!");        
             
        if(callBack != null)
            callBack(asset);
        });
}
function cargaImagen(url,app,callback)
{   
 
     var tex = new pc.Texture( app.graphicsDevice, {
            mipmaps: false
        } );
        tex.minFilter = pc.FILTER_LINEAR;
        tex.magFilter = pc.FILTER_LINEAR;
        tex.addressU = pc.ADDRESS_CLAMP_TO_EDGE;
        tex.addressV = pc.ADDRESS_CLAMP_TO_EDGE;
    
        var img = document.createElement( 'img');       
        img.src = url;
        img.crossOrigin = 'anonymous';
        console.log("creando el id para sacar el source de la imagen " + url);
        //img.id = "img"+Math.ceil(Math.random()*9999) + "_" + Math.ceil(Math.random()*9999);       
        img.onload =  function(e) {            
            tex.setSource( img );             
            
            if(callback != null)
                callback(tex);
            
        };     
    
}

var withFinalHud = true;
window.gameplay = {
	ballSpeed: 0.042,
    enemy_ballSpeed:0.63,
    //delayStart:300, //in miliseconds
    startLevel:1, 
    numLevels:99,
    trailSize:1,
	//roadTexture1:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAACACAMAAADTa0c4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAllBMVEVnGH9ZDW9JAl1oDYJ2Fpl1FphpDoOYH718HKZsD4eGILJyEY+RILqQIbp3GJxqDoV/Hqp9HahuEIqIIbSJILRzE5KUILyTILt5GZ+AH6tvEIuLIbd0FJSWH716G6JrDoaDIK+CH65xEY6OIbl8HKWRIbp2GJtXDGx4GZ6AHqt5GqGCIK5wEIx2FpeFILF+Hah0EpH///9Ur4EVAAAAAWJLR0Qx2dsdcgAAAAd0SU1FB+ILGREXJOn9kC0AAAMQSURBVHja7d3tUhpBEIVhk1ZaJJFIxCgSiaAmGvNx/1eXGCPC7kxPz8Cyc3b6vYN5qqu2ivODvT3LsizLsorvzY5r+711gLc7zQDafq8BGIABGIABGIABGIABGIABGIABpAPQ/gEVDdBj7pUMQIfMh0kn0BGAPv+tXy4AHT0BHKWcQDcABvyvQakA9O4Z4H3CCXQBgI75f8fxAl0AGPKyYYkA9OEV4CT6BDoAMOKVRuUB0MdVgNPYE8AHGPNa49IA6Gwd4FPkCaAD0DlXOo8TQAcYcq24TyE4AF3UAS6iTgAcYMKOJuUA0KUL4DLmBKABaMrOpsUAuN/PnyNOABmArjwAfKUXQAaYsbdZCQD0xQ9wrT4BYIA5C827D0ALCWChPQFYANpnMe1QBAvQ40DKoQgV4GkKkrvRnQAqQD/0fu1QBArwPAXJ6YYiUIBB+P3KoQgTgG41ALeaE4AEeJ2C5O4UApAAQ937Vb+OIQLQiRZAMRQhAoy079cMRYAAdKoHCA9FS4CvLacHGOvfrxiKlgDBW2k4NUB1CpI7C50AHEB9CpL7FhCAA1B/Al8KfArRAFxTkNy9fAJoAJPY94eGIjAA9xQkJw9FWAC+KUhuKgmAAaS8n7kzAP4pSO5BEIACmKW9XxyKkADoeyqAMBQhAcxT38/82AUAeQqS8w9FOAB0kP5+5h8+ARyA4BQk5xuKYADoZjMA31AEA6CYguQ8QxEKgGYKkvvpPgEUANUUJOceikAAdFOQnHsowgCgu83f7xmKMACifwdz5/p1DAJAPwXJ/XKcAARAxBQkN8IEiJmC5BxDEQJA1BQk9xsRIG4KkqsPRfkDxE5BcrWhKH+ALX0CX6p+CrMHoPvtAlSHouwBEqYgucpQlDtAyhQkVxmKMgdIm4Lk1oei3AG2//7KUJQ3AD00AbA2FOUNkDwFya0ORVkD0HUzAKtDUdYAj828f20oyhpggylIbgECQI0FArCLDMAADMAADMAADMAADMAADMAADMAADCAHgJb+bi8bgLYyAAMwAAMwAAMwgNb6AwNPBtG4AVwgAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE4LTExLTI1VDE3OjIzOjM2KzAwOjAwYaJYNQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOC0xMS0yNVQxNzoyMzozNiswMDowMBD/4IkAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAAAElFTkSuQmCC",
    //roadTexture2:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAACACAMAAADTa0c4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAVFBMVEUASYQAI1EANWoAKkEAnTYAOzoAbL8AcrIAbMMAVTYAI0oAbr0AcmwAdpsAdpwAVzYAcbQAKkAAc28AQncAPDoAcLUAK0EAI0kAWDYAK0AAfDb////KWtyeAAAAAWJLR0QbAmDUpAAAAAd0SU1FB+ILGREMBpWrGlMAAAKzSURBVHja7d3tcpswFIThpAQhYopjAo6b+7/QdpwedRzzoSNUw0q7N+DhmaN5f/rpaeM9b7ytv58ABCAAAQiwB4AfD9/eAB79wwQgAAEIQAACEIAABCAAAQhAAAIQgACoAEWROcDLS94ApTFl1gCVtVXOALWx1tT5Arwe7J8dXrMFaOx1Ta4AhfkCMCEpTAHgp/27Nk+A0ghASAoTAKisW0AK8QFq8w8gIIXwAF8JlOlTCA/Q2JupU4gOUJhbAHUK0QFa+23aFIIDlOY7gDaF4ACVvZsyhdgAtbkHMMd8AG4TGJZCaIDGjk6VQmSAwowDqFKIDNDaiWlSCAxQmikATQqBASo7ubccAGozDaBIISzAeAL1KYQFaOzsvFOIClCYeQBzShygtQvzTSEoQGmWAHxTCArwtvT93inEBDguHoB3CiEB5hOoSyEkQOPz/dZ2qQIsJVCVQkSAxQRqUggIsJxATQoF4H3jKQA8EqhIIR6ATwLdCSynEA7AL4Gy5RTCAXSa7/dIIRrASfEAro/glBiAdwJlSykEA/BPoDuBPikARQJlQ0oAmgS6EzimA6BLoGw+hVAAygTKulQAtAl0j+CcCIA6gbI2DQB9At0J9EkADKHfP5tCHICQBLoTOOIDhCVQdviABwhMoKxDBziveADXR3AGBwhOoKzFBuhXHsB0CkEAViRQNiADrEmgO4ELLsC6BMrGUwgBsDKBsg4VYG0C3SM4gwKsTqCsxQRYn0B3Aj0kQIQEygZEgEu0AxhN4e4BPqIkUHafwt0DREqgrEMDiJVA9wh+gQFES6CsxQKIl0B3Aj0UQMQEygYkgJgJdCdwwQGIm0DZbQp3DfBp/ss+YQAeMQIQgAAEIAABCEAAAhCAAAQgAAEIQAACbA2w1R8u7gVgsxGAAAQgAAEIsN1+A33/J7QMR+U1AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE4LTExLTI1VDE3OjEyOjA2KzAwOjAwmQ1S6QAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOC0xMS0yNVQxNzoxMjowNiswMDowMOhQ6lUAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAAAElFTkSuQmCC"
    
};
window.gamePlayEvents =  {
    showEndScreen:function(road,score)
    {   
        //road is the current road you're playing before win/die
        //score is the score you obtain currentPlaying
        console.log("End Screen show be appear");
    },  
    retry:function(road)
    {
        console.log("will retry game with the road num " + road);
        _control.backMain(road);
    },
    startGame:function(road)
    {
        console.log("will start game with the road num " + road);
    }
};

function vibrar(intensidad)
{
    if (window.navigator && window.navigator.vibrate) {
  
        navigator.vibrate(intensidad);
    } 
}



Level1 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'V',	'A',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'CR',	'V'	],
[	'N',	'N',	'N'	],
[	'V',	'A',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'CA',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'CV',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'V',	'A',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'CV',	'R'	],
[	'N',	'N',	'N'	],
[	'CV',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'A',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'CAD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RI',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CAI'],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	]];



Level2 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'CR',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'CAD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CRI'],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'CRD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'CR',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'VI',	'N'	],
[	'N',	'N',	'N'	],
[	'CVD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'VD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CVI'],
[	'N',	'N',	'N'	],
[	'CVD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'CV',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'V',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CAI'],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	]];


Level3 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N' ],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A' ],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'AD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'AI'],
[	'N',	'N',	'N'	],
[	'AD',	'N',	'N'	],
[	'N',	'N',	'N' ],
[	'N',	'N',	'AI'],
[	'N',	'N',	'N'	],
[	'N',	'CAD',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	]];

Level4 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'CA',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CRI'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'CVD',	'N',	'N'],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'V',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'R',	'V',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CVI'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'R',	'V',	'A'	],
[	'N',	'N',	'N'	],
[	'V',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'A',	'CV',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'AD',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CAI'	],
[	'N',	'N',	'N'	],
[	'AD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CAI'],
[	'N',	'N',	'N'	],
[	'AD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'V',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R' ],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	]];

Level5 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'CR',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'CAD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CRI'],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'CRD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'CR',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'VI',	'N'	],
[	'N',	'N',	'N'	],
[	'CVD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'VD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CVI'],
[	'N',	'N',	'N'	],
[	'CVD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'CV',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'V',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CAI'],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	]];

Level6 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'CA',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'V',	'A',	'R'	],
[	'V',	'A',	'R'	],
[	'N',	'N',	'N'],
[	'A',	'V',	'R'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'V',	'R',	'A'	],
[	'V',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'CA',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CRI'],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	]];

Level7 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'RD',	'N',	'N'	],
[	'RD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'VI'],
[	'N',	'N',	'VI'	],
[	'N',	'N',	'VI'	],
[	'N',	'N',	'VI'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'AD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'CAD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'VI'	],
[	'N',	'N',	'VI'	],
[	'N',	'RD',	'N'	],
[	'N',	'N',	'N'	],
[	'AD',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'R',	'V',	'A'	],
[	'R',	'V',	'A'	],
[	'R',	'V',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'V',	'A',	'R'	],
[	'V',	'A',	'R'	],
[	'V',	'A',	'R'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'CA',	'V',	'R'],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	]];

Level8 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'V',	'A',	'R'	],
[	'N',	'N',	'N'	],
[	'V',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'V',	'A',	'R'	],
[	'N',	'N',	'N'	],
[	'CV',	'R',	'A'	],
[	'N',	'N',	'N'	],
[	'R',	'A',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'R',	'V',	'A'	],
[	'N',	'N',	'N'	],
[	'R',	'V',	'A'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'CR',	'V',	'A'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'RV',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'CV',	'A',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'R',	'V'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'CV',	'A',	'R'	],
[	'N',	'N',	'N'	],
[	'A',	'V',	'R'	],
[	'N',	'N',	'N'	],
[	'N',	'RA',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'A'	],
[	'N',	'N',	'N'	],
[	'N',	'A',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'CAI'	],
[	'N',	'N',	'N'	],
[	'N',	'AI',	'N'	],
[	'N',	'N',	'N'	],
[	'CA',	'V',	'R'],
[	'N',	'N',	'N'	],
[	'N',	'RR',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	]];

var Level9= [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","RV","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["N","RA","N" ],
["A","N","N" ],
["N","N","A" ],
["N","N","A" ],
["N","RV","N" ],
["N","N","CV" ],
["N","V","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","RR","N" ],
["N","RD","N" ],
["R","N","N" ],
["N","RI","N" ],
["N","N","R" ],
["R","N","N" ],
["N","RV","N" ],
["N","N","V" ],
["N","V","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","RR","N" ],
["N","RD","N" ],
["R","N","N" ],
["N","RD","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","CR" ],
["N","RR","N" ],
["N","R","N" ],
["N","RD","N" ],
["RD","N","N" ],
["R","N","N" ],
["N","RA","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","A","N" ],
["N","AI","N" ],
["A","N","N" ],
["A","N","N" ],
["N","RR","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","N","RI" ],
["N","N","R" ],
["R","N","N" ],
["R","N","N" ],
["N","RV","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","V","N" ],
["N","V","N" ],
["V","N","N" ],
["V","N","N" ],
["N","RA","N" ],
["A","R","V" ]];

var Level10 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","A","N" ],
["A","N","N" ],
["A","N","N" ],
["N","RR","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","N","R" ],
["N","N","RI" ],
["N","R","N" ],
["N","RD","N" ],
["R","N","N" ],
["R","N","N" ],
["N","RA","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["N","A","N" ],
["N","A","N" ],
["A","N","N" ],
["AD","N","N" ],
["N","RR","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","CR","A" ],
["N","N","RI" ],
["N","N","R" ],
["N","RD","N" ],
["N","R","N" ],
["RD","N","N" ],
["R","N","N" ],
["N","RA","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","RV","N" ],
["N","N","V" ],
["N","N","V" ],
["V","N","N" ],
["N","V","N" ],
["N","N","V" ],
["N","N","V" ],
["V","N","N" ],
["N","V","N" ],
["N","N","V" ],
["N","N","V" ],
["V","N","N" ],
["N","V","N" ],
["N","N","V" ],
["CVD","N","N" ],
["N","V","N" ],
["N","N","VI" ],
["N","VI","N" ],
["N","N","V" ],
["N","RR","N" ],
["N","R","N" ],
["N","RI","N" ],
["R","N","N" ],
["R","N","N" ],
["N","RA","N" ],
["A","R","V" ]];

var Level11 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","AD","N" ],
["A","N","N" ],
["A","N","N" ],
["N","RR","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","N","R" ],
["N","N","R" ],
["R","N","N" ],
["RD","N","N" ],
["N","RA","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","A","N" ],
["N","A","N" ],
["CA","N","N" ],
["A","N","N" ],
["N","RR","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","N","RI" ],
["N","N","R" ],
["R","N","N" ],
["R","N","N" ],
["N","RA","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","N","AI" ],
["N","N","A" ],
["N","A","N" ],
["N","A","N" ],
["A","N","N" ],
["A","N","N" ],
["N","RR","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","RV","N" ],
["N","N","VI" ],
["N","VD","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","RR","N" ],
["N","RD","N" ],
["R","N","N" ],
["N","RD","N" ],
["N","N","R" ],
["N","N","R" ],
["R","N","N" ],
["N","RV","N" ],
["N","N","V" ],
["N","V","N" ],
["R","A","CV" ],
["R","A","V" ],
["R","A","V" ],
["N","RR","N" ],
["N","RD","N" ],
["RD","N","N" ]];

var Level12= [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","RA","N" ],
["AD","N","N" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["N","RR","N" ],
["N","RD","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","RI","N" ],
["N","N","CRI" ],
["RD","N","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","RV","N" ],
["N","N","VI" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","RA","N" ],
["AD","N","N" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["N","RR","N" ],
["N","RI","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","CR" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RV","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["VD","N","N" ],
["N","N","VI" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","RA","N" ],
["AD","N","N" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","RR","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["R","A","V" ]];

var Level13 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","RA","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","RV","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","CV","R" ],
["N","RR","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","RA","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","N","AI" ],
["AD","N","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["AD","N","N" ],
["N","N","AI" ],
["N","AD","N" ],
["N","AI","N" ],
["N","N","AI" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","N","AI" ],
["N","RV","N" ],
["N","N","VI" ],
["N","N","CVI" ],
["N","VI","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","VI","N" ],
["N","N","VI" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","VI","N" ],
["A","R","V" ]];

var Level14 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["RD","N","N" ],
["N","RD","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RV","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["VD","N","N" ],
["VD","N","N" ],
["N","N","VI" ],
["N","VI","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","RR","N" ],
["A","CR","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["RD","N","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","N","RI" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","RV","N" ],
["N","N","VI" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","N","CVI" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","N","VI" ],
["N","VD","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["N","RA","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["CA","V","R" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","N","AI" ],
["A","V","R" ]];

var Level15 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["A","V","R" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","N","AI" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","N","AI" ],
["N","AI","N" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","N","AI" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","RV","N" ],
["N","N","VI" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","VD","N" ],
["VD","N","N" ],
["VD","N","N" ],
["N","N","VI" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","N","VI" ],
["VD","N","N" ],
["VD","N","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","RA","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","AD","N" ],
["N","AI","N" ],
["N","RV","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","RR","N" ],
["V","R","A" ],
["V","R","A" ]];

var Level16 =[
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","RA","N" ],
["AD","N","N" ],
["AD","N","N" ],
["N","N","AI" ],
["AD","N","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["AD","N","N" ],
["N","N","AI" ],
["N","AD","N" ],
["N","AI","N" ],
["N","N","AI" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","N","AI" ],
["N","RV","N" ],
["N","N","VI" ],
["N","N","VI" ],
["N","VI","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","VI","N" ],
["N","N","VI" ],
["VD","N","N" ],
["VD","N","N" ],
["N","VD","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","RR","N" ],
["N","RI","N" ],
["N","RD","N" ],
["N","N","RI" ],
["RD","N","N" ],
["N","RD","N" ],
["N","RI","N" ],
["N","N","RI" ],
["RD","N","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["RD","N","N" ],
["RD","N","N" ],
["N","N","RI" ],
["N","RA","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","AD","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["AD","N","N" ],
["N","AI","N" ],
["N","N","AI" ],
["N","N","AI" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","CRI","N" ]];


var Level17= [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["A","V","R" ],
["N","RR","N" ],
["N","RD","N" ],
["RD","N","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RD","N" ],
["N","N","RI" ],
["N","N","RI" ],
["N","RA","N" ],
["AD","N","N" ],
["N","AI","N" ],
["AD","N","N" ],
["N","CAI","N" ],
["N","N","AI" ],
["N","N","AI" ],
["AD","N","N" ],
["N","AI","N" ],
["N","N","AI" ],
["N","AI","N" ],
["AD","N","N" ],
["N","AI","N" ],
["N","AI","N" ],
["N","RR","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","N","RI" ],
["N","RI","N" ],
["RD","N","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RI","N" ],
["RD","N","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","RA","N" ],
["AD","N","N" ],
["N","AD","N" ],
["AD","N","N" ],
["N","N","AI" ],
["N","N","AI" ],
["AD","N","N" ],
["N","AD","N" ],
["N","N","AI" ],
["N","AI","N" ],
["AD","N","N" ],
["N","AI","N" ],
["N","AI","N" ],
["N","RR","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RD","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","RD","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RI","N" ],
["RD","N","N" ],
["N","N","RI" ],
["N","N","RI" ],
["N","RD","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RI","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","RI","N" ]];

var Level18 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","N","RI" ],
["N","RA","N" ],
["AD","N","N" ],
["N","N","AI" ],
["N","AI","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","AI","N" ],
["N","AI","N" ],
["N","N","AI" ],
["N","AI","N" ],
["N","N","AI" ],
["N","N","AI" ],
["N","AI","N" ],
["N","AD","N" ],
["N","N","AI" ],
["N","AI","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","AD","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","AI","N" ],
["N","N","AI" ],
["N","AI","N" ],
["N","RV","N" ],
["N","N","VI" ],
["N","CVI","N" ],
["N","N","VI" ],
["N","VI","N" ],
["N","N","VI" ],
["N","VI","N" ],
["VD","N","N" ],
["N","VI","N" ],
["N","VD","N" ],
["N","N","VI" ],
["N","VI","N" ],
["N","N","VI" ],
["N","VI","N" ],
["N","N","VI" ],
["N","VI","N" ],
["VD","N","N" ],
["N","VI","N" ],
["VD","N","N" ],
["N","VD","N" ],
["N","N","VI" ],
["N","VD","N" ],
["N","N","VI" ],
["N","VD","N" ],
["VD","N","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","RA","N" ],
["AD","N","N" ],
["N","AI","N" ],
["N","N","CAI" ],
["N","AD","N" ],
["N","N","AI" ],
["N","AI","N" ],
["N","N","AI" ],
["N","AD","N" ],
["AD","N","N" ],
["N","AI","N" ],
["N","N","AI" ],
["N","AI","N" ],
["AD","N","N" ],
["AD","N","N" ]];

var Level19 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","N","AI" ],
["N","AD","N" ],
["AD","N","N" ],
["N","AD","N" ],
["AD","N","N" ],
["N","AD","N" ],
["N","N","AI" ],
["N","AD","N" ],
["N","N","AI" ],
["N","AI","N" ],
["AD","N","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","RV","N" ],
["N","N","VI" ],
["N","VI","N" ],
["N","N","VI" ],
["N","VD","N" ],
["A","R","CV" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","RA","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","N","AI" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","N","AI" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["AD","N","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","CA","V" ],
["N","N","AI" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","RR","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RD","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","RD","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RD","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RD","N" ],
["RD","N","N" ],
["N","N","RI" ],
["N","RI","N" ],
["R","V","A" ],
["R","V","A" ],
["CR","V","A" ],
["N","RI","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RI","N" ],
["N","N","RI" ]];

var Level20 =[
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","N","VI" ],
["N","VD","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","VI","N" ],
["N","N","VI" ],
["N","VD","N" ],
["VD","N","N" ],
["N","VD","N" ],
["N","VD","N" ],
["N","N","VI" ],
["N","VI","N" ],
["N","VI","N" ],
["N","N","VI" ],
["N","VD","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","VI","N" ],
["N","VI","N" ],
["N","RA","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","AD","N" ],
["N","N","AI" ],
["N","AD","N" ],
["AD","N","N" ],
["N","AI","N" ],
["N","N","AI" ],
["N","AD","N" ],
["N","RR","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RI","N" ],
["RD","N","N" ],
["N","RD","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RI","N" ],
["RD","N","N" ],
["N","RI","N" ],
["RD","N","N" ],
["N","RI","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RI","N" ],
["RD","N","N" ],
["N","RI","N" ],
["N","RD","N" ],
["N","RV","N" ],
["N","N","VI" ],
["VD","N","N" ],
["VD","N","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["VD","N","N" ],
["N","VI","N" ],
["N","VD","N" ],
["N","N","VI" ]];

var Level21= [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["A","V","R" ],
["A","V","R" ],
["N","RV","N" ],
["N","N","VI" ],
["N","VD","N" ],
["N","VI","N" ],
["VD","N","N" ],
["N","VI","N" ],
["N","N","VI" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","RA","N" ],
["CAD","N","N" ],
["N","AD","N" ],
["AD","N","N" ],
["N","AI","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","RR","N" ],
["N","RI","N" ],
["N","N","RI" ],
["RD","N","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RI","N" ],
["N","N","RI" ],
["RD","N","N" ],
["N","RI","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","RI","N" ],
["N","N","RI" ],
["N","N","RI" ],
["N","RR","N" ],
["N","RI","N" ],
["RD","N","N" ],
["N","RD","N" ],
["RD","N","N" ],
["N","RV","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","RA","N" ],
["AD","N","N" ],
["N","AD","N" ],
["AD","N","N" ],
["N","N","AI" ],
["N","AI","N" ],
["AD","N","N" ],
["N","AD","N" ],
["N","RV","N" ],
["N","N","VI" ],
["N","VI","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","VI","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","RA","N" ],
["AD","N","N" ],
["N","N","AI" ],
["N","AI","N" ],
["AD","N","N" ],
["R","A","V" ],
["R","A","V" ]];

var Level22 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","RR","N" ],
["N","RD","N" ],
["N","RI","N" ],
["N","N","RI" ],
["RD","N","N" ],
["N","RI","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RI","N" ],
["N","N","RI" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["N","RD","N" ],
["N","N","RI" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["RD","N","N" ],
["N","RI","N" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["N","RA","N" ],
["AD","N","N" ],
["N","AI","N" ],
["AD","N","N" ],
["N","N","AI" ],
["N","AD","N" ],
["V","R","A" ],
["V","R","CA" ],
["V","R","A" ],
["N","AI","N" ],
["AD","N","N" ],
["N","AI","N" ],
["N","AI","N" ],
["N","RV","N" ],
["N","N","VI" ],
["N","VD","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["VD","N","N" ],
["N","VD","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","N","VI" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["VD","N","N" ],
["N","VD","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","VD","N" ],
["N","N","VI" ],
["N","N","VI" ],
["N","RA","N" ],
["AD","N","N" ],
["AD","N","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","N","AI" ],
["N","AD","N" ],
["N","N","AI" ],
["N","N","AI" ]];


var Level23 =[
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["A","R","V" ],
["A","R","V" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","RD","N" ],
["N","RA","N" ],
["AD","N","N" ],
["N","AD","N" ],
["AD","N","N" ],
["N","AD","N" ],
["N","N","AI" ],
["N","AD","N" ],
["N","N","AI" ],
["N","RR","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","N","RI" ],
["N","RI","N" ],
["N","CRI","N" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["N","RV","N" ],
["N","N","VI" ],
["N","VD","N" ],
["N","N","VI" ],
["N","VD","N" ],
["N","N","VI" ],
["VD","N","N" ],
["N","VD","N" ],
["N","N","VI" ],
["N","VI","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","VD","N" ],
["N","N","VI" ],
["V","R","A" ],
["V","R","A" ],
["CV","R","A" ],
["N","RR","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RI","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RI","N" ],
["N","N","RI" ],
["RD","N","N" ],
["N","RV","N" ],
["N","N","VI" ],
["N","VD","N" ],
["N","N","VI" ],
["N","VI","N" ],
["VD","N","N" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["N","N","VI" ],
["N","VI","N" ],
["N","N","VI" ],
["VD","N","N" ],
["N","VD","N" ],
["N","N","CVI" ],
["VD","N","N" ],
["N","N","VI" ],
["N","VD","N" ],
["N","N","VI" ],
["VD","N","N" ]];

var Level24 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","RI","N" ],
["N","N","RI" ],
["N","RI","N" ],
["RD","N","N" ],
["N","N","RI" ],
["N","RD","N" ],
["N","N","RI" ],
["RD","N","N" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RI","N" ],
["RD","N","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RV","N" ],
["N","N","VI" ],
["VD","N","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","VI","N" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","RR","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["N","N","RI" ],
["N","RI","N" ],
["N","N","RI" ],
["N","RD","N" ],
["N","N","RI" ],
["RD","N","N" ],
["N","RD","N" ],
["RD","N","N" ],
["RD","N","N" ],
["N","RV","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["N","N","VI" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["N","N","VI" ],
["N","VI","N" ],
["N","N","VI" ],
["N","VI","N" ],
["N","N","VI" ],
["VD","N","N" ],
["N","VD","N" ],
["N","N","VI" ],
["N","RA","N" ]];

var Level25 = [
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
[	'N',	'N',	'N'	],
["N","N","AI" ],
["N","AI","N" ],
["N","N","AI" ],
["N","N","AI" ],
["N","RR","N" ],
["N","RD","N" ],
["N","N","RI" ],
["RD","N","N" ],
["N","RI","N" ],
["RD","N","N" ],
["N","N","RI" ],
["N","RD","N" ],
["RD","N","N" ],
["N","RD","N" ],
["N","N","RI" ],
["N","RD","N" ],
["RD","N","N" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["N","RA","N" ],
["AD","N","N" ],
["N","N","AI" ],
["N","AD","N" ],
["N","N","AI" ],
["N","AD","N" ],
["AD","N","N" ],
["AD","N","N" ],
["N","N","AI" ],
["N","AI","N" ],
["N","N","AI" ],
["N","N","AI" ],
["AD","N","N" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["N","RV","N" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["R","V","A" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["V","A","R" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","R","V" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["A","V","R" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["V","R","A" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
["R","A","V" ],
[" N","N","CVI"]];