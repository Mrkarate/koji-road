var MensajeScoreGui = pc.createScript('mensajeScoreGui');
MensajeScoreGui.attributes.add("texturas", {type: "asset", assetType: "texture", array: true, title: "texturas"});
MensajeScoreGui.attributes.add('material', { type: 'asset', assetType: 'material', array: true });
MensajeScoreGui.attributes.add('parpadeo', {type: 'number', title: 'parpadeo'});
MensajeScoreGui.attributes.add('aparicion', {type: 'number', title: 'aparicion'});

// initialize code called once per entity
MensajeScoreGui.prototype.initialize = function() {
    
    console.log("PERO Q MIERDA ");
    this.tween = null;
    var pos = this.entity.getLocalPosition();
    this.posicionInicial = new pc.Vec3(pos.x,pos.y,pos.z);
    //this.entity.setLocalPosition(100,100,100);;
    this.alfaCont = 1;
    this.contStartAlfa = 0;
    if(this.aparicion == 0)
        this.entity.enabled = false;
};

MensajeScoreGui.prototype.poneTextura = function(num) {
         
     if(this.tween !=  null)
         this.tween.stop();
    
     if(this.posicionInicial == null)
         this.initialize();
     var materialDifuso = this.material[0].resource; 
     var textureDifuse = this.texturas[num].resource; 
     this.entity.setLocalPosition(this.posicionInicial.x,this.posicionInicial.y,this.posicionInicial.z);    
    
     materialDifuso.diffuseMap = textureDifuse;
     materialDifuso.opacityMap = textureDifuse;
     materialDifuso.opacity = 1;
     this.alfaCont = 1;
     this.contStartAlfa = 0;
     materialDifuso.update();   
    
     if(this.parpadeo == 1)
     { 
           var posicion = this.entity.getLocalPosition();
          this.tween = this.entity.tween(posicion)
            .to(new pc.Vec3(posicion.x, posicion.y, posicion.z), 1.2, pc.QuadraticOut)
            .on('update',this.controlaAlpha.bind(this,materialDifuso)) 
            .on('complete',this.escondeMensaje.bind(this)) 
            .start(); 
     }
    
};
MensajeScoreGui.prototype.controlaAlpha = function(materialDifuso) {
    this.contStartAlfa = this.contStartAlfa + 1;
    if(this.contStartAlfa == 5)
    {
        this.entity.enabled = !this.entity.enabled;
        this.contStartAlfa = 0;
    }
    /*this.contStartAlfa = this.contStartAlfa + 1;
    
    if(this.contStartAlfa >= 12)
    {
        this.alfaCont = this.alfaCont - 0.027;
        if(this.alfaCont < 0)
            this.alfaCont = 0;
        materialDifuso.opacity = this.alfaCont;
        materialDifuso.update(); 
        //console.log("Q CTM PASA PO!!");
    }*/
    
    
};
MensajeScoreGui.prototype.escondeMensaje = function(materialDifuso) {
    
   this.entity.enabled = false;
};
