var CambiaTextura = pc.createScript('cambiaTextura');
CambiaTextura.attributes.add('material', { type: 'asset', assetType: 'material', array: false });
CambiaTextura.attributes.add("texturas", {type: "asset", assetType: "texture", array: true, title: "texturas"});
CambiaTextura.attributes.add('transparente', {type: 'number', default:0,title: 'transparente'});
CambiaTextura.attributes.add('emisivo', {type: 'number', default:0,title: 'emisivo'});
CambiaTextura.attributes.add('esElement', {type: 'number', default:0,title: 'esElement'});
// initialize code called once per entity
CambiaTextura.prototype.initialize = function() {
    
};

// update code called every frame
CambiaTextura.prototype.cambio = function(num) {
    
    var textura = this.texturas[num];
    if(this.esElement == 0)
    {
        var material = this.material.resource;
        material.diffuseMap = textura.resource;
        if(this.transparente == 1)
           material.opacityMap = textura;
        if(this.emisivo == 1)
           material.emissiveMap = textura;


        material.update();
    }
    else
    {
        this.entity.element.texture = textura.resource;        
    }
    
   
};

// swap method called for script hot-reloading
// inherit your script state here
// CambiaTextura.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/