var Parpadear = pc.createScript('parpadear');
Parpadear.attributes.add('tiempoDesaparicion', {type: 'number', default:0.85,title: 'tiempoDesaparicion'});
Parpadear.attributes.add('tiempoAparicion', {type: 'number', default:0.45,title: 'tiempoAparicion'});
Parpadear.attributes.add('esElement', {type: 'number', default:1,title: 'esElement'});
// initialize code called once per entity
Parpadear.prototype.initialize = function() {
    
    this.contador = 0;
};

// update code called every frame
Parpadear.prototype.update = function(dt) {
    
    this.contador = this.contador + dt;
    
    if(this.esElement == 1)
    {
        if(this.entity.element.enabled)
        {
            if(this.contador > this.tiempoDesaparicion)
            {
                this.contador = 0;        
                this.entity.element.enabled = false;
            }
        }
        else if(!this.entity.element.enabled)
        {
            if(this.contador > this.tiempoAparicion)
            {
                this.contador = 0;        
                this.entity.element.enabled = true;
            }
        }
    }
    else
    {
        if(this.entity.model.enabled)
        {
            if(this.contador > this.tiempoDesaparicion)
            {
                this.contador = 0;        
                this.entity.model.enabled = false;
            }
        }
        else if(!this.entity.model.enabled)
        {
            if(this.contador > this.tiempoAparicion)
            {
                this.contador = 0;        
                this.entity.model.enabled = true;
            }
        }
    }
};

// swap method called for script hot-reloading
// inherit your script state here
// Parpadear.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/