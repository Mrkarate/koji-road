var AnimaMsg = pc.createScript('animaMsg');
AnimaMsg.attributes.add('destinox', {type: 'number', default:0,title: 'destinox'});
AnimaMsg.attributes.add('destinoy', {type: 'number', default:0,title: 'destinoy'});
AnimaMsg.attributes.add('destinoz', {type: 'number', default:0,title: 'destinoz'});
AnimaMsg.attributes.add('tiempo', {type: 'number', default:0.6,title: 'tiempo'});
AnimaMsg.attributes.add('esElement', {type: 'number', default:0,title: 'esElement'});
AnimaMsg.attributes.add('delaySaca', {type: 'number', default:1.8,title: 'delaySaca'});
AnimaMsg.attributes.add("boton", {type: "asset", assetType: "texture", array: true, title: "boton"});
AnimaMsg.attributes.add('tipoTween', {type: 'string', default:"BounceOut",title: 'tipoTween'});
// initialize code called once per entity
// 
AnimaMsg.prototype.elijeTween = function(tween) {
   
   
    switch(tween)
    {
        case "BounceOut":
            tween = pc.BounceOut;
        break;
         case "SineIn":
            tween = pc.SineIn;
        break;
         case "SineOut":
           tween = pc.SineOut;
        break;
         case "SineInOut":
           tween = pc.SineInOut;
        break;
            
    }
    return(tween);
    
};
AnimaMsg.prototype.initialize = function() {
    
    
    this.mc = this.entity;   
    this.tween = null;  
    var origen = this.entity.getLocalPosition();
    this.origen =  new pc.Vec3(origen.x,origen.y,origen.z);    
    //this.destino = new pc.Vec3(this.destinox,this.destinoy,this.destinoz);
    this.tipoTween = this.elijeTween(this.tipoTween);
    this.entity.enabled = false;
    
};
AnimaMsg.prototype.clickBoton = function(callBack,cambiar,textIndex) {
   
    if(cambiar != null)    
    {
        if(this.boton != null && this.boton[textIndex] != null)
            this.entity.element.texture = this.boton[textIndex].resource;  
    }
    else
    {    
        if(this.boton.length > 0 && this.boton[1] != null)
        {
            this.entity.element.texture = this.boton[1].resource;   
            setTimeout(this.restauraBoton.bind(this,callBack),180); 
        }
    }
    
};
AnimaMsg.prototype.restauraBoton = function(callBack)
{
    if(this.boton[0] != null)
    {
        this.entity.element.texture = this.boton[0].resource;
        callBack();  
    }
};
AnimaMsg.prototype.pone = function(callBack,tiempo)
{
    var t = this.tiempo;    
     if(tiempo != null)
        t = tiempo;
   this.entity.setLocalPosition(this.origen.x,this.origen.y,this.origen.z);
   var pos = this.entity.getLocalPosition();
   var destino = new pc.Vec3(this.destinox,this.destinoy,this.destinoz);
   var origen = new pc.Vec3(pos.x + destino.x ,pos.y + destino.y,pos.z + destino.z);
   this.entity.setLocalPosition(origen.x,origen.y,origen.z);   
   this.entity.enabled = true; 
   pos = this.entity.getLocalPosition();
   this.tween =  this.mc.tween(pos).to(this.origen, t, this.tipoTween);   
   this.tween.on('complete',this.terminaPone.bind(this,callBack)); 
   this.tween.start();     
};

AnimaMsg.prototype.poneScale = function(callBack,tiempo,tipoTween)
{   
  var t = this.tiempo;    
     if(tiempo != null)
        t = tiempo;
    
     var tt = this.tipoTween;
    if(tipoTween != null)
      tt = tipoTween;
    
  tt = this.elijeTween(tt);
   this.entity.setLocalScale(0,0,0);  
   var destino = new pc.Vec3(1,1,1);  
   var pos = this.entity.getLocalScale();  
   this.entity.enabled = true;   
   this.tween =  this.mc.tween(pos).to(destino,  t, tt);   
   this.tween.on('complete',this.terminaPone.bind(this,callBack)); 
   this.tween.start();     
};
AnimaMsg.prototype.sacaScale = function(esconder,callBack,tiempo,tipoTween)
{    
    var t = this.tiempo;    
     if(tiempo != null)
        t = tiempo;
    
     var tt = this.tipoTween;
    if(tipoTween != null)
      tt = tipoTween;
    
   tt = this.elijeTween(tt); 
   this.entity.setLocalScale(1,1,1);  
   var destino = new pc.Vec3(0,0,0);  
   var pos = this.entity.getLocalScale();
   this.entity.enabled = true;   
   this.tween =  this.mc.tween(pos).to(destino, t, tt);   
   this.tween.on('complete',this.terminaSaca.bind(this,esconder,callBack)); 
   this.tween.start();     
};

AnimaMsg.prototype.saca = function(esconder,callBack,tiempo)
{  
   var t = this.tiempo;    
     if(tiempo != null)
        t = tiempo;
   this.entity.setLocalPosition(this.origen.x,this.origen.y,this.origen.z);
   var pos = this.entity.getLocalPosition(); 
   var destino = new pc.Vec3(this.destinox,this.destinoy,this.destinoz);
   destino = new pc.Vec3(pos.x + destino.x ,pos.y + destino.y,pos.z + destino.z);  
   this.entity.enabled = true; 
   this.tween =  this.mc.tween(pos).to(destino, t, this.tipoTween);     
   this.tween.on('complete',this.terminaSaca.bind(this,esconder,callBack)); 
   this.tween.start();     
};
AnimaMsg.prototype.sacaInverso = function(esconder,callBack,tiempo)
{  
   var t = this.tiempo;    
     if(tiempo != null)
        t = tiempo;
   this.entity.setLocalPosition(this.origen.x,this.origen.y,this.origen.z);
   var pos = this.entity.getLocalPosition(); 
   var destino = new pc.Vec3(this.destinox,this.destinoy,this.destinoz);
   destino = new pc.Vec3(pos.x - destino.x ,pos.y - destino.y,pos.z - destino.z);  
   this.entity.enabled = true; 
   this.tween =  this.mc.tween(pos).to(destino, t, this.tipoTween);     
   this.tween.on('complete',this.terminaSaca.bind(this,esconder,callBack)); 
   this.tween.start();     
};
AnimaMsg.prototype.terminaPone = function(callBack)
{     
     if(callBack != null)
       callBack();
};

AnimaMsg.prototype.terminaSaca = function(esconder,callBack)
{
     if(esconder)
       this.entity.enabled = false;
    
     if(callBack != null)
       callBack();
};

AnimaMsg.prototype.poneSaca = function(callBack)
{    
   
};

AnimaMsg.prototype.animaMano = function(tipo)
{
    var t = this.tiempo;    
  
   
   var pos = this.entity.getLocalPosition();
   if(tipo == 0)
       var destino = new pc.Vec3(pos.x + this.destinox,pos.y,pos.z); 
   else if(tipo == 1)
   {
       var destino = new pc.Vec3(pos.x - this.destinox*2,pos.y,pos.z); 
       t = t *2;
   }
   else if(tipo == 2)
   {
       var destino = new pc.Vec3(pos.x + this.destinox*2,pos.y,pos.z); 
       t = t *2;
   }
   tipo = tipo + 1;
   if(tipo == 3)
       tipo = 1;
   this.entity.enabled = true;   
   this.tween =  this.mc.tween(pos).to(destino, t, this.tipoTween);   
   this.tween.on('complete',this.animaMano.bind(this,tipo)); 
   this.tween.start();     
};
AnimaMsg.prototype.detieneMano = function(tipo)
{
    this.tween.stop();   
};

// swap method called for script hot-reloading
// inherit your script state here
// AnimaMsg.prototype.swap = function(old) { };

// to learn more about script anatomy, please read:
// http://developer.playcanvas.com/en/user-manual/scripting/