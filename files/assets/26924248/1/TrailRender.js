 var TrailRender = pc.createScript('TrailRender');
TrailRender.attributes.add('player', {type: 'entity', title: 'player'});

// initialize code called once per entity
TrailRender.prototype.initialize = function() {
   
    var app = this.app;
    
    var x='1234567';
    var str='';
    for(var i=0;i<x.length;i++){
        //获取数字
        var number=x[i];
        //获取长度
        var length=1;        
        while(x[i+length]===number){
            length+=1;
        }
        i+=length-1;
        str=str.concat(length+number);
    }
    console.log(str);
    
    
    // var manager = app.root.findByName('Manager');
    // this.gamemanager = manager.script.GameStateManager;
    //this.player = app.root.findByName('Player');
    this.scale = 0.2;
    this.delay = 0;
    this.trails = [];
    this.count = this.player.children.length;
    
    this.index = 0;
    this.timer = 0;
    var obj = this.entity.children[0];
    //obj.setLocalScale(0.001,0.001,0.001);
    this.trails.push(obj);
    for(var i =0;i<this.count;i++)
    {
        var clone = obj.clone();
        this.entity.addChild(clone);
        //clone.setLocalScale(0.001,0.001,0.001);
        this.trails.push(clone);
    }
    
};

TrailRender.prototype.update = function(dt) {
    // if(!this.gamemanager.start || !this.gamemanager.startgame)
    // {
    //     return;
    // }
    // if(this.gamemanager.lose)
    // {
    //     return;
    // }
     
    
    this.timer += dt;
    if(this.timer > this.delay)
    {
        this.timer = 0;
        this.SetTrail();
    }

};

TrailRender.prototype.SetTrail = function(){
  
    
    if(gameOver) //|| enSalto)    
        this.trails[this.index].enabled = false;
    
    else
        this.trails[this.index].enabled = true; 
    
    var pos = this.player.getPosition();//this.player.children[this.index].getPosition();
    var eulerz = this.player.getEulerAngles().z;
    var delta = 0;//(this.scale/20)*this.index;
    
    this.trails[this.index].setLocalScale(this.scale-delta,this.scale-delta,this.scale-delta);
    this.trails[this.index].setPosition(pos.x,pos.y,pos.z);
    this.trails[this.index].setEulerAngles(90,0,eulerz);
    this.trails[this.index].script.SingleTrail.Init();
    this.index ++;
    if(this.index == this.count)
    {
        this.index = 0;
    }
};

TrailRender.prototype.Init = function(){
    this.index = 0;
    this.timer = 0;
    for(var i =0;i<this.count;i++)
    {
        this.trails[i].setLocalScale(0.001,0.001,0.001);
    }
};