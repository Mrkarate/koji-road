var vientoPista8 = [];
vientoPista8[0] = 25;
vientoPista8[1] = 124;


var pista8Spline = [];
pista8Spline[0] = new pc.Vec3(0,0,0);
pista8Spline[1] = new pc.Vec3(0,0,0);
pista8Spline[2] = new pc.Vec3(-0.009177734,0.676275,25.92472);
pista8Spline[3] = new pc.Vec3(-0.01133264,0.6778625,51.90183);
pista8Spline[4] = new pc.Vec3(-0.0142813,0.6810375,77.87894);
pista8Spline[5] = new pc.Vec3(-0.01246436,0.8683625,103.8553);
pista8Spline[6] = new pc.Vec3(-0.01222871,1.310481,129.8284);
pista8Spline[7] = new pc.Vec3(-0.009599414,1.9558,155.7984);
pista8Spline[8] = new pc.Vec3(-0.004576465,2.763838,181.7621);
pista8Spline[9] = new pc.Vec3(0.006796484,3.692525,207.7233);
pista8Spline[10] = new pc.Vec3(0.08565059,4.657725,233.6824);
pista8Spline[11] = new pc.Vec3(0.9431734,5.158581,259.6393);
pista8Spline[12] = new pc.Vec3(2.758058,5.102225,285.5499);
pista8Spline[13] = new pc.Vec3(5.524078,4.52755,311.3714);
pista8Spline[14] = new pc.Vec3(9.249271,3.464719,337.0562);
pista8Spline[15] = new pc.Vec3(13.96705,1.943894,362.5527);
pista8Spline[16] = new pc.Vec3(19.73635,-0.0079375,387.804);
pista8Spline[17] = new pc.Vec3(26.64936,-2.3622,412.7313);
pista8Spline[18] = new pc.Vec3(34.84771,-5.078413,437.2258);
pista8Spline[19] = new pc.Vec3(44.56222,-8.117682,461.1197);
pista8Spline[20] = new pc.Vec3(56.24389,-11.43159,484.0719);
pista8Spline[21] = new pc.Vec3(70.57112,-14.89234,505.4367);
pista8Spline[22] = new pc.Vec3(88.47574,-18.20386,523.9053);
pista8Spline[23] = new pc.Vec3(107.9209,-21.44871,540.8201);
pista8Spline[24] = new pc.Vec3(127.7184,-24.83723,557.2956);
pista8Spline[25] = new pc.Vec3(147.6453,-28.41942,573.5729);
pista8Spline[26] = new pc.Vec3(167.5023,-32.23101,589.8806);
pista8Spline[27] = new pc.Vec3(187.0903,-36.3093,606.4489);
pista8Spline[28] = new pc.Vec3(206.1673,-40.70191,623.522);
pista8Spline[29] = new pc.Vec3(224.4404,-45.45806,641.3572);
pista8Spline[30] = new pc.Vec3(241.4962,-50.63649,660.2458);
pista8Spline[31] = new pc.Vec3(256.7664,-56.29593,680.4705);
pista8Spline[32] = new pc.Vec3(269.4384,-62.4713,702.2637);
pista8Spline[33] = new pc.Vec3(278.5065,-69.1642,725.6343);
pista8Spline[34] = new pc.Vec3(282.9774,-76.5175,750.0999);
pista8Spline[35] = new pc.Vec3(283.4061,-84.47008,774.8179);
pista8Spline[36] = new pc.Vec3(281.6528,-92.99654,799.281);
pista8Spline[37] = new pc.Vec3(276.6457,-101.904,823.1353);
pista8Spline[38] = new pc.Vec3(267.3145,-110.8011,845.6419);
pista8Spline[39] = new pc.Vec3(253.5905,-119.1855,865.9968);
pista8Spline[40] = new pc.Vec3(236.4909,-126.7627,883.9915);
pista8Spline[41] = new pc.Vec3(217.0752,-133.5159,899.8575);
pista8Spline[42] = new pc.Vec3(196.4347,-139.6008,914.4039);
pista8Spline[43] = new pc.Vec3(175.203,-145.1801,928.2925);
pista8Spline[44] = new pc.Vec3(153.7191,-150.368,941.9436);
pista8Spline[45] = new pc.Vec3(132.1645,-155.2289,955.5994);
pista8Spline[46] = new pc.Vec3(110.679,-159.8835,969.4382);
pista8Spline[47] = new pc.Vec3(89.40224,-164.496,983.6101);
pista8Spline[48] = new pc.Vec3(68.41629,-169.0949,998.2133);
pista8Spline[49] = new pc.Vec3(47.82989,-173.7106,1013.368);
pista8Spline[50] = new pc.Vec3(27.78323,-178.3802,1029.213);
pista8Spline[51] = new pc.Vec3(8.460878,-183.1475,1045.905);
pista8Spline[52] = new pc.Vec3(-9.898955,-188.0537,1063.609);
pista8Spline[53] = new pc.Vec3(-27.01558,-193.1329,1082.47);
pista8Spline[54] = new pc.Vec3(-42.57625,-198.3938,1102.586);
pista8Spline[55] = new pc.Vec3(-55.65448,-203.9453,1124.298);
pista8Spline[56] = new pc.Vec3(-64.85334,-209.8651,1147.827);
pista8Spline[57] = new pc.Vec3(-70.2183,-215.9474,1172.484);
pista8Spline[58] = new pc.Vec3(-72.26647,-222.0014,1197.647);
pista8Spline[59] = new pc.Vec3(-71.29145,-227.9287,1222.9);
pista8Spline[60] = new pc.Vec3(-66.69206,-233.6816,1247.789);
pista8Spline[61] = new pc.Vec3(-58.71031,-239.1464,1271.879);
pista8Spline[62] = new pc.Vec3(-47.84606,-244.2369,1294.904);
pista8Spline[63] = new pc.Vec3(-34.7086,-248.9327,1316.805);
pista8Spline[64] = new pc.Vec3(-19.63529,-253.2285,1337.511);
pista8Spline[65] = new pc.Vec3(-2.872085,-257.0879,1356.963);
pista8Spline[66] = new pc.Vec3(15.45173,-260.5222,1375.046);
pista8Spline[67] = new pc.Vec3(35.01261,-263.5022,1391.87);
pista8Spline[68] = new pc.Vec3(55.59941,-266.0438,1407.502);
pista8Spline[69] = new pc.Vec3(77.00228,-268.1605,1422.066);
pista8Spline[70] = new pc.Vec3(99.02031,-269.8823,1435.74);
pista8Spline[71] = new pc.Vec3(121.0792,-271.4204,1449.372);
pista8Spline[72] = new pc.Vec3(142.8511,-272.869,1463.466);
pista8Spline[73] = new pc.Vec3(164.0992,-274.2517,1478.343);
pista8Spline[74] = new pc.Vec3(184.4953,-275.6001,1494.367);
pista8Spline[75] = new pc.Vec3(203.5679,-276.9441,1511.938);
pista8Spline[76] = new pc.Vec3(220.6569,-278.357,1531.432);
pista8Spline[77] = new pc.Vec3(234.9202,-280.0342,1553.047);
pista8Spline[78] = new pc.Vec3(245.5892,-282.1218,1576.609);
pista8Spline[79] = new pc.Vec3(252.2942,-284.7396,1601.539);
pista8Spline[80] = new pc.Vec3(255.0398,-287.9382,1627.148);
pista8Spline[81] = new pc.Vec3(254.3758,-291.7537,1652.82);
pista8Spline[82] = new pc.Vec3(250.6692,-296.2495,1678.115);
pista8Spline[83] = new pc.Vec3(243.2588,-301.3506,1702.454);
pista8Spline[84] = new pc.Vec3(231.8407,-306.8614,1725.092);
pista8Spline[85] = new pc.Vec3(216.8192,-312.4333,1745.507);
pista8Spline[86] = new pc.Vec3(199.0646,-317.7794,1763.679);
pista8Spline[87] = new pc.Vec3(179.4615,-322.8713,1779.932);
pista8Spline[88] = new pc.Vec3(158.6712,-327.7505,1794.716);
pista8Spline[89] = new pc.Vec3(137.1284,-332.4775,1808.438);
pista8Spline[90] = new pc.Vec3(115.1066,-337.112,1821.413);
pista8Spline[91] = new pc.Vec3(92.78362,-341.6983,1833.881);
pista8Spline[92] = new pc.Vec3(70.33141,-346.3234,1846.1);
pista8Spline[93] = new pc.Vec3(48.06811,-351.2159,1858.559);
pista8Spline[94] = new pc.Vec3(26.20189,-356.4733,1871.557);
pista8Spline[95] = new pc.Vec3(5.0546,-362.2231,1885.498);
pista8Spline[96] = new pc.Vec3(-14.80423,-368.6304,1900.951);
pista8Spline[97] = new pc.Vec3(-32.50843,-375.8438,1918.508);
pista8Spline[98] = new pc.Vec3(-47.18745,-383.8367,1938.362);
pista8Spline[99] = new pc.Vec3(-58.61764,-392.3611,1960.046);
pista8Spline[100] = new pc.Vec3(-66.35512,-401.22,1983.181);
pista8Spline[101] = new pc.Vec3(-70.39034,-410.1725,2007.208);
pista8Spline[102] = new pc.Vec3(-71.1958,-419.0246,2031.601);
pista8Spline[103] = new pc.Vec3(-67.5636,-427.4318,2055.797);
pista8Spline[104] = new pc.Vec3(-56.85373,-434.5676,2078.297);
pista8Spline[105] = new pc.Vec3(-42.00188,-440.7132,2098.672);
pista8Spline[106] = new pc.Vec3(-24.49116,-446.1579,2117.053);
pista8Spline[107] = new pc.Vec3(-5.021858,-451.0353,2133.528);
pista8Spline[108] = new pc.Vec3(15.73748,-455.3998,2148.515);
pista8Spline[109] = new pc.Vec3(37.23203,-459.4167,2162.536);
pista8Spline[110] = new pc.Vec3(59.09151,-463.2249,2176.042);
pista8Spline[111] = new pc.Vec3(81.05755,-466.9395,2189.404);
pista8Spline[112] = new pc.Vec3(102.9004,-470.6551,2202.964);
pista8Spline[113] = new pc.Vec3(124.4044,-474.4506,2217.031);
pista8Spline[114] = new pc.Vec3(145.3283,-478.3885,2231.911);
pista8Spline[115] = new pc.Vec3(165.3732,-482.5192,2247.903);
pista8Spline[116] = new pc.Vec3(184.1704,-486.8684,2265.285);
pista8Spline[117] = new pc.Vec3(201.269,-491.4197,2284.291);
pista8Spline[118] = new pc.Vec3(216.2985,-496.118,2304.938);
pista8Spline[119] = new pc.Vec3(228.8421,-500.8735,2327.162);
pista8Spline[120] = new pc.Vec3(238.2931,-505.5447,2350.881);
pista8Spline[121] = new pc.Vec3(244.143,-509.9443,2375.78);
pista8Spline[122] = new pc.Vec3(246.2318,-514.2012,2401.299);
pista8Spline[123] = new pc.Vec3(245.1888,-518.2892,2426.917);
pista8Spline[124] = new pc.Vec3(240.8245,-522.1151,2452.216);
pista8Spline[125] = new pc.Vec3(232.8323,-525.6082,2476.66);
pista8Spline[126] = new pc.Vec3(221.2663,-528.707,2499.686);
pista8Spline[127] = new pc.Vec3(206.5764,-531.3876,2520.919);
pista8Spline[128] = new pc.Vec3(189.4548,-533.6732,2540.304);
pista8Spline[129] = new pc.Vec3(170.6104,-535.6189,2558.068);
pista8Spline[130] = new pc.Vec3(150.6514,-537.2888,2574.609);
pista8Spline[131] = new pc.Vec3(130.0774,-538.7455,2590.399);
pista8Spline[132] = new pc.Vec3(109.302,-540.0425,2605.941);
pista8Spline[133] = new pc.Vec3(88.73431,-541.2252,2621.763);
pista8Spline[134] = new pc.Vec3(68.89234,-542.3364,2638.483);
pista8Spline[135] = new pc.Vec3(50.61426,-543.4171,2656.896);
pista8Spline[136] = new pc.Vec3(34.30786,-544.4236,2677.077);
pista8Spline[137] = new pc.Vec3(20.46307,-545.3206,2699.019);
pista8Spline[138] = new pc.Vec3(9.820474,-546.1216,2722.675);
pista8Spline[139] = new pc.Vec3(1.783755,-546.7481,2747.366);
pista8Spline[140] = new pc.Vec3(-4.576961,-547.1725,2772.543);
pista8Spline[141] = new pc.Vec3(-9.364464,-547.469,2798.069);
pista8Spline[142] = new pc.Vec3(-12.73155,-547.6772,2823.824);
pista8Spline[143] = new pc.Vec3(-14.83935,-547.8377,2849.713);
pista8Spline[144] = new pc.Vec3(-15.86925,-547.9619,2875.668);
pista8Spline[145] = new pc.Vec3(-16.05399,-548.0026,2901.644);
pista8Spline[146] = new pc.Vec3(-16.05538,-548.004,2927.621);
pista8Spline[147] = new pc.Vec3(-16.05677,-548.0048,2953.599);
pista8Spline[148] = new pc.Vec3(-16.05419,-548.0046,2979.576);
pista8Spline[149] = new pc.Vec3(-16.05082,-548.0044,3005.553);
pista8Spline[150] = new pc.Vec3(-16.05756,-547.3275,3031.479);


var pista8Spline_angulos = [];
pista8Spline_angulos[0] = new pc.Vec3(0,0,0);
pista8Spline_angulos[1] = new pc.Vec3(270,89.8692,0);
pista8Spline_angulos[2] = new pc.Vec3(270,89.99709,0);
pista8Spline_angulos[3] = new pc.Vec3(270,89.99709,0);
pista8Spline_angulos[4] = new pc.Vec3(270.0969,90.09989,0);
pista8Spline_angulos[5] = new pc.Vec3(270.7166,180.0001,269.9999);
pista8Spline_angulos[6] = new pc.Vec3(271.2071,180.0117,269.9999);
pista8Spline_angulos[7] = new pc.Vec3(271.6216,180.0121,269.9997);
pista8Spline_angulos[8] = new pc.Vec3(271.9333,180.0177,269.9999);
pista8Spline_angulos[9] = new pc.Vec3(272.1565,180.0235,270);
pista8Spline_angulos[10] = new pc.Vec3(271.7541,180.8171,270);
pista8Spline_angulos[11] = new pc.Vec3(270.4698,182.9624,270.0001);
pista8Spline_angulos[12] = new pc.Vec3(270.708,5.047751,89.99986);
pista8Spline_angulos[13] = new pc.Vec3(271.8214,7.178236,89.99998);
pista8Spline_angulos[14] = new pc.Vec3(272.8651,9.347209,90.00007);
pista8Spline_angulos[15] = new pc.Vec3(273.8394,11.64487,89.99996);
pista8Spline_angulos[16] = new pc.Vec3(274.7617,14.12777,90);
pista8Spline_angulos[17] = new pc.Vec3(275.614,16.92258,90.00001);
pista8Spline_angulos[18] = new pc.Vec3(276.3755,20.17001,89.99998);
pista8Spline_angulos[19] = new pc.Vec3(277.0562,24.32073,89.99999);
pista8Spline_angulos[20] = new pc.Vec3(277.5626,29.97425,90);
pista8Spline_angulos[21] = new pc.Vec3(277.6459,38.34315,90.00002);
pista8Spline_angulos[22] = new pc.Vec3(277.0644,48.19538,90);
pista8Spline_angulos[23] = new pc.Vec3(277.3079,49.73408,90.00002);
pista8Spline_angulos[24] = new pc.Vec3(277.6989,50.59904,89.99998);
pista8Spline_angulos[25] = new pc.Vec3(278.1678,50.78494,89.99998);
pista8Spline_angulos[26] = new pc.Vec3(278.7222,50.30703,90.00002);
pista8Spline_angulos[27] = new pc.Vec3(279.3649,49.09583,89.99998);
pista8Spline_angulos[28] = new pc.Vec3(280.1251,47.09986,90.00001);
pista8Spline_angulos[29] = new pc.Vec3(281.0041,44.08374,89.99998);
pista8Spline_angulos[30] = new pc.Vec3(282.0204,39.83442,89.99999);
pista8Spline_angulos[31] = new pc.Vec3(283.1668,33.94781,89.99999);
pista8Spline_angulos[32] = new pc.Vec3(284.338,26.02806,90);
pista8Spline_angulos[33] = new pc.Vec3(285.6557,16.04348,90);
pista8Spline_angulos[34] = new pc.Vec3(287.1353,4.554802,90);
pista8Spline_angulos[35] = new pc.Vec3(288.5376,358.7358,89.99999);
pista8Spline_angulos[36] = new pc.Vec3(289.7234,352.5452,90);
pista8Spline_angulos[37] = new pc.Vec3(290.2328,343.1781,90);
pista8Spline_angulos[38] = new pc.Vec3(289.6084,331.6076,89.99998);
pista8Spline_angulos[39] = new pc.Vec3(287.9386,320.8377,90);
pista8Spline_angulos[40] = new pc.Vec3(285.9665,312.3501,90.00002);
pista8Spline_angulos[41] = new pc.Vec3(284.2436,306.7789,90.00002);
pista8Spline_angulos[42] = new pc.Vec3(282.9263,303.9341,90.00002);
pista8Spline_angulos[43] = new pc.Vec3(281.9304,302.6591,90.00002);
pista8Spline_angulos[44] = new pc.Vec3(281.1324,302.3158,90.00001);
pista8Spline_angulos[45] = new pc.Vec3(280.453,302.4689,90.00001);
pista8Spline_angulos[46] = new pc.Vec3(280.262,303.1826,90);
pista8Spline_angulos[47] = new pc.Vec3(280.2028,304.1959,89.99999);
pista8Spline_angulos[48] = new pc.Vec3(280.2053,305.5255,89.99999);
pista8Spline_angulos[49] = new pc.Vec3(280.2777,307.2628,90.00004);
pista8Spline_angulos[50] = new pc.Vec3(280.4469,309.4729,89.99999);
pista8Spline_angulos[51] = new pc.Vec3(280.7169,312.2846,89.99999);
pista8Spline_angulos[52] = new pc.Vec3(281.0786,315.7535,90.00002);
pista8Spline_angulos[53] = new pc.Vec3(281.4838,319.9365,90.00001);
pista8Spline_angulos[54] = new pc.Vec3(281.8849,324.7279,89.99999);
pista8Spline_angulos[55] = new pc.Vec3(282.8243,333.7934,90);
pista8Spline_angulos[56] = new pc.Vec3(283.4355,343.3725,90);
pista8Spline_angulos[57] = new pc.Vec3(283.5753,351.8186,90);
pista8Spline_angulos[58] = new pc.Vec3(283.3305,358.6172,90.00001);
pista8Spline_angulos[59] = new pc.Vec3(283.0383,6.321208,90.00001);
pista8Spline_angulos[60] = new pc.Vec3(282.509,14.53253,90.00001);
pista8Spline_angulos[61] = new pc.Vec3(281.7419,21.95745,89.99999);
pista8Spline_angulos[62] = new pc.Vec3(280.8493,28.34453,90.00002);
pista8Spline_angulos[63] = new pc.Vec3(279.976,33.51137,90);
pista8Spline_angulos[64] = new pc.Vec3(279.1248,38.85965,90);
pista8Spline_angulos[65] = new pc.Vec3(278.1201,43.78925,90.00002);
pista8Spline_angulos[66] = new pc.Vec3(277.0813,47.38266,89.99999);
pista8Spline_angulos[67] = new pc.Vec3(276.0966,51.13027,89.99998);
pista8Spline_angulos[68] = new pc.Vec3(275.1341,54.34882,90.00001);
pista8Spline_angulos[69] = new pc.Vec3(274.2177,57.09093,90.00002);
pista8Spline_angulos[70] = new pc.Vec3(273.5005,58.67594,89.99997);
pista8Spline_angulos[71] = new pc.Vec3(273.2903,57.81242,90.00003);
pista8Spline_angulos[72] = new pc.Vec3(273.114,56.21363,89.99999);
pista8Spline_angulos[73] = new pc.Vec3(273.002,53.61257,89.99998);
pista8Spline_angulos[74] = new pc.Vec3(272.958,49.85247,89.99995);
pista8Spline_angulos[75] = new pc.Vec3(272.9963,44.56731,90.00002);
pista8Spline_angulos[76] = new pc.Vec3(273.3435,37.60088,90.00001);
pista8Spline_angulos[77] = new pc.Vec3(274.1074,29.00917,89.99996);
pista8Spline_angulos[78] = new pc.Vec3(275.1672,19.70082,90.00003);
pista8Spline_angulos[79] = new pc.Vec3(276.4177,10.45821,90);
pista8Spline_angulos[80] = new pc.Vec3(277.7348,1.96885,90.00005);
pista8Spline_angulos[81] = new pc.Vec3(279.2108,355.3344,90.00003);
pista8Spline_angulos[82] = new pc.Vec3(280.6907,347.6593,90);
pista8Spline_angulos[83] = new pc.Vec3(281.8758,338.2443,90);
pista8Spline_angulos[84] = new pc.Vec3(282.4876,328.2632,90);
pista8Spline_angulos[85] = new pc.Vec3(282.1711,319.3413,90);
pista8Spline_angulos[86] = new pc.Vec3(281.5823,312.355,90);
pista8Spline_angulos[87] = new pc.Vec3(281.041,307.2841,89.99998);
pista8Spline_angulos[88] = new pc.Vec3(280.6397,303.7813,89.99998);
pista8Spline_angulos[89] = new pc.Vec3(280.3583,301.3811,90.00001);
pista8Spline_angulos[90] = new pc.Vec3(280.2039,299.7567,90.00002);
pista8Spline_angulos[91] = new pc.Vec3(280.1421,298.6795,90.00001);
pista8Spline_angulos[92] = new pc.Vec3(280.5117,298.7621,90.00001);
pista8Spline_angulos[93] = new pc.Vec3(281.2344,299.8322,90);
pista8Spline_angulos[94] = new pc.Vec3(282.1748,301.8244,90.00002);
pista8Spline_angulos[95] = new pc.Vec3(283.4601,305.2726,90);
pista8Spline_angulos[96] = new pc.Vec3(285.1641,310.942,90.00001);
pista8Spline_angulos[97] = new pc.Vec3(287.0824,318.9867,90);
pista8Spline_angulos[98] = new pc.Vec3(288.619,327.8405,90);
pista8Spline_angulos[99] = new pc.Vec3(289.6295,336.8026,90);
pista8Spline_angulos[100] = new pc.Vec3(290.1426,346.1535,89.99999);
pista8Spline_angulos[101] = new pc.Vec3(290.1071,354.5359,90.00001);
pista8Spline_angulos[102] = new pc.Vec3(289.7168,1.142159,89.99999);
pista8Spline_angulos[103] = new pc.Vec3(287.3869,18.21391,90);
pista8Spline_angulos[104] = new pc.Vec3(284.6798,31.51483,90);
pista8Spline_angulos[105] = new pc.Vec3(282.807,40.15235,89.99998);
pista8Spline_angulos[106] = new pc.Vec3(281.4627,46.86974,90.00001);
pista8Spline_angulos[107] = new pc.Vec3(280.189,52.27053,90.00001);
pista8Spline_angulos[108] = new pc.Vec3(279.2244,55.77505,90);
pista8Spline_angulos[109] = new pc.Vec3(278.6183,57.78175,89.99999);
pista8Spline_angulos[110] = new pc.Vec3(278.2913,58.64529,90.00002);
pista8Spline_angulos[111] = new pc.Vec3(278.1949,58.56654,89.99999);
pista8Spline_angulos[112] = new pc.Vec3(278.2825,57.62577,90.00003);
pista8Spline_angulos[113] = new pc.Vec3(278.5439,55.84523,89.99999);
pista8Spline_angulos[114] = new pc.Vec3(278.9255,53.13965,89.99998);
pista8Spline_angulos[115] = new pc.Vec3(279.3888,49.51208,89.99999);
pista8Spline_angulos[116] = new pc.Vec3(279.8774,44.78914,90);
pista8Spline_angulos[117] = new pc.Vec3(280.2794,39.02534,90.00001);
pista8Spline_angulos[118] = new pc.Vec3(280.529,32.94421,89.99999);
pista8Spline_angulos[119] = new pc.Vec3(280.5158,25.74055,90.00001);
pista8Spline_angulos[120] = new pc.Vec3(280.114,17.56963,89.99998);
pista8Spline_angulos[121] = new pc.Vec3(279.5065,8.858453,90.00003);
pista8Spline_angulos[122] = new pc.Vec3(279.2884,1.035954,90.00001);
pista8Spline_angulos[123] = new pc.Vec3(278.7872,354.1099,89.99998);
pista8Spline_angulos[124] = new pc.Vec3(278.1279,346.1559,89.99998);
pista8Spline_angulos[125] = new pc.Vec3(277.3081,337.587,89.99999);
pista8Spline_angulos[126] = new pc.Vec3(276.3881,329.1675,90.00002);
pista8Spline_angulos[127] = new pc.Vec3(275.4753,321.6886,90.00002);
pista8Spline_angulos[128] = new pc.Vec3(274.6492,315.6753,89.99998);
pista8Spline_angulos[129] = new pc.Vec3(273.9703,311.2202,90.00002);
pista8Spline_angulos[130] = new pc.Vec3(273.4273,308.3533,89.99994);
pista8Spline_angulos[131] = new pc.Vec3(273.0196,306.9111,89.99997);
pista8Spline_angulos[132] = new pc.Vec3(272.7226,306.9405,89.99996);
pista8Spline_angulos[133] = new pc.Vec3(272.5122,308.4661,89.99996);
pista8Spline_angulos[134] = new pc.Vec3(272.4315,312.444,90);
pista8Spline_angulos[135] = new pc.Vec3(272.3183,318.0407,90.00005);
pista8Spline_angulos[136] = new pc.Vec3(272.1053,324.219,90.00005);
pista8Spline_angulos[137] = new pc.Vec3(271.857,331.5246,90);
pista8Spline_angulos[138] = new pc.Vec3(271.6495,339.8501,90.00006);
pista8Spline_angulos[139] = new pc.Vec3(271.1281,343.9247,90);
pista8Spline_angulos[140] = new pc.Vec3(270.7828,347.6533,90.00025);
pista8Spline_angulos[141] = new pc.Vec3(270.5439,351.0266,90);
pista8Spline_angulos[142] = new pc.Vec3(270.3947,354.0073,90);
pista8Spline_angulos[143] = new pc.Vec3(270.3214,356.6018,90.00031);
pista8Spline_angulos[144] = new pc.Vec3(270.2056,358.7709,90.00047);
pista8Spline_angulos[145] = new pc.Vec3(270,89.98386,0);
pista8Spline_angulos[146] = new pc.Vec3(270,89.98827,0);
pista8Spline_angulos[147] = new pc.Vec3(270,90.00149,0);
pista8Spline_angulos[148] = new pc.Vec3(270,90.00588,0);
pista8Spline_angulos[149] = new pc.Vec3(270,89.99854,0);
pista8Spline_angulos[150] = new pc.Vec3(275.5223,179.929,270);

