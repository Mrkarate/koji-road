 var CameraPath = pc.createScript('cameraPath');
CameraPath.attributes.add("velocidad", {type: "number", title: "velocidad"});
CameraPath.attributes.add("pistas", {type: "entity", title: "pistas"});
CameraPath.attributes.add("distanciaEntrePistas", {type: "number", title: "distanciaEntrePistas"});
CameraPath.attributes.add("bajadaEntrePistas", {type: "number", title: "bajadaEntrePistas"});
CameraPath.attributes.add('bolasRojas', {type: 'entity', title: 'bolasRojas'});
CameraPath.attributes.add('bolasVerdes', {type: 'entity', title: 'bolasVerdes'});
CameraPath.attributes.add('bolasAmarillas', {type: 'entity', title: 'bolasAmarillas'});
CameraPath.attributes.add('bolasBi', {type: 'entity', title: 'bolasBi'});
CameraPath.attributes.add('rampasRojas', {type: 'entity', title: 'rampasRojas'});
CameraPath.attributes.add('rampasVerdes', {type: 'entity', title: 'rampasVerdes'});
CameraPath.attributes.add('rampasAmarillas', {type: 'entity', title: 'rampasAmarillas'});
CameraPath.attributes.add('vientos', {type: 'entity', title: 'vientos'});


CameraPath.prototype.initialize = function() {
    
    
    this.enPause = false;
    this.control = this.entity.script.controlBola;     
    this.velocidad = window.gameplay.ballSpeed;
    /*var textura1= getTexture(window.gameplay.roadTexture1);
    var textura2 = getTexture(window.gameplay.roadTexture2);
    
    cambiaTextura3D(textura1,this.pistas.children[1]);
    cambiaTextura3D(textura1,this.pistas.children[1].children[0]);
    cambiaTextura3D(textura2,this.pistas.children[0]);
    cambiaTextura3D(textura2,this.pistas.children[0].children[0]);*/
    
};
CameraPath.prototype.apagaPistas = function()
{
   for(var i = 0; i < this.pistas.children.length; i++)
       this.pistas.children[i].enabled = false;
};
CameraPath.prototype.start = function(moverse)
{
    this.velocidad = window.gameplay.ballSpeed;
    this.createPath();
    this.entity.setPosition(this.px.value(0), this.py.value(0), this.pz.value(0));
    this.entity.lookAt(this.px.value(0.01), this.py.value(0.01), this.pz.value(0.01));
    if(moverse)
        this.empezar();
};
CameraPath.prototype.empezar = function()
{
    this.velocidad = window.gameplay.ballSpeed*(1 + (currentLevelCont*0.022));
    this.pathListo = false;  
    this.setPistas();
    idNoRev = -1;
    tocaSonido("empezar");
    
    //this.poneMiniMapa();   
    var titulo = _2dScreen.findByName("roadTexto"); 
    titulo.enabled = true;
    titulo.element.text = road_texto+" "+ (currentLevelCont +1);    
    titulo.script.animaMsg.pone(this.sacaTitulo.bind(this));
    
    var scoreTXT = _2dScreen.findByName("score");
    if(!battle_mode)
        scoreTXT.enabled = true;
    
    setTexto(scoreTXT.element,score);
};
CameraPath.prototype.sacaTitulo = function()
{
    var titulo = _2dScreen.findByName("roadTexto");
    setTimeout(titulo.script.animaMsg.sacaInverso.bind(titulo.script.animaMsg,true),1000);  
};
CameraPath.prototype.poneMiniMapa = function()
{
   var miniMapa = _2dScreen.findByName("miniMapa");
   var bolasMapa = _2dScreen.findByName("bolasMapa");
    
   miniMapa.children[0].element.text = "ROAD " + (currentLevelCont +1);
   //miniMapa.script.animaMsg.clickBoton(null,false,currentLevel-1); 
   
   miniMapa.enabled = true;
   bolasMapa.enabled = true;  
   bolasMapa.script.animaMsg.clickBoton(null,false,this.control.currentColor);
};
CameraPath.prototype.setMiniMapa = function(p)
{
    return;
   var miniMapa = _2dScreen.findByName("miniMapa");
   var bolasMapa = _2dScreen.findByName("bolasMapa");
   p = p*100;
   var x1 = 0;
   var y1 = 0;
    
   var y2 = 100;
   var x2 = 101;
    
    var M = (y2-y1)/(x2-x1);
    
    var Y = M*(p-x1) + y1;
    
    var P = Math.round(Y);
    
    if(P > 101)
        P = 101;
   
    var arrayPuntos = window["puntosMapaLevel"+currentLevel];
    var pos = bolasMapa.getLocalPosition();
    bolasMapa.setLocalPosition(arrayPuntos[P].x,arrayPuntos[P].y,pos.z);
    
};
// update code called every frame
CameraPath.prototype.update = function(dt) {
       
    if(!this.pathListo)
        return;
    
    if(enPause)
    {
        if(!this.enPause)
        {
            forceMute(true);
            this.enPause = true;
        }
        return;
    }
    else
    {
        if(this.enPause)
        {
            this.enPause = false;
            forceMute(false);
        }
    }
 
    this.porcentaje = this.porcentaje + this.velocidad*dt;
    var percent = this.porcentaje;
  
  
    this.entity.setPosition(this.px.value(percent), this.py.value(percent), this.pz.value(percent));
    this.entity.lookAt(this.px.value(percent+0.01), this.py.value(percent+0.01), this.pz.value(percent+0.01));
    this.setMiniMapa(percent);
    
    
    if(percent > 1)
    {
         this.entity.lookAt(this.px.value(percent), this.py.value(percent), this.pz.value(percent) + 0.25);
        this.pathListo = false;
        currentLevel = getLevel(false);//currentLevel + 1;
        currentLevelCont = currentLevelCont + 1;
        if(currentLevel == totalLevels + 1)
            currentLevel = 1;       
        
        /*if(currentLevelCont == window.gameplay.numLevels)
        {
            currentLevelCont = currentLevelCont - 1;
            this.control.finGamePlay();
            return;
        }*/
        
        this.createPath();        
        var puntoInicial = new pc.Vec3();
        var levelPuntos =  arrayLevels[currentLevelCont][1];
        var levelPos = arrayLevels[currentLevelCont][0];
        puntoInicial = new pc.Vec3(levelPos.x + levelPuntos[1].x ,levelPos.y + levelPuntos[1].y ,levelPos.z + levelPuntos[1].z);      
        this.control.saltoGrande(puntoInicial,0);
    }
    else
    {       
        for(var i = 0; i < vientosPuestos.length; i++)
        {
           var posBola =  this.control.bolaSkin.getPosition();
           var posObjeto = vientosPuestos[i].getPosition();
           var distancia = new pc.Vec3();
           distancia.sub2(posBola,posObjeto);
           distancia = distancia.length();
           ////console.log("mi distancia con el viento " + distancia);
           if(distancia <= 360)
               vientosPuestos[i].enabled = true;
            
        }
    }
  
};
CameraPath.prototype.setPistas = function()
{
    if(currentLevelCont > 0)
        arrayLevels[currentLevelCont-1].enabled = false;
    
    this.porcentaje = 0;
    this.pathListo = true;    
};


CameraPath.prototype.createPath = function () {
    this.pathListo = false;
    var curveMode = pc.CURVE_CARDINAL;
    
    // Create curves for position
    this.px = new pc.Curve(); 
    this.px.type = curveMode;
    
    this.py = new pc.Curve(); 
    this.py.type = curveMode;    
    
    this.pz = new pc.Curve(); 
    this.pz.type = curveMode;
    
    // Create curves for target look at position
    this.tx = new pc.Curve();
    this.tx.type = curveMode;
    
    this.ty = new pc.Curve();
    this.ty.type = curveMode;
    
    this.tz = new pc.Curve();
    this.tz.type = curveMode;
    
    // Create curves for the 'up' vector for use with the lookAt function to 
    // allow for roll and avoid gimbal lock
    this.ux = new pc.Curve();
    this.ux.type = curveMode;
    
    this.uy = new pc.Curve();
    this.uy.type = curveMode;
    
    this.uz = new pc.Curve();
    this.uz.type = curveMode;
    
    
    var levelPuntos = window["pista"+currentLevel+"Spline"];   
    var level = this.pistas.children[currentLevel-1];   
    var puntoFinal = new pc.Vec3();   
    var bajada = 0;
    var extraY = -0.19;
    if(arrayLevels.length == 0)
        puntoFinal = new pc.Vec3(0,0,0);
    else
    {
        var levelAnterior = arrayLevels[currentLevelCont-1][1];
        var mcLvlPos = arrayLevels[currentLevelCont-1][0];        
        puntoFinal = new pc.Vec3(mcLvlPos.x + levelAnterior[levelAnterior.length -1].x ,mcLvlPos.y + levelAnterior[levelAnterior.length -1].y,mcLvlPos.z + levelAnterior[levelAnterior.length -1].z);       
        bajada = this.bajadaEntrePistas;
    }   
    level.setPosition(puntoFinal.x,puntoFinal.y - bajada,puntoFinal.z + this.distanciaEntrePistas);   
    arrayLevels.push([level.getPosition().clone(),levelPuntos]);
    
    var posLevel = level.getPosition(); 
    this.setObjetosPadres(posLevel);
    level.setPosition(posLevel.x,posLevel.y + extraY,posLevel.z);   
    // Get the total linear distance of the path (this isn't correct but gives a decent approximation in length)
    var pathLength = 0;
    
    // Store the distance from the start of the path for each path node
    var nodePathLength = [];
    
    // For use when calculating the distance between two nodes on the path
    var distanceBetween = new pc.Vec3();
    
    // Push 0 as we are starting our loop from 1 for ease
    nodePathLength.push(0);
    var k = 1;
    for (i = 2; i < levelPuntos.length; i++) {
        
        var prevNode = new pc.Vec3();
        prevNode.add2(posLevel,levelPuntos[i-1]);
        var nextNode = new pc.Vec3();
        nextNode.add2(posLevel,levelPuntos[i]);
        
        // Work out the distance between the current node and the one before in the path
        distanceBetween.sub2(prevNode, nextNode);
        pathLength += distanceBetween.length();        
        nodePathLength.push(pathLength);       
     
    }
    var vientosPuntos = window["vientoPista"+currentLevel];
    console.log("q mierda este error rq " + currentLevel);
    var angulos = window["pista"+currentLevel + "Spline_angulos"];
    vientosPuestos = [];
    objetosEnNivel = [];
    for (i = 1; i < levelPuntos.length; i++) {
        // Calculate the time for the curve key based on the distance of the path to the node
        // and the total path length so the speed of the camera travel stays relatively
        // consistent throughout
        var t = nodePathLength[i-1] / pathLength;
        var node = new pc.Vec3();
       
        node.add2(posLevel,levelPuntos[i]);
        
        var pos = node;
       
        this.px.add(t, pos.x);
        this.py.add(t, pos.y);
        this.pz.add(t, pos.z);
        
        if(i % 2 == 0)
        {
            this.creaObjetos(levelPuntos[i],i,k);
            k = k + 1;
        }
        var indexPunto = i;
        var punto = levelPuntos[i];
       for(var p = 0; p< vientosPuntos.length;p++)
       {
           
            if(vientosPuntos[p] == indexPunto)
            {                 
               //console.log("puse el viento rq!!! VAMO MIERDA!! LA WEA ");
               var pos = this.vientos.getPosition();
               this.vientos.children[indexVientos].setPosition(pos.x + punto.x,pos.y + punto.y,pos.z + punto.z);            
               pos = this.vientos.children[indexVientos].getLocalPosition();               
               this.vientos.children[indexVientos].enabled = false;                         
               this.vientos.children[indexVientos].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);       
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.vientos.children[indexVientos].rotateLocal(0,0,-anguloT);

               vientosPuestos.push(this.vientos.children[indexVientos]);
             
               indexVientos = this.aumentaIndexObjeto(indexVientos,limiteVientos);              

            }
       } 
    
        
        // Create and store a lookAt position based on the node position and the forward direction
       /* var lookAt = pos.clone().add(node.forward);
        this.tx.add(t, lookAt.x);
        this.ty.add(t, lookAt.y);
        this.tz.add(t, lookAt.z);
        
        var up = node.up;
        this.ux.add(t, up.x);
        this.uy.add(t, up.y);
        this.uz.add(t, up.z);*/
    }
    this.porcentaje = 0;
    level.enabled = true;
};
CameraPath.prototype.setObjetosPadres = function(punto)
{
    
    for(var i = 0; i < limiteBolas; i++)
    {
        if(i < limiteRampas)
        {
            this.rampasAmarillas.children[i].enabled = false;
            this.rampasRojas.children[i].enabled = false;
            this.rampasVerdes.children[i].enabled = false;
            
            //this.rampasAmarillas.children[i].children[0].script.cambiaTextura.cambio(currentLevel-1);
            //this.rampasRojas.children[i].children[0].script.cambiaTextura.cambio(currentLevel-1);
            //this.rampasVerdes.children[i].children[0].script.cambiaTextura.cambio(currentLevel-1);
        }       
        
        this.bolasAmarillas.children[i].enabled = false;
        this.bolasVerdes.children[i].enabled = false;
        this.bolasRojas.children[i].enabled = false;
        
        if(i < limiteBolasBi)
        {        
            this.bolasBi.children[i].enabled = false;            
        }
    }
    indexBolaAmarilla = 0;
    indexBolaRoja = 0;
    indexBolaVerde = 0;
    indexBolaBi = 0;   
    indexRampaAmarilla = 0;
    indexRampaRoja = 0;
    indexRampaVerde = 0;
    indexVientos = 0;
    this.vientos.setPosition(punto);
    this.bolasRojas.setPosition(punto);
    this.bolasVerdes.setPosition(punto);
    this.bolasAmarillas.setPosition(punto);
    this.bolasBi.setPosition(punto);  
    this.rampasVerdes.setPosition(punto);
    this.rampasRojas.setPosition(punto);
    this.rampasAmarillas.setPosition(punto);
};
CameraPath.prototype.creaObjetos = function(punto,indexPunto,index)
{
   var level = window["Level"+currentLevelObjeto];
   var angulos = window["pista"+currentLevel + "Spline_angulos"];
 
   if(index >= level.length)
       return;
    
   if(currentLevelCont == 0)
   {
       if(index == 2)
           return;
   }
    
   for(var i = 0; i < 3; i++)
   {
       var corrimientoX = 0;
       if(i == 0)
           corrimientoX = -3.8;
       else if(i == 2)
           corrimientoX = 3.8;
       var extraY = 0;      
      
       
       switch(level[index][i])
       {
           case "A":
               var pos = this.bolasAmarillas.getPosition();
               this.bolasAmarillas.children[indexBolaAmarilla].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               pos = this.bolasAmarillas.children[indexBolaAmarilla].getLocalPosition();               
               this.bolasAmarillas.children[indexBolaAmarilla].enabled = true;                         
               this.bolasAmarillas.children[indexBolaAmarilla].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                   
               this.bolasAmarillas.children[indexBolaAmarilla].translateLocal(0,0,corrimientoX); 
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasAmarillas.children[indexBolaAmarilla].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasAmarillas.children[indexBolaAmarilla],level[index][i],index]);
               indexBolaAmarilla = this.aumentaIndexObjeto(indexBolaAmarilla,limiteBolas);              
              
           break;
           case "V":
               var pos = this.bolasVerdes.getPosition();
               this.bolasVerdes.children[indexBolaVerde].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasVerdes.children[indexBolaVerde].enabled = true;
               this.bolasVerdes.children[indexBolaVerde].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);               
               this.bolasVerdes.children[indexBolaVerde].translateLocal(0,0,corrimientoX);
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasVerdes.children[indexBolaVerde].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasVerdes.children[indexBolaVerde],level[index][i],index]);
               indexBolaVerde = this.aumentaIndexObjeto(indexBolaVerde,limiteBolas);
               
           break;
           case "R":
               var pos = this.bolasRojas.getPosition();
               this.bolasRojas.children[indexBolaRoja].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasRojas.children[indexBolaRoja].enabled = true;
               this.bolasRojas.children[indexBolaRoja].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                
               this.bolasRojas.children[indexBolaRoja].translateLocal(0,0,corrimientoX);
                var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasRojas.children[indexBolaRoja].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasRojas.children[indexBolaRoja],level[index][i],index]);
               indexBolaRoja = this.aumentaIndexObjeto(indexBolaRoja,limiteBolas);              
           break;
               case "AI":
               var pos = this.bolasAmarillas.getPosition();
               this.bolasAmarillas.children[indexBolaAmarilla].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               pos = this.bolasAmarillas.children[indexBolaAmarilla].getLocalPosition();               
               this.bolasAmarillas.children[indexBolaAmarilla].enabled = true;                         
               this.bolasAmarillas.children[indexBolaAmarilla].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                   
               this.bolasAmarillas.children[indexBolaAmarilla].translateLocal(0,0,corrimientoX); 
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasAmarillas.children[indexBolaAmarilla].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasAmarillas.children[indexBolaAmarilla],level[index][i],index,corrimientoX]);
               indexBolaAmarilla = this.aumentaIndexObjeto(indexBolaAmarilla,limiteBolas);              
              
           break;
           case "VI":
               var pos = this.bolasVerdes.getPosition();
               this.bolasVerdes.children[indexBolaVerde].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasVerdes.children[indexBolaVerde].enabled = true;
               this.bolasVerdes.children[indexBolaVerde].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);               
               this.bolasVerdes.children[indexBolaVerde].translateLocal(0,0,corrimientoX);
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasVerdes.children[indexBolaVerde].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasVerdes.children[indexBolaVerde],level[index][i],index,corrimientoX]);
               indexBolaVerde = this.aumentaIndexObjeto(indexBolaVerde,limiteBolas);
               
           break;
           case "RI":
               var pos = this.bolasRojas.getPosition();
               this.bolasRojas.children[indexBolaRoja].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasRojas.children[indexBolaRoja].enabled = true;
               this.bolasRojas.children[indexBolaRoja].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                
               this.bolasRojas.children[indexBolaRoja].translateLocal(0,0,corrimientoX);
                var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasRojas.children[indexBolaRoja].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasRojas.children[indexBolaRoja],level[index][i],index,corrimientoX]);
               indexBolaRoja = this.aumentaIndexObjeto(indexBolaRoja,limiteBolas);              
           break;
           case "AD":
               var pos = this.bolasAmarillas.getPosition();
               this.bolasAmarillas.children[indexBolaAmarilla].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               pos = this.bolasAmarillas.children[indexBolaAmarilla].getLocalPosition();               
               this.bolasAmarillas.children[indexBolaAmarilla].enabled = true;                         
               this.bolasAmarillas.children[indexBolaAmarilla].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                   
               this.bolasAmarillas.children[indexBolaAmarilla].translateLocal(0,0,corrimientoX); 
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasAmarillas.children[indexBolaAmarilla].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasAmarillas.children[indexBolaAmarilla],level[index][i],index,corrimientoX]);
               indexBolaAmarilla = this.aumentaIndexObjeto(indexBolaAmarilla,limiteBolas);              
              
           break;
           case "VD":
               var pos = this.bolasVerdes.getPosition();
               this.bolasVerdes.children[indexBolaVerde].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasVerdes.children[indexBolaVerde].enabled = true;
               this.bolasVerdes.children[indexBolaVerde].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);               
               this.bolasVerdes.children[indexBolaVerde].translateLocal(0,0,corrimientoX);
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasVerdes.children[indexBolaVerde].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasVerdes.children[indexBolaVerde],level[index][i],index,corrimientoX]);
               indexBolaVerde = this.aumentaIndexObjeto(indexBolaVerde,limiteBolas);
               
           break;
           case "RD":
               var pos = this.bolasRojas.getPosition();
               this.bolasRojas.children[indexBolaRoja].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasRojas.children[indexBolaRoja].enabled = true;
               this.bolasRojas.children[indexBolaRoja].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                
               this.bolasRojas.children[indexBolaRoja].translateLocal(0,0,corrimientoX);
                var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasRojas.children[indexBolaRoja].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasRojas.children[indexBolaRoja],level[index][i],index,corrimientoX]);
               indexBolaRoja = this.aumentaIndexObjeto(indexBolaRoja,limiteBolas);              
           break;    
               
           case "RA":
               var pos = this.rampasAmarillas.getPosition();
               this.rampasAmarillas.children[indexRampaAmarilla].setPosition(pos.x + punto.x,pos.y + punto.y,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.rampasAmarillas.children[indexRampaAmarilla].enabled = true;
               this.rampasAmarillas.children[indexRampaAmarilla].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);               
                objetosEnNivel.push([this.rampasAmarillas.children[indexRampaAmarilla],level[index][i],index]);
               indexRampaAmarilla = this.aumentaIndexObjeto(indexRampaAmarilla,limiteRampas);
           break;
           case "RV":
               var pos = this.rampasVerdes.getPosition();
               this.rampasVerdes.children[indexRampaVerde].setPosition(pos.x + punto.x,pos.y + punto.y,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.rampasVerdes.children[indexRampaVerde].enabled = true;
               this.rampasVerdes.children[indexRampaVerde].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180); 
               objetosEnNivel.push([this.rampasVerdes.children[indexRampaVerde],level[index][i],index]);
               indexRampaVerde = this.aumentaIndexObjeto(indexRampaVerde,limiteRampas);
           break;
           case "RR":
               var pos = this.rampasRojas.getPosition();
               this.rampasRojas.children[indexRampaRoja].setPosition(pos.x + punto.x,pos.y + punto.y,pos.z + punto.z);
              // var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.rampasRojas.children[indexRampaRoja].enabled = true;
               this.rampasRojas.children[indexRampaRoja].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180); 
                objetosEnNivel.push([this.rampasRojas.children[indexRampaRoja],level[index][i],index]);
               indexRampaRoja = this.aumentaIndexObjeto(indexRampaRoja,limiteRampas);
           break;
           // colores
           case "CA":
               var pos = this.bolasBi.getPosition();
               this.bolasBi.children[indexBolaBi].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               pos = this.bolasBi.children[indexBolaBi].getLocalPosition();               
               this.bolasBi.children[indexBolaBi].enabled = true;                         
               this.bolasBi.children[indexBolaBi].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                   
               this.bolasBi.children[indexBolaBi].translateLocal(0,0,corrimientoX); 
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasBi.children[indexBolaBi].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasBi.children[indexBolaBi],level[index][i],index]);
               indexBolaBi = this.aumentaIndexObjeto(indexBolaBi,limiteBolasBi);              
              
           break;
           case "CV":
               var pos = this.bolasBi.getPosition();
               this.bolasBi.children[indexBolaBi].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasBi.children[indexBolaBi].enabled = true;
               this.bolasBi.children[indexBolaBi].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);               
               this.bolasBi.children[indexBolaBi].translateLocal(0,0,corrimientoX);
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasBi.children[indexBolaBi].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasBi.children[indexBolaBi],level[index][i],index]);
               indexBolaBi = this.aumentaIndexObjeto(indexBolaBi,limiteBolasBi);
               
           break;
           case "CR":
              
               
               var pos = this.bolasBi.getPosition();
               this.bolasBi.children[indexBolaBi].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasBi.children[indexBolaBi].enabled = true;
               
               this.bolasBi.children[indexBolaBi].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                
               this.bolasBi.children[indexBolaBi].translateLocal(0,0,corrimientoX);
                var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasBi.children[indexBolaBi].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasBi.children[indexBolaBi],level[index][i],index]);
               indexBolaBi = this.aumentaIndexObjeto(indexBolaBi,limiteBolasBi);              
           break;  
           case "CAI":
               var pos = this.bolasBi.getPosition();
               this.bolasBi.children[indexBolaBi].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               pos = this.bolasBi.children[indexBolaBi].getLocalPosition();               
               this.bolasBi.children[indexBolaBi].enabled = true;                         
               this.bolasBi.children[indexBolaBi].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                   
               this.bolasBi.children[indexBolaBi].translateLocal(0,0,corrimientoX); 
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasBi.children[indexBolaBi].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasBi.children[indexBolaBi],level[index][i],index,corrimientoX]);
               indexBolaBi = this.aumentaIndexObjeto(indexBolaBi,limiteBolasBi);              
              
           break;
           case "CVI":
               var pos = this.bolasBi.getPosition();
               this.bolasBi.children[indexBolaBi].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasBi.children[indexBolaBi].enabled = true;
               this.bolasBi.children[indexBolaBi].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);               
               this.bolasBi.children[indexBolaBi].translateLocal(0,0,corrimientoX);
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasBi.children[indexBolaBi].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasBi.children[indexBolaBi],level[index][i],index,corrimientoX]);
               indexBolaBi = this.aumentaIndexObjeto(indexBolaBi,limiteBolasBi);
               
           break;
           case "CRI":              
               
               var pos = this.bolasBi.getPosition();
               this.bolasBi.children[indexBolaBi].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasBi.children[indexBolaBi].enabled = true;
               
               this.bolasBi.children[indexBolaBi].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                
               this.bolasBi.children[indexBolaBi].translateLocal(0,0,corrimientoX);
                var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasBi.children[indexBolaBi].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasBi.children[indexBolaBi],level[index][i],index,corrimientoX]);
               indexBolaBi = this.aumentaIndexObjeto(indexBolaBi,limiteBolasBi);              
           break; 
           case "CAD":
               var pos = this.bolasBi.getPosition();
               this.bolasBi.children[indexBolaBi].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               pos = this.bolasBi.children[indexBolaBi].getLocalPosition();               
               this.bolasBi.children[indexBolaBi].enabled = true;                         
               this.bolasBi.children[indexBolaBi].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                   
               this.bolasBi.children[indexBolaBi].translateLocal(0,0,corrimientoX); 
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasBi.children[indexBolaBi].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasBi.children[indexBolaBi],level[index][i],index,corrimientoX]);
               indexBolaBi = this.aumentaIndexObjeto(indexBolaBi,limiteBolasBi);              
              
           break;
           case "CVD":
               var pos = this.bolasBi.getPosition();
               this.bolasBi.children[indexBolaBi].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasBi.children[indexBolaBi].enabled = true;
               this.bolasBi.children[indexBolaBi].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);               
               this.bolasBi.children[indexBolaBi].translateLocal(0,0,corrimientoX);
               var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasBi.children[indexBolaBi].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasBi.children[indexBolaBi],level[index][i],index,corrimientoX]);
               indexBolaBi = this.aumentaIndexObjeto(indexBolaBi,limiteBolasBi);
               
           break;
           case "CRD":              
               
               var pos = this.bolasBi.getPosition();
               this.bolasBi.children[indexBolaBi].setPosition(pos.x + punto.x,pos.y + punto.y+extraY,pos.z + punto.z);
               //var posAnterior = new pc.Vec3(pos.x + puntoAnterior.x,pos.y + puntoAnterior.y,pos.z + puntoAnterior.z);
               this.bolasBi.children[indexBolaBi].enabled = true;
               
               this.bolasBi.children[indexBolaBi].setEulerAngles(-180,360 - (angulos[indexPunto].y+angulos[indexPunto].z),180);                
               this.bolasBi.children[indexBolaBi].translateLocal(0,0,corrimientoX);
                var anguloT = angulos[indexPunto].x - 270;      
               if(anguloT > 0 && (angulos[indexPunto].y+angulos[indexPunto].z) < 180)
                   this.bolasBi.children[indexBolaBi].rotateLocal(0,0,-anguloT);
               objetosEnNivel.push([this.bolasBi.children[indexBolaBi],level[index][i],index,corrimientoX]);
               indexBolaBi = this.aumentaIndexObjeto(indexBolaBi,limiteBolasBi);              
           break;        
               
               
               
               
       }
   }
};
CameraPath.prototype.aumentaIndexObjeto = function(num,limite)
{
    num = num + 1;
    if(num == limite)
        num = 0;
    return(num);
};