var KojiComponent = pc.createScript('kojiComponent');

KojiComponent.attributes.add('element_mainLogo', {type: 'entity'});
KojiComponent.attributes.add('element_gotBall_msg1', {type: 'entity'});
KojiComponent.attributes.add('element_gotBall_msg2', {type: 'entity'});
KojiComponent.attributes.add('element_gotBall_msg3', {type: 'entity'});
KojiComponent.attributes.add('element_gotBall_msg4', {type: 'entity'});
KojiComponent.attributes.add('element_tapToStart', {type: 'entity'});
KojiComponent.attributes.add('element_endScreen', {type: 'entity'});
KojiComponent.attributes.add('element_tutorial', {type: 'entity'});
KojiComponent.attributes.add('element_mano', {type: 'entity'});
KojiComponent.attributes.add('element_swipeMano', {type: 'entity'});
KojiComponent.attributes.add('element_sound', {type: 'entity'});

KojiComponent.attributes.add('material_ball_color1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ball_color2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ball_color3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ball_color1_sticker', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ball_color2_sticker', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ball_color3_sticker', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ramp_color1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ramp_color2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ramp_color3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ramp_color1_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ramp_color2_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ramp_color3_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ball_BI', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_ball_BI_borde', {type: "asset", assetType: "material"});

KojiComponent.attributes.add('material_pista1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista1_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista1_edificio1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista1_edificio2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista1_edificio3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista1_edificio1_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista1_edificio2_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista1_edificio3_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista1_rampa', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista1_rampaBorde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista1_aro', {type: "asset", assetType: "material"});


KojiComponent.attributes.add('material_pista2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista2_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista2_edificio1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista2_edificio2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista2_edificio3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista2_edificio1_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista2_edificio2_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista2_edificio3_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista2_rampa', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista2_rampaBorde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista2_aro', {type: "asset", assetType: "material"});


KojiComponent.attributes.add('material_pista3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista3_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista3_edificio1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista3_edificio2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista3_edificio3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista3_edificio1_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista3_edificio2_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista3_edificio3_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista3_rampa', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista3_rampaBorde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista3_aro', {type: "asset", assetType: "material"});


KojiComponent.attributes.add('material_pista4', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista4_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista4_edificio1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista4_edificio2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista4_edificio3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista4_edificio1_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista4_edificio2_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista4_edificio3_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista4_rampa', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista4_rampaBorde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista4_aro', {type: "asset", assetType: "material"});


KojiComponent.attributes.add('material_pista5', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista5_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista5_edificio1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista5_edificio2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista5_edificio3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista5_edificio1_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista5_edificio2_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista5_edificio3_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista5_rampa', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista5_rampaBorde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista5_aro', {type: "asset", assetType: "material"});


KojiComponent.attributes.add('material_pista6', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista6_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista6_edificio1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista6_edificio2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista6_edificio3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista6_edificio1_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista6_edificio2_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista6_edificio3_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista6_rampa', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista6_rampaBorde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista6_aro', {type: "asset", assetType: "material"});


KojiComponent.attributes.add('material_pista7', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista7_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista7_edificio1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista7_edificio2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista7_edificio3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista7_edificio1_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista7_edificio2_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista7_edificio3_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista7_rampa', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista7_rampaBorde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista7_aro', {type: "asset", assetType: "material"});


KojiComponent.attributes.add('material_pista8', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista8_borde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista8_edificio1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista8_edificio2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista8_edificio3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista8_edificio1_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista8_edificio2_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista8_edificio3_cartel', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista8_rampa', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista8_rampaBorde', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_pista8_aro', {type: "asset", assetType: "material"});


KojiComponent.attributes.add('material_glow_color1', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_glow_color2', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('material_glow_color3', {type: "asset", assetType: "material"});
KojiComponent.attributes.add('particle_color1', {type: 'entity'});
KojiComponent.attributes.add('particle_color2', {type: 'entity'});
KojiComponent.attributes.add('particle_color3', {type: 'entity'});
KojiComponent.attributes.add('sound_musicaIngame',{type: 'entity'});

KojiComponent.attributes.add('masterClass',{type: 'entity'});
KojiComponent.attributes.add('textura_transparente', {type: "asset", assetType: "texture"});

KojiComponent.attributes.add('element_tapToContinue', {type: 'entity'});
KojiComponent.attributes.add('element_road', {type: 'entity'});

KojiComponent.attributes.add('total_score_end', {type: 'entity'});
KojiComponent.attributes.add('total_score_endText', {type: 'entity'});
KojiComponent.attributes.add('total_score', {type: 'entity'});
KojiComponent.attributes.add('best_score', {type: 'entity'});


KojiComponent.prototype.initialize = function() {  
        
    this.texturasCargadas = [];
    this.texturasCargadasEstado = [];
    this.arrayTextureVars = [];
    for(var i = 0; i < 1000; i++)
        this.texturasCargadasEstado[i] = 0;
    
    this.arrayTextureVars[0]="msg1_texto";
    this.arrayTextureVars[1]="msg2_texto";
    this.arrayTextureVars[2]="msg3_texto";
    this.arrayTextureVars[3]="msg4_texto";
    this.arrayTextureVars[4]="msg5_texto";
    this.arrayTextureVars[5]="msg6_texto";
    this.arrayTextureVars[6]="msg_text_col";
    this.arrayTextureVars[7]="logo1_texto";
    this.arrayTextureVars[8]="logo2_texto";
    this.arrayTextureVars[9]="logo1_text_col";
    this.arrayTextureVars[10]="logo2_text_col";
    this.arrayTextureVars[11]="instructions_texto";
    this.arrayTextureVars[12]="instructions_desc1_texto";
    this.arrayTextureVars[13]="instructions_desc2_texto";
    this.arrayTextureVars[14]="instructions_desc3_texto";
    this.arrayTextureVars[15]="instructions_text_col";
    this.arrayTextureVars[16]="instructions_desc1_text_col";
    this.arrayTextureVars[17]="instructions_desc2_text_col";
    this.arrayTextureVars[18]="instructions_desc3_text_col";
    this.arrayTextureVars[19]="msg1_textura";
    this.arrayTextureVars[20]="msg2_textura";
    this.arrayTextureVars[21]="msg3_textura";
    this.arrayTextureVars[22]="msg4_textura";
    this.arrayTextureVars[23]="msg5_textura";
    this.arrayTextureVars[24]="msg6_textura";
    this.arrayTextureVars[25]="mano_textura";
    this.arrayTextureVars[26]="swipe_bar_textura";
    this.arrayTextureVars[27]="soundON_textura";
    this.arrayTextureVars[28]="soundOFF_textura";
    this.arrayTextureVars[29]="tapToStart_texto";
    this.arrayTextureVars[30]="tapToContinue_texto";
    this.arrayTextureVars[31]="road_texto";
    this.arrayTextureVars[32]="tapToStart_text_col";
    this.arrayTextureVars[33]="tapToContinue_text_col";
    this.arrayTextureVars[34]="road_text_col";
    this.arrayTextureVars[35]="total_score_texto";
    this.arrayTextureVars[36]="total_score_text_col";
    this.arrayTextureVars[37]="best_score_texto";
    this.arrayTextureVars[38]="best_score_text_col";
    this.arrayTextureVars[39]="score_end_text_col";
    this.arrayTextureVars[40]="score_text_col";
    this.arrayTextureVars[41]="main_music";
    this.arrayTextureVars[42]="bola_sticker1_textura";
    this.arrayTextureVars[43]="bola_sticker2_textura";
    this.arrayTextureVars[44]="bola_sticker3_textura";
    this.arrayTextureVars[45]="bola_color1";
    this.arrayTextureVars[46]="bola_color2";
    this.arrayTextureVars[47]="bola_color3";
    this.arrayTextureVars[48]="bolaBI_textura";
    this.arrayTextureVars[49]="rampa1_textura";
    this.arrayTextureVars[50]="rampa2_textura";
    this.arrayTextureVars[51]="rampa3_textura";
    this.arrayTextureVars[52]="pista1_textura";
    this.arrayTextureVars[53]="pista1_color";
    this.arrayTextureVars[54]="pista1_borde_color";
    this.arrayTextureVars[55]="pista1_edificios1_textura";
    this.arrayTextureVars[56]="pista1_edificios1_color";
    this.arrayTextureVars[57]="pista1_edificios2_textura";
    this.arrayTextureVars[58]="pista1_edificios2_color";
    this.arrayTextureVars[59]="pista1_edificios3_textura";
    this.arrayTextureVars[60]="pista1_edificios3_color";
    this.arrayTextureVars[61]="pista1_edificiosCartel1_textura";
    this.arrayTextureVars[62]="pista1_edificiosCartel2_textura";
    this.arrayTextureVars[63]="pista1_edificiosCartel3_textura";
    this.arrayTextureVars[64]="pista1_rampaFinal_textura";
    this.arrayTextureVars[65]="pista1_aroCheckpoint_color";
    this.arrayTextureVars[66]="pista1_niebla_color";
    this.arrayTextureVars[67]="pista2_textura";
    this.arrayTextureVars[68]="pista2_color";
    this.arrayTextureVars[69]="pista2_borde_color";
    this.arrayTextureVars[70]="pista2_edificios1_textura";
    this.arrayTextureVars[71]="pista2_edificios1_color";
    this.arrayTextureVars[72]="pista2_edificios2_textura";
    this.arrayTextureVars[73]="pista2_edificios2_color";
    this.arrayTextureVars[74]="pista2_edificios3_textura";
    this.arrayTextureVars[75]="pista2_edificios3_color";
    this.arrayTextureVars[76]="pista2_edificiosCartel1_textura";
    this.arrayTextureVars[77]="pista2_edificiosCartel2_textura";
    this.arrayTextureVars[78]="pista2_edificiosCartel3_textura";
    this.arrayTextureVars[79]="pista2_rampaFinal_textura";
    this.arrayTextureVars[80]="pista2_aroCheckpoint_color";
    this.arrayTextureVars[81]="pista2_niebla_color";
    this.arrayTextureVars[82]="pista3_textura";
    this.arrayTextureVars[83]="pista3_color";
    this.arrayTextureVars[84]="pista3_borde_color";
    this.arrayTextureVars[85]="pista3_edificios1_textura";
    this.arrayTextureVars[86]="pista3_edificios1_color";
    this.arrayTextureVars[87]="pista3_edificios2_textura";
    this.arrayTextureVars[88]="pista3_edificios2_color";
    this.arrayTextureVars[89]="pista3_edificios3_textura";
    this.arrayTextureVars[90]="pista3_edificios3_color";
    this.arrayTextureVars[91]="pista3_edificiosCartel1_textura";
    this.arrayTextureVars[92]="pista3_edificiosCartel2_textura";
    this.arrayTextureVars[93]="pista3_edificiosCartel3_textura";
    this.arrayTextureVars[94]="pista3_rampaFinal_textura";
    this.arrayTextureVars[95]="pista3_aroCheckpoint_color";
    this.arrayTextureVars[96]="pista3_niebla_color";
    this.arrayTextureVars[97]="pista4_textura";
    this.arrayTextureVars[98]="pista4_color";
    this.arrayTextureVars[99]="pista4_borde_color";
    this.arrayTextureVars[100]="pista4_edificios1_textura";
    this.arrayTextureVars[101]="pista4_edificios1_color";
    this.arrayTextureVars[102]="pista4_edificios2_textura";
    this.arrayTextureVars[103]="pista4_edificios2_color";
    this.arrayTextureVars[104]="pista4_edificios3_textura";
    this.arrayTextureVars[105]="pista4_edificios3_color";
    this.arrayTextureVars[106]="pista4_edificiosCartel1_textura";
    this.arrayTextureVars[107]="pista4_edificiosCartel2_textura";
    this.arrayTextureVars[108]="pista4_edificiosCartel3_textura";
    this.arrayTextureVars[109]="pista4_rampaFinal_textura";
    this.arrayTextureVars[110]="pista4_aroCheckpoint_color";
    this.arrayTextureVars[111]="pista4_niebla_color";
    this.arrayTextureVars[112]="pista5_textura";
    this.arrayTextureVars[113]="pista5_color";
    this.arrayTextureVars[114]="pista5_borde_color";
    this.arrayTextureVars[115]="pista5_edificios1_textura";
    this.arrayTextureVars[116]="pista5_edificios1_color";
    this.arrayTextureVars[117]="pista5_edificios2_textura";
    this.arrayTextureVars[118]="pista5_edificios2_color";
    this.arrayTextureVars[119]="pista5_edificios3_textura";
    this.arrayTextureVars[120]="pista5_edificios3_color";
    this.arrayTextureVars[121]="pista5_edificiosCartel1_textura";
    this.arrayTextureVars[122]="pista5_edificiosCartel2_textura";
    this.arrayTextureVars[123]="pista5_edificiosCartel3_textura";
    this.arrayTextureVars[124]="pista5_rampaFinal_textura";
    this.arrayTextureVars[125]="pista5_aroCheckpoint_color";
    this.arrayTextureVars[126]="pista5_niebla_color";
    this.arrayTextureVars[127]="pista6_textura";
    this.arrayTextureVars[128]="pista6_color";
    this.arrayTextureVars[129]="pista6_borde_color";
    this.arrayTextureVars[130]="pista6_edificios1_textura";
    this.arrayTextureVars[131]="pista6_edificios1_color";
    this.arrayTextureVars[132]="pista6_edificios2_textura";
    this.arrayTextureVars[133]="pista6_edificios2_color";
    this.arrayTextureVars[134]="pista6_edificios3_textura";
    this.arrayTextureVars[135]="pista6_edificios3_color";
    this.arrayTextureVars[136]="pista6_edificiosCartel1_textura";
    this.arrayTextureVars[137]="pista6_edificiosCartel2_textura";
    this.arrayTextureVars[138]="pista6_edificiosCartel3_textura";
    this.arrayTextureVars[139]="pista6_rampaFinal_textura";
    this.arrayTextureVars[140]="pista6_aroCheckpoint_color";
    this.arrayTextureVars[141]="pista6_niebla_color";
    this.arrayTextureVars[142]="pista7_textura";
    this.arrayTextureVars[143]="pista7_color";
    this.arrayTextureVars[144]="pista7_borde_color";
    this.arrayTextureVars[145]="pista7_edificios1_textura";
    this.arrayTextureVars[146]="pista7_edificios1_color";
    this.arrayTextureVars[147]="pista7_edificios2_textura";
    this.arrayTextureVars[148]="pista7_edificios2_color";
    this.arrayTextureVars[149]="pista7_edificios3_textura";
    this.arrayTextureVars[150]="pista7_edificios3_color";
    this.arrayTextureVars[151]="pista7_edificiosCartel1_textura";
    this.arrayTextureVars[152]="pista7_edificiosCartel2_textura";
    this.arrayTextureVars[153]="pista7_edificiosCartel3_textura";
    this.arrayTextureVars[154]="pista7_rampaFinal_textura";
    this.arrayTextureVars[155]="pista7_aroCheckpoint_color";
    this.arrayTextureVars[156]="pista7_niebla_color";
    this.arrayTextureVars[157]="pista8_textura";
    this.arrayTextureVars[158]="pista8_color";
    this.arrayTextureVars[159]="pista8_borde_color";
    this.arrayTextureVars[160]="pista8_edificios1_textura";
    this.arrayTextureVars[161]="pista8_edificios1_color";
    this.arrayTextureVars[162]="pista8_edificios2_textura";
    this.arrayTextureVars[163]="pista8_edificios2_color";
    this.arrayTextureVars[164]="pista8_edificios3_textura";
    this.arrayTextureVars[165]="pista8_edificios3_color";
    this.arrayTextureVars[166]="pista8_edificiosCartel1_textura";
    this.arrayTextureVars[167]="pista8_edificiosCartel2_textura";
    this.arrayTextureVars[168]="pista8_edificiosCartel3_textura";
    this.arrayTextureVars[169]="pista8_rampaFinal_textura";
    this.arrayTextureVars[170]="pista8_aroCheckpoint_color";
    this.arrayTextureVars[171]="pista8_niebla_color";

    
};


KojiComponent.prototype.update = function(dt) 
{
    for(var i = 0; i<this.arrayTextureVars.length; i++)
    {
        //if(i == 0)
          // console.log("cargare la textura " + this.arrayTextureVars[i] + "  1 " + this.arrayTextureVars[i].includes("textura") + " 2 " + noEsNull(window[this.arrayTextureVars[i]]) + " 3 " +this.texturasCargadas.includes(window[this.arrayTextureVars[i]]) + " 4 " + this.texturasCargadasEstado[i]  );
        if(this.arrayTextureVars[i].includes("textura")) 
        {             
            if(noEsNull(window[this.arrayTextureVars[i]]) && !this.texturasCargadas.includes(window[this.arrayTextureVars[i]]) && this.texturasCargadasEstado[i] == 0)
            {
                 this.texturasCargadasEstado[i] = 1;
                 if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "mano_textura")
                    cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargada.bind(this,window[this.arrayTextureVars[i]],this.element_mano,i));  
                 else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "swipe_bar_textura")
                    cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargada.bind(this,window[this.arrayTextureVars[i]],this.element_swipeMano,i));  
                 else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "soundON_textura")
                    cargaTextura(window[this.arrayTextureVars[i]],this.app,this.soundOffON.bind(this,window[this.arrayTextureVars[i]],0,i));  
                 else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "soundOFF_textura")
                    cargaTextura(window[this.arrayTextureVars[i]],this.app,this.soundOffON.bind(this,window[this.arrayTextureVars[i]],1,i));  
                
                 else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "bola_sticker1_textura")
                     cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this.material_ball_color1_sticker.resource,i));  
                 else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "bola_sticker2_textura")
                     cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this.material_ball_color2_sticker.resource,i));  
                 else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "bola_sticker3_textura")
                     cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this.material_ball_color3_sticker.resource,i));  
                
                 else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "bolaBI_textura")
                     cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this.material_ball_BI.resource,i));  
                 else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "rampa1_textura")
                     cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this.material_ramp_color1.resource,i));  
                 else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "rampa2_textura")
                     cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this.material_ramp_color2.resource,i));  
                 else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "rampa3_textura")
                     cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this.material_ramp_color3.resource,i)); 
                 else if(this.arrayTextureVars[i].includes("pista"))
                 {
                    for(var k = 0; k < 8; k++)
                    {
                        if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "pista"+(k+1)+"_textura")
                            cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this["material_pista"+(k+1)].resource,i)); 
                        else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "pista"+(k+1)+"_edificios1_textura")
                            cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this["material_pista"+(k+1)+"_edificio1"].resource,i)); 
                        else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "pista"+(k+1)+"_edificios2_textura")
                            cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this["material_pista"+(k+1)+"_edificio2"].resource,i)); 
                        else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "pista"+(k+1)+"_edificios3_textura")
                            cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this["material_pista"+(k+1)+"_edificio3"].resource,i)); 
                        else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "pista"+(k+1)+"_edificiosCartel1_textura")
                            cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this["material_pista"+(k+1)+"_edificio1_cartel"].resource,i)); 
                        else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "pista"+(k+1)+"_edificiosCartel2_textura")
                            cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this["material_pista"+(k+1)+"_edificio2_cartel"].resource,i)); 
                        else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "pista"+(k+1)+"_edificiosCartel3_textura")
                            cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this["material_pista"+(k+1)+"_edificio3_cartel"].resource,i)); 
                        else if(noEsNull(window[this.arrayTextureVars[i]]) && this.arrayTextureVars[i] == "pista"+(k+1)+"_rampaFinal_textura")
                            cargaTextura(window[this.arrayTextureVars[i]],this.app,this.texturaCargadaMaterial.bind(this,window[this.arrayTextureVars[i]],this["material_pista"+(k+1)+"_rampa"].resource,i)); 
                    }
                 }
            }
        }
        else  if(this.arrayTextureVars[i].includes("texto")) 
         {             
            if(this.arrayTextureVars[i] == "tapToStart_texto")
            {
                if(this.element_tapToStart.element.text != window[this.arrayTextureVars[i]])
                    this.element_tapToStart.element.text =  window[this.arrayTextureVars[i]];
            }
            else  if(this.arrayTextureVars[i] == "road_texto")
            {
                if(!this.element_road.element.text.includes(window[this.arrayTextureVars[i]]))
                    this.element_road.element.text =  window[this.arrayTextureVars[i]];
            }
            else  if(this.arrayTextureVars[i] == "tapToContinue_texto")
            {
                if(this.element_tapToContinue.element.text != window[this.arrayTextureVars[i]])
                    this.element_tapToContinue.element.text =  window[this.arrayTextureVars[i]];
            }
            else  if(this.arrayTextureVars[i] == "logo1_texto")
            {
                if(this.element_mainLogo.children[0].element.text != window[this.arrayTextureVars[i]])
                    this.element_mainLogo.children[0].element.text =  window[this.arrayTextureVars[i]];
            }
            else  if(this.arrayTextureVars[i] == "logo2_texto")
            {
                if(this.element_mainLogo.children[1].element.text != window[this.arrayTextureVars[i]])
                    this.element_mainLogo.children[1].element.text =  window[this.arrayTextureVars[i]];
            }
            else  if(this.arrayTextureVars[i] == "instructions_texto")
            {
                if(this.element_tutorial.children[1].element.text != window[this.arrayTextureVars[i]])
                    this.element_tutorial.children[1].element.text =  window[this.arrayTextureVars[i]];
            }
            else  if(this.arrayTextureVars[i] == "instructions_desc1_texto")
            {
                if(this.element_tutorial.children[2].element.text != window[this.arrayTextureVars[i]])
                    this.element_tutorial.children[2].element.text =  window[this.arrayTextureVars[i]];
            }
            else  if(this.arrayTextureVars[i] == "instructions_desc2_texto")
            {
                if(this.element_tutorial.children[3].element.text != window[this.arrayTextureVars[i]])
                    this.element_tutorial.children[3].element.text =  window[this.arrayTextureVars[i]];
            }
            else  if(this.arrayTextureVars[i] == "instructions_desc3_texto")
            {
                if(this.element_tutorial.children[4].element.text != window[this.arrayTextureVars[i]])
                    this.element_tutorial.children[4].element.text =  window[this.arrayTextureVars[i]];
            }
            else  if(this.arrayTextureVars[i] == "msg1_texto")
            {
                if(this.element_gotBall_msg1.script.animaTextoMsg.boton[0] != window[this.arrayTextureVars[i]])
                    this.msgComeBola(0,window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "msg2_texto")
            {
               if(this.element_gotBall_msg1.script.animaTextoMsg.boton[1] != window[this.arrayTextureVars[i]])
                    this.msgComeBola(1,window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "msg3_texto")
            {
                if(this.element_gotBall_msg1.script.animaTextoMsg.boton[2] != window[this.arrayTextureVars[i]])
                    this.msgComeBola(2,window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "msg4_texto")
            {
              if(this.element_gotBall_msg1.script.animaTextoMsg.boton[3] != window[this.arrayTextureVars[i]])
                    this.msgComeBola(3,window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "msg5_texto")
            {
               if(this.element_gotBall_msg1.script.animaTextoMsg.boton[4] != window[this.arrayTextureVars[i]])
                    this.msgComeBola(4,window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "msg6_texto")
            {
                if(this.element_gotBall_msg1.script.animaTextoMsg.boton[5] != window[this.arrayTextureVars[i]])
                    this.msgComeBola(5,window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "total_score_texto")
            {
                if(this.total_score_endText.element.text != window[this.arrayTextureVars[i]])
                     this.total_score_endText.element.text =  window[this.arrayTextureVars[i]];
            } 
            else  if(this.arrayTextureVars[i] == "best_score_texto")
            {
                 if(!this.best_score.element.text.includes(window[this.arrayTextureVars[i]]))
                     this.best_score.element.text = window[this.arrayTextureVars[i]];
            } 
             
        }
        else  if(this.arrayTextureVars[i].includes("text_col")) 
        {             
            if(this.arrayTextureVars[i] == "tapToStart_text_col")
            {
                if(this.element_tapToStart.element.color.toString() != window[this.arrayTextureVars[i]])
                    this.element_tapToStart.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "road_text_col")
            {
                if(this.element_road.element.color.toString()  != window[this.arrayTextureVars[i]])
                    this.element_road.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "tapToContinue_text_col")
            {
                if(this.element_tapToContinue.element.color.toString()  != window[this.arrayTextureVars[i]])
                    this.element_tapToContinue.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "logo1_text_col")
            {
                if(this.element_mainLogo.children[0].element.color.toString()  != window[this.arrayTextureVars[i]])
                    this.element_mainLogo.children[0].element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "logo2_text_col")
            {
                if(this.element_mainLogo.children[1].element.color.toString()  != window[this.arrayTextureVars[i]])
                    this.element_mainLogo.children[1].element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "instructions_text_col")
            {
                if(this.element_tutorial.children[1].element.color.toString()  != window[this.arrayTextureVars[i]])
                    this.element_tutorial.children[1].element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "instructions_desc1_text_col")
            {
                if(this.element_tutorial.children[2].element.color.toString()  != window[this.arrayTextureVars[i]])
                    this.element_tutorial.children[2].element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "instructions_desc2_text_col")
            {
                if(this.element_tutorial.children[3].element.color.toString()  != window[this.arrayTextureVars[i]])
                    this.element_tutorial.children[3].element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "instructions_desc3_text_col")
            {
                if(this.element_tutorial.children[4].element.color.toString()  != window[this.arrayTextureVars[i]])
                    this.element_tutorial.children[4].element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "msg_text_col")
            {
                if(this.element_gotBall_msg1.element.color.toString()  != window[this.arrayTextureVars[i]])
                {
                    this.element_gotBall_msg1.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.element_gotBall_msg2.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.element_gotBall_msg3.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.element_gotBall_msg4.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);                    
                }
            } 
            else  if(this.arrayTextureVars[i] == "total_score_text_col")
            {
                if(this.total_score_endText.element.color.toString() != window[this.arrayTextureVars[i]])
                    this.total_score_endText.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "score_end_text_col")
            {
                if(this.total_score_end.element.color.toString() != window[this.arrayTextureVars[i]])
                    this.total_score_end.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "best_score_text_col")
            {
                if(this.best_score.element.color.toString() != window[this.arrayTextureVars[i]])
                    this.best_score.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }
            else  if(this.arrayTextureVars[i] == "score_text_col")
            {
                if(this.total_score.element.color.toString() != window[this.arrayTextureVars[i]])
                    this.total_score.element.color =  new pc.Color().fromString(window[this.arrayTextureVars[i]]);
            }       
        
        }
        else  if(this.arrayTextureVars[i].includes("_color")) 
        {             
            if(this.arrayTextureVars[i] == "bola_color1")
            {
                if(this.material_ball_color1.resource.diffuse == null || this.material_ball_color1.resource.diffuse.toString()  != window[this.arrayTextureVars[i]]) 
                {
                    this.material_ball_color1.resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ball_color1.resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ball_color1.resource.update();
                    
                    this.material_glow_color1.resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_glow_color1.resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_glow_color1.resource.update();
                    
                    this.material_ramp_color1_borde.resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ramp_color1_borde.resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ramp_color1_borde.resource.update();
                    
                   
                  
                    for(var p = 0; p < 4; p++)
                    {
                        var colorEsp = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                        this.particle_color1.children[p].particlesystem.colorGraph.curves[0].keys[0][1] = colorEsp.r;
                        this.particle_color1.children[p].particlesystem.colorGraph.curves[1].keys[0][1] = colorEsp.g;
                        this.particle_color1.children[p].particlesystem.colorGraph.curves[2].keys[0][1] = colorEsp.b;
                    }
                    
                }
            }
            else  if(this.arrayTextureVars[i] == "bola_color2")
            {
                if(this.material_ball_color2.resource.diffuse == null || this.material_ball_color2.resource.diffuse.toString()  != window[this.arrayTextureVars[i]])
                {
                    this.material_ball_color2.resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ball_color2.resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ball_color2.resource.update();
                    
                    this.material_glow_color2.resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_glow_color2.resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_glow_color2.resource.update();
                    
                    this.material_ramp_color2_borde.resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ramp_color2_borde.resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ramp_color2_borde.resource.update();
                    
                    for(var p = 0; p < 4; p++)
                    {
                       var colorEsp = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                        this.particle_color2.children[p].particlesystem.colorGraph.curves[0].keys[0][1] = colorEsp.r;
                        this.particle_color2.children[p].particlesystem.colorGraph.curves[1].keys[0][1] = colorEsp.g;
                        this.particle_color2.children[p].particlesystem.colorGraph.curves[2].keys[0][1] = colorEsp.b;
                    }
                }
            }
            else  if(this.arrayTextureVars[i] == "bola_color3")
            {
                if(this.material_ball_color3.resource.diffuse == null || this.material_ball_color3.resource.diffuse.toString()  != window[this.arrayTextureVars[i]])
                {
                    this.material_ball_color3.resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ball_color3.resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ball_color3.resource.update();
                    
                    this.material_glow_color3.resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_glow_color3.resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_glow_color3.resource.update();
                    
                    this.material_ramp_color3_borde.resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ramp_color3_borde.resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                    this.material_ramp_color3_borde.resource.update();
                    
                    for(var p = 0; p < 4; p++)
                    {
                        var colorEsp = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                        this.particle_color3.children[p].particlesystem.colorGraph.curves[0].keys[0][1] = colorEsp.r;
                        this.particle_color3.children[p].particlesystem.colorGraph.curves[1].keys[0][1] = colorEsp.g;
                        this.particle_color3.children[p].particlesystem.colorGraph.curves[2].keys[0][1] = colorEsp.b;
                    }
                }
            }
            else if(this.arrayTextureVars[i].includes("pista"))
            {
                for(var k = 0; k < 8; k++)
                {     
                    if(this.arrayTextureVars[i] == "pista"+(k+1)+"_color")
                    {
                        if((window["pista"+(k+1)+"_textura"] == null || window["pista"+(k+1)+"_textura"] == "") && (this["material_pista"+(k+1)].resource.diffuse == null || this["material_pista"+(k+1)].resource.diffuse.toString()  != window[this.arrayTextureVars[i]]) )  
                        {
                            this["material_pista"+(k+1)].resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)].resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)].resource.update();
                        }
                    }
                    else if(this.arrayTextureVars[i] == "pista"+(k+1)+"_edificios1_color")
                    {
                        if((window["pista"+(k+1)+"_edificios1_textura"] == null || window["pista"+(k+1)+"_edificios1_textura"] == "") && (this["material_pista"+(k+1)+"_edificio1"].resource.diffuse == null || this["material_pista"+(k+1)+"_edificio1"].resource.diffuse.toString()  != window[this.arrayTextureVars[i]]) )  
                        {
                            this["material_pista"+(k+1)+"_edificio1"].resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_edificio1"].resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_edificio1"].resource.update();
                        }
                    }
                    else if(this.arrayTextureVars[i] == "pista"+(k+1)+"_edificios2_color")
                    {
                        if((window["pista"+(k+1)+"_edificios2_textura"] == null || window["pista"+(k+1)+"_edificios2_textura"] == "") && (this["material_pista"+(k+1)+"_edificio2"].resource.diffuse == null || this["material_pista"+(k+1)+"_edificio2"].resource.diffuse.toString()  != window[this.arrayTextureVars[i]]) )  
                        {
                            this["material_pista"+(k+1)+"_edificio2"].resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_edificio2"].resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_edificio2"].resource.update();
                        }
                    }
                    else if(this.arrayTextureVars[i] == "pista"+(k+1)+"_edificios3_color")
                    {
                        if((window["pista"+(k+1)+"_edificios3_textura"] == null || window["pista"+(k+1)+"_edificios3_textura"] == "") && (this["material_pista"+(k+1)+"_edificio3"].resource.diffuse == null || this["material_pista"+(k+1)+"_edificio3"].resource.diffuse.toString()  != window[this.arrayTextureVars[i]]) )  
                        {
                            this["material_pista"+(k+1)+"_edificio3"].resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_edificio3"].resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_edificio3"].resource.update();
                        }
                    }
                    else if(this.arrayTextureVars[i] == "pista"+(k+1)+"_borde_color")
                    {
                        if(this["material_pista"+(k+1)+"_borde"].resource.diffuse == null || this["material_pista"+(k+1)+"_borde"].resource.diffuse.toString()  != window[this.arrayTextureVars[i]])  
                        {
                            this["material_pista"+(k+1)+"_borde"].resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_borde"].resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_borde"].resource.update();

                            this["material_pista"+(k+1)+"_rampaBorde"].resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_rampaBorde"].resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_rampaBorde"].resource.update();
                        }
                    }
                    else if(this.arrayTextureVars[i] == "pista"+(k+1)+"_aroCheckpoint_color")
                    {
                        if(this["material_pista"+(k+1)+"_aro"].resource.diffuse == null || this["material_pista"+(k+1)+"_aro"].resource.diffuse.toString()  != window[this.arrayTextureVars[i]])  
                        {
                            this["material_pista"+(k+1)+"_aro"].resource.diffuse = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_aro"].resource.emissive = new pc.Color().fromString(window[this.arrayTextureVars[i]]);
                            this["material_pista"+(k+1)+"_aro"].resource.update();                        
                        }
                    }
                    
                }
            }           
           
        } 
        else if(this.arrayTextureVars[i].includes("music")) 
        {               
           if(noEsNull(window[this.arrayTextureVars[i]]) && !this.texturasCargadas.includes(window[this.arrayTextureVars[i]]) && this.texturasCargadasEstado[i] == 0)
           {
               this.texturasCargadasEstado[i] = 1;
               if(this.arrayTextureVars[i] == "main_music")                   
                   cargaMusica(window[this.arrayTextureVars[i]],this.app,this.sonidoCargado.bind(this,window[this.arrayTextureVars[i]],this.sound_musicaIngame,i,"musicaIngame"));  
           }               
            
        }
        
    }
    for(var l = 0; l < 8; l++)
    {
        if(l < 3)
        {
            if(window["bola_sticker"+(l+1)+"_textura"] == "" || window["bola_sticker"+(l+1)+"_textura"] == null)
            {
                window["bola_sticker"+(l+1)+"_textura"]  = "transparente";
                this["material_ball_color"+(l+1)+"_sticker"].resource.diffuse = new pc.Color(1,1,1,1);
                this["material_ball_color"+(l+1)+"_sticker"].resource.emissive = new pc.Color(1,1,1,1);
               
                this["material_ball_color"+(l+1)+"_sticker"].resource.diffuseMap = this.textura_transparente.resource;
                this["material_ball_color"+(l+1)+"_sticker"].resource.emissiveMap = this.textura_transparente.resource;
                this["material_ball_color"+(l+1)+"_sticker"].resource.opacity = this.textura_transparente.resource;
            }           
        }
        if(window["pista"+(l+1)+"_edificiosCartel1_textura"] == "" || window["pista"+(l+1)+"_edificiosCartel1_textura"] == null)
        {
            window["pista"+(l+1)+"_edificiosCartel1_textura"]  = "transparente";
            this["material_pista"+(l+1)+"_edificio1_cartel"].resource.diffuse = new pc.Color(1,1,1,1);
            this["material_pista"+(l+1)+"_edificio1_cartel"].resource.emissive = new pc.Color(1,1,1,1);
               
            this["material_pista"+(l+1)+"_edificio1_cartel"].resource.diffuseMap = this.textura_transparente.resource;
            this["material_pista"+(l+1)+"_edificio1_cartel"].resource.emissiveMap = this.textura_transparente.resource;
            this["material_pista"+(l+1)+"_edificio1_cartel"].resource.opacity = this.textura_transparente.resource;
        } 
        if(window["pista"+(l+1)+"_edificiosCartel2_textura"] == "" || window["pista"+(l+1)+"_edificiosCartel2_textura"] == null)
        {
            window["pista"+(l+1)+"_edificiosCartel2_textura"]  = "transparente";
            this["material_pista"+(l+1)+"_edificio2_cartel"].resource.diffuse = new pc.Color(1,1,1,1);
            this["material_pista"+(l+1)+"_edificio2_cartel"].resource.emissive = new pc.Color(1,1,1,1);
               
            this["material_pista"+(l+1)+"_edificio2_cartel"].resource.diffuseMap = this.textura_transparente.resource;
            this["material_pista"+(l+1)+"_edificio2_cartel"].resource.emissiveMap = this.textura_transparente.resource;
            this["material_pista"+(l+1)+"_edificio2_cartel"].resource.opacity = this.textura_transparente.resource;
        }         
        if(window["pista"+(l+1)+"_edificiosCartel3_textura"] == "" || window["pista"+(l+1)+"_edificiosCartel3_textura"] == null)
        {
            window["pista"+(l+1)+"_edificiosCartel3_textura"]  = "transparente";
            this["material_pista"+(l+1)+"_edificio3_cartel"].resource.diffuse = new pc.Color(1,1,1,1);
            this["material_pista"+(l+1)+"_edificio3_cartel"].resource.emissive = new pc.Color(1,1,1,1);
               
            this["material_pista"+(l+1)+"_edificio3_cartel"].resource.diffuseMap = this.textura_transparente.resource;
            this["material_pista"+(l+1)+"_edificio3_cartel"].resource.emissiveMap = this.textura_transparente.resource;
            this["material_pista"+(l+1)+"_edificio3_cartel"].resource.opacity = this.textura_transparente.resource;
        }         
    }
    
};


KojiComponent.prototype.texturaCargada = function(url,enty,indexTex,tex)
{
    tex.mipmaps = false;
    enty.element.textureAsset = tex;
    this.texturasCargadas.push(url);
    this.texturasCargadasEstado[indexTex] = 0;
};
KojiComponent.prototype.sonidoCargado = function(url,enty,indexTex,name,snd)
{       
    enty.sound.slot(name).asset = snd;
    this.texturasCargadas.push(url);
    this.texturasCargadasEstado[indexTex] = 0;
};
KojiComponent.prototype.texturaCargadaMaterial = function(url,material,indexTex,tex)
{      
    material.diffuse = new pc.Color(1,1,1,1);
    material.emissive = new pc.Color(1,1,1,1);
    material.diffuseMap = tex.resource;    
    material.opacityMap = tex.resource; 
    material.emissiveMap = tex.resource;     
    material.update(); 
    tex.mipmaps = false; 
    this.texturasCargadas.push(url);
    this.texturasCargadasEstado[indexTex] = 0;
};
KojiComponent.prototype.msgComeBola = function(index,str)
{
    for(var i = 0; i < 4; i++)
    {
        this["element_gotBall_msg"+(i+1)].script.animaTextoMsg.boton[index] = str;        
    }
};
KojiComponent.prototype.soundOffON = function(url,index,indexTex,tex)
{
    this.element_sound.script.animaMsg.boton[index] = tex;
    this.texturasCargadas.push(url);
    _control.checkSoundConfig();
    this.texturasCargadasEstado[indexTex] = 0;
};